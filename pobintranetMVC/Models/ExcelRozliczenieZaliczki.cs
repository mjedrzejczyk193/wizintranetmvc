﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace pobintranetMVC.Models
{
    public class ExcelRozliczenieZaliczki
    {
        public DateTime data
        {
            get;
            set;
        }

        public int idpracownika
        {
            get;
            set;
        }

        public double kwota
        {
            get;
            set;
        }

        public string pracownik
        {
            get;
            set;
        }

        public string tytul
        {
            get;
            set;
        }

        public ExcelRozliczenieZaliczki(string _pracownik, double _kwota, string _tytul, string _data)
        {
            this.pracownik = _pracownik;
            this.kwota = _kwota;
            this.tytul = _tytul;
            this.data = DateTime.ParseExact(_data, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            this.idpracownika = this.GetIdPracownika(_pracownik);
        }

        private int GetIdPracownika(string _pracownik)
        {
            int num;
            intranetDataContext _intranetDataContext = new intranetDataContext();
            try
            {
                List<string> list = _pracownik.Split(new char[] { ' ' }).ToList<string>();
                num = (list[0] != "MOŁDOCH" ? (
                    //from p in _intranetDataContext.pracowniks
                    //where p.imie.Contains(list[1]) && p.nazwisko.Contains(list[0])
                    //select p).First<pracownik>().idpracownika 
                    _intranetDataContext.pracowniks.Where(p => p.imie.Contains(list[1]) && p.nazwisko.Contains(list[0])).FirstOrDefault().idpracownika)
                    : 10494);
            }
            catch (Exception)
            {
                num = 0;
            }
            return num;
        }
    }
}