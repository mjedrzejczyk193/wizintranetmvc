﻿using pobintranetMVC.Controllers._06;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Diagnostics;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace pobintranetMVC.Models
{
    public static class Help
    {
        private static DataClasses1DataContext db;

        private static intranetDataContext db2;

        public static string connectionString;

        public static string adres_serwera;

        public static string sciezka_skanowania;

        public static string user;

        public static string pass;

        public static string domain;

        private static List<DirFile> tempDirFileList;

        public static string pustyPodpis;

        public static string data_uruchomienia;

        static Help()
        {
            Help.db = new DataClasses1DataContext();
            Help.db2 = new intranetDataContext();
            Help.connectionString = ConfigurationManager.ConnectionStrings["pob_iNTRANETConnectionString"].ConnectionString;
            Help.adres_serwera = @"\\10.66.15.109";
            Help.sciezka_skanowania = @"\skany\faktury";
            Help.domain = "praca";
            Help.user = @"skany";
            Help.pass = "SK2020!@";
            Help.tempDirFileList = new List<DirFile>();
            Help.pustyPodpis = "iVBORw0KGgoAAAANSUhEUgAAA+gAAAH0CAYAAACuKActAAAgAElEQVR4Xu3XMREAAAgDMfBvGhn8EBT0UpbuOAIECBAgQIAAAQIECBAgQOBdYN8TCECAAAECBAgQIECAAAECBAiMge4JCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAFwrhPQAAAbMSURBVAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBAw0P0AAQIECBAgQIAAAQIECBAICBjogRJEIECAAAECBAgQIECAAAECBrofIECAAAECBAgQIECAAAECAQEDPVCCCAQIECBAgAABAgQIECBAwED3AwQIECBAgAABAgQIECBAICBgoAdKEIEAAQIECBAgQIAAAQIECBjofoAAAQIECBAgQIAAAQIECAQEDPRACSIQIECAAAECBAgQIECAAAED3Q8QIECAAAECBAgQIECAAIGAgIEeKEEEAgQIECBAgAABAgQIECBgoPsBAgQIECBAgAABAgQIECAQEDDQAyWIQIAAAQIECBAgQIAAAQIEDHQ/QIAAAQIECBAgQIAAAQIEAgIGeqAEEQgQIECAAAECBAgQIECAgIHuBwgQIECAAAECBAgQIECAQEDAQA+UIAIBAgQIECBAgAABAgQIEDDQ/QABAgQIECBAgAABAgQIEAgIGOiBEkQgQIAAAQIECBAgQIAAAQIGuh8gQIAAAQIECBAgQIAAAQIBAQM9UIIIBAgQIECAAAECBAgQIEDAQPcDBAgQIECAAAECBAgQIEAgIGCgB0oQgQABAgQIECBAgAABAgQIGOh+gAABAgQIECBAgAABAgQIBAQM9EAJIhAgQIAAAQIECBAgQIAAAQPdDxAgQIAAAQIECBAgQIAAgYCAgR4oQQQCBAgQIECAAAECBAgQIGCg+wECBAgQIECAAAECBAgQIBAQMNADJYhAgAABAgQIECBAgAABAgQMdD9AgAABAgQIECBAgAABAgQCAgZ6oAQRCBAgQIAAAQIECBAgQICAge4HCBAgQIAAAQIECBAgQIBAQMBAD5QgAgECBAgQIECAAAECBAgQMND9AAECBAgQIECAAAECBAgQCAgY6IESRCBAgAABAgQIECBAgAABAga6HyBAgAABAgQIECBAgAABAgEBAz1QgggECBAgQIAAAQIECBAgQMBA9wMECBAgQIAAAQIECBAgQCAgYKAHShCBAAECBAgQIECAAAECBAgY6H6AAAECBAgQIECAAAECBAgEBAz0QAkiECBAgAABAgQIECBAgAABA90PECBAgAABAgQIECBAgACBgICBHihBBAIECBAgQIAAAQIECBAgYKD7AQIECBAgQIAAAQIECBAgEBAw0AMliECAAAECBAgQIECAAAECBAx0P0CAAAECBAgQIECAAAECBAICBnqgBBEIECBAgAABAgQIECBAgICB7gcIECBAgAABAgQIECBAgEBAwEAPlCACAQIECBAgQIAAAQIECBA42NgB9XiesokAAAAASUVORK5CYII=";
            Help.data_uruchomienia = "01-09-2020";
        }

        internal static void NrSprzedazyPlus()
        {
            string sql = "UPDATE parametry SET nr_sprzedazy = nr_sprzedazy + 1";
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("{0}", sql), sqlConnection);
                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            }
        }

        internal static void WpiszCenyMaterialuDoArchiwum(int idMaterialu, decimal staraCena, decimal nowaCena)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                magazyn_materialy_cena_arch mmca = new magazyn_materialy_cena_arch();
                mmca.data_wprowadzenia = DateTime.Now;
                mmca.cena = nowaCena;
                mmca.poprzednia_cena = staraCena;
                mmca.idmaterialu = idMaterialu;
                mmca.kod = GetKodMaterialu(idMaterialu);
                if (nowaCena != staraCena)
                {
                    dc.magazyn_materialy_cena_arches.InsertOnSubmit(mmca);
                    dc.SubmitChanges();
                }
            }
            catch (Exception)
            {

            }
        }

        internal static float GetResztaZGodziny(string ilosc)
        {
            int minutes = 0;
            try
            {
                minutes = TimeSpan.Parse(ilosc).Minutes;
            }
            catch (Exception)
            {
                try
                {
                    minutes = Convert.ToInt32(ilosc.Split(':')[1]);
                }
                catch (Exception)
                {
                }
            }
            try
            {
                if (minutes > 0 && minutes < 30)
                {
                    return float.Parse("0.25");
                }
                else if (minutes == 30)
                {
                    return float.Parse("0.5");
                }
                else if (minutes > 30 && minutes < 59)
                {
                    return float.Parse("0.75");
                }
                return 0;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static string GetNumerZamowienia(int id)
        {
            return db2.zamowieniaDokuments.Where(z => z.IdZamowienia == id).First().NrZamowienia;
        }

        public static string GetSumaZamowieniaZPozycji(int id)
        {
            return db2.zamowieniaPozycjes.Where(z => z.IdZamowienia == id).Sum(z => Math.Round(Convert.ToDecimal(z.WartoscPozycji), 2, MidpointRounding.AwayFromZero)).ToString("C");
        }

        public static string GetSumaZamowieniaZPozycji2(int id)
        {
            return db2.zamowieniaPozycjes.Where(z => z.IdZamowienia == id).Sum(z => Math.Round(Convert.ToDecimal(z.WartoscPozycji), 2, MidpointRounding.AwayFromZero)).ToString().Replace("00", "");
        }

        internal static projekt GetProjekt(int idp)
        {
            return db2.projekts.Where(p => p.idprojektu == idp).First();
        }

        public static loginy GetLogin(string zu)
        {
            try
            {

                intranetDataContext dc = new intranetDataContext();
                return dc.loginy.Where(l => l.pracownik_id == Convert.ToInt32(zu)).First();
            }
            catch (Exception)
            {
                return new loginy();
            }
        }

        public static void CzyscSciezkePDF(string sciezka)
        {
            try
            {
                FileInfo[] files = (new DirectoryInfo(string.Concat(sciezka, "pdf\\"))).GetFiles();
                for (int i = 0; i < (int)files.Length; i++)
                {
                    if (files[i].CreationTime.ToShortDateString() != DateTime.Now.ToShortDateString())
                    {
                        files[i].Delete();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public static string DataSlownie(int? miesiac, int? rok)
        {
            int? nullable = miesiac;
            if (nullable.HasValue)
            {
                switch (nullable.GetValueOrDefault())
                {
                    case 1:
                        {
                            return string.Format("Styczeń {0}", rok);
                        }
                    case 2:
                        {
                            return string.Format("Luty {0}", rok);
                        }
                    case 3:
                        {
                            return string.Format("Marzec {0}", rok);
                        }
                    case 4:
                        {
                            return string.Format("Kwiecień {0}", rok);
                        }
                    case 5:
                        {
                            return string.Format("Maj {0}", rok);
                        }
                    case 6:
                        {
                            return string.Format("Czerwiec {0}", rok);
                        }
                    case 7:
                        {
                            return string.Format("Lipiec {0}", rok);
                        }
                    case 8:
                        {
                            return string.Format("Sierpień {0}", rok);
                        }
                    case 9:
                        {
                            return string.Format("Wrzesień {0}", rok);
                        }
                    case 10:
                        {
                            return string.Format("Październik {0}", rok);
                        }
                    case 11:
                        {
                            return string.Format("Listopad {0}", rok);
                        }
                    case 12:
                        {
                            return string.Format("Grudzień {0}", rok);
                        }
                }
            }
            return "";
        }

        public static void DodajSkan(int idfaktury, string plik, int kto)
        {
            intranetDataContext _intranetDataContext = new intranetDataContext();
            skany _skany = new skany();
            try
            {
                bool juzJest = _intranetDataContext.skany.Where(s => s.id_faktury == idfaktury && s.zaakceptowano_rachunkowo != -1).Any();
                if (juzJest)
                {
                    _skany.id_faktury = idfaktury;
                    skany starySkan = _intranetDataContext.skany.Where(s => s.id_faktury == idfaktury && s.zaakceptowano_rachunkowo != -1).First();
                    _skany.sciezka = plik;
                    _skany.data_utworzenia = DateTime.Now;
                    _skany.zaakceptowano_rachunkowo = starySkan.zaakceptowano_rachunkowo;
                    _skany.zaakceptowano_dz_zakupu = starySkan.zaakceptowano_dz_zakupu;
                    _skany.zaakceptowano_kierownik = starySkan.zaakceptowano_kierownik;
                    _skany.zaakceptowano_dyrektor = starySkan.zaakceptowano_dyrektor;
                    _skany.zaakceptowano_prezes = starySkan.zaakceptowano_prezes;
                    _skany.przekierowanie_r = starySkan.przekierowanie_r;
                    _skany.przekierowanie_z = starySkan.przekierowanie_z;
                    _skany.przekierowanie_k = starySkan.przekierowanie_k;
                    _skany.czesciowa_ile = 0;
                }
                else
                {
                    _skany = new skany()
                    {
                        id_faktury = idfaktury,
                        sciezka = plik,
                        data_utworzenia = DateTime.Now,
                        zaakceptowano_kierownik = -1,
                        zaakceptowano_prezes = -1,
                        zaakceptowano_rachunkowo = -1,
                        zaakceptowano_dyrektor = -1,
                        zaakceptowano_dz_zakupu = -1,
                        przekierowanie_r = -1,
                        przekierowanie_z = -1,
                        przekierowanie_k = -1,
                        czesciowa_ile = 0
                    };
                }
            }
            catch (Exception)
            {
                _skany = new skany()
                {
                    id_faktury = idfaktury,
                    sciezka = plik,
                    data_utworzenia = DateTime.Now,
                    zaakceptowano_kierownik = -1,
                    zaakceptowano_prezes = -1,
                    zaakceptowano_rachunkowo = -1,
                    zaakceptowano_dyrektor = -1,
                    zaakceptowano_dz_zakupu = -1,
                    przekierowanie_r = -1,
                    przekierowanie_z = -1,
                    przekierowanie_k = -1,
                    czesciowa_ile = 0
                };
            }

            try
            {
                _skany.kto = new int?(kto);
            }
            catch (Exception)
            {
                _skany.kto = -1;
            }
            pobintranetMVC.zakup _zakup = _intranetDataContext.zakups.Where(z => z.idzakupu == idfaktury).FirstOrDefault();
            try
            {
                kontaSyntetyczne2 _kontaSyntetyczne2 = (
                    from k in _intranetDataContext.kontaSyntetyczne2
                    where (int?)k.ids2 == _zakup.ids2
                    select k).First<kontaSyntetyczne2>();
                _skany.id_pracownika = (
                    from p in _intranetDataContext.projekts
                    where p.numer == (int?)Convert.ToInt32(_kontaSyntetyczne2.s2nr)
                    select p).First<projekt>().idpracownika;
            }
            catch (Exception)
            {
                _skany.id_pracownika = new int?(0);
            }
            _intranetDataContext.skany.InsertOnSubmit(_skany);
            _intranetDataContext.SubmitChanges();
        }

        public static int SprawdzIloscFakturDoPodpisania(int zu, int ktory)
        {
            intranetDataContext dc = new intranetDataContext();
            switch (ktory)
            {
                case 1:
                    return dc.skany.Where(s => s.przekierowanie_r == zu && s.zaakceptowano_rachunkowo == -1).Select(s => s.id_faktury).Distinct().ToList().Count;
                case 2:
                    return dc.skany.Where(s => s.przekierowanie_z == zu && s.zaakceptowano_rachunkowo != -1 && s.zaakceptowano_dz_zakupu == -1).Select(s => s.id_faktury).Distinct().ToList().Count;
                case 3:
                    return dc.skany.Where(s => s.przekierowanie_k == zu && s.zaakceptowano_dz_zakupu != -1 && s.zaakceptowano_kierownik == -1).Select(s => s.id_faktury).Distinct().ToList().Count;
                case 4:
                    return dc.skany.Where(s => s.zaakceptowano_dyrektor == -1 && s.zaakceptowano_kierownik != -1 && s.zaakceptowano_dz_zakupu != -1 && s.zaakceptowano_rachunkowo != -1).Select(s => s.id_faktury).Distinct().ToList().Count;
                default:
                    return dc.skany.Where(s => s.zaakceptowano_prezes == -1 && s.zaakceptowano_dyrektor != -1).Select(s => s.id_faktury).Distinct().ToList().Count;
            }
        }

        public static List<akord_stawka> GetAkordStawka(int rok, int pracownik)
        {
            List<akord_stawka> akordStawkas;
            List<akord_stawka> akordStawkas1 = new List<akord_stawka>();
            using (SqlConnection sqlConnection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("select idpracownika as pracownik, miesiac, sum(suma_godz_zl)  as akord, stawka_godz as stawka, CASE WHEN dbo.akord_dni_full.rok < 2019 THEN 0 WHEN dbo.akord_dni_full.rok = 2019 AND dbo.akord_dni_full.miesiac < 9 THEN 0 ELSE sum(dbo.akord_dni_full.godziny_nocne)*0.2*ISNULL((SELECT stawka_min FROM kadry_parametry WHERE miesiac = dbo.akord_dni_full.miesiac AND rok = dbo.akord_dni_full.rok),0) END as godziny_nocne from akord_dni_full where rok = {0} and idpracownika = {1} group by idpracownika, miesiac, stawka_godz, rok", rok, pracownik), sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                try
                {
                    while (sqlDataReader.Read())
                    {
                        akord_stawka akordStawka = new akord_stawka()
                        {
                            idpracownika = sqlDataReader.GetInt32(0),
                            miesiac = sqlDataReader.GetInt32(1),
                            akord = sqlDataReader.GetDouble(2) + sqlDataReader.GetDouble(4),
                            stawka = sqlDataReader.GetDouble(3)
                        };
                        akordStawkas1.Add(akordStawka);
                    }
                }
                finally
                {
                    sqlDataReader.Close();
                }
                akordStawkas = akordStawkas1;
            }
            return akordStawkas;
        }

        public static List<data_budowa> GetDataBudowa(int rok, int pracownik)
        {
            List<data_budowa> dataBudowas;
            List<data_budowa> dataBudowas1 = new List<data_budowa>();
            using (SqlConnection sqlConnection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("select id_prac, cast(a.Data as smalldatetime) as Data, (select'[' + cast(numer as nvarchar) + '] ' + nazwa from projekt where idprojektu = a.idbudowy) as Budowa, a.godz_od as Od, a.godz_do as Do, a.przestoje as Przestoje, a.dojazd as Dojazd from akord_godziny a WHERE YEAR(a.Data) = {0} and id_prac = {1} order by Data", rok, pracownik), sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                try
                {
                    while (sqlDataReader.Read())
                    {
                        data_budowa dataBudowa = new data_budowa()
                        {
                            id_prac = sqlDataReader.GetInt32(0),
                            data = sqlDataReader.GetDateTime(1),
                            budowa = sqlDataReader.GetString(2),
                            @from = sqlDataReader.GetDouble(3),
                            to = sqlDataReader.GetDouble(4),
                            dojazd = sqlDataReader.GetDouble(5),
                            przestoje = sqlDataReader.GetDouble(6)
                        };
                        dataBudowas1.Add(dataBudowa);
                    }
                }
                finally
                {
                    sqlDataReader.Close();
                }
                dataBudowas = dataBudowas1;
            }
            return dataBudowas;
        }

        public static List<ExcelRozliczenieZaliczki> GetExcelRozliczenieZaliczki(string path, HttpPostedFileBase excelFile)
        {
            ISheet sheet;
            List<ExcelRozliczenieZaliczki> excelRozliczenieZaliczkis = new List<ExcelRozliczenieZaliczki>();
            sheet = (Path.GetExtension(path) != ".xls" ? (new XSSFWorkbook(excelFile.InputStream)).GetSheetAt(0) : (new HSSFWorkbook(excelFile.InputStream)).GetSheetAt(0));
            for (int i = 0; i <= sheet.LastRowNum; i++)
            {
                if (sheet.GetRow(i) != null)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(sheet.GetRow(i).GetCell(0).StringCellValue))
                        {
                            break;
                        }
                        else
                        {
                            string stringCellValue = sheet.GetRow(i).GetCell(0).StringCellValue;
                            double num = 0;
                            try
                            {
                                num = Convert.ToDouble(sheet.GetRow(i).GetCell(1).NumericCellValue);
                            }
                            catch (Exception)
                            {
                                try
                                {
                                    num = Convert.ToDouble(sheet.GetRow(i).GetCell(1).StringCellValue.Replace(" zł", ""));
                                }
                                catch (Exception)
                                {
                                    num = 0;
                                }
                            }
                            string str = sheet.GetRow(i).GetCell(2).StringCellValue;
                            string stringCellValue1 = "";
                            try
                            {
                                stringCellValue1 = sheet.GetRow(i).GetCell(3).StringCellValue;
                            }
                            catch (Exception)
                            {
                                DateTime dateTime = DateTime.FromOADate(sheet.GetRow(i).GetCell(3).NumericCellValue);
                                stringCellValue1 = dateTime.ToString("dd-MM-yyyy");
                            }
                            excelRozliczenieZaliczkis.Add(new ExcelRozliczenieZaliczki(stringCellValue, num, str, stringCellValue1));
                        }
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
            }
            return excelRozliczenieZaliczkis;
        }

        public static pobintranetMVC.zakup GetFaktura(int idFaktury)
        {
            return Help.db2.zakups.Where(z => z.idzakupu == idFaktury).FirstOrDefault();
            //(
            //from f in Help.db2.zakups
            //where f.idzakupu == idFaktury
            //select f).First<zakup>();
        }

        public static dynamic GetImieNazwiskoPracownika(int? pracownik)
        {
            if (pracownik == -1 || pracownik == null)
            {
                return "BRAK";
            }
            else if (pracownik == -2)
            {
                return "DYREKTOR";
            }
            else if (pracownik == -3)
            {
                return "PREZES";
            }
            else if (pracownik == 0)
            {
                return "BRAK";
            }
            //pobintranetMVC.Models.pracownik _pracownik = (
            //    from p in Help.db.pracownik
            //    where p.idpracownika == pracownik
            //    select p).First<pobintranetMVC.Models.pracownik>();
            intranetDataContext dc = new intranetDataContext();
            pobintranetMVC.pracownik _pracownik = dc.pracowniks.Where(p => p.idpracownika == pracownik).FirstOrDefault();
            return string.Format("{0} {1}", _pracownik.imie, _pracownik.nazwisko);
        }

        public static dynamic GetEmail(int? pracownik)
        {
            intranetDataContext dc = new intranetDataContext();
            loginy login = dc.loginy.Where(l => l.pracownik_id == Convert.ToInt32(pracownik)).FirstOrDefault();
            return login.login_email;
        }

        public static string GetImieNazwiskoPracownikaZEmaila(string email)
        {
            if(string.IsNullOrEmpty(email))
            {
                return "";
            }
            
            intranetDataContext dc = new intranetDataContext();
            loginy login = dc.loginy.Where(l => l.login_email == email.Replace(" ", "")).FirstOrDefault();
            return GetImieNazwiskoPracownika(login.pracownik_id);
        }

        public static List<DirFile> GetListaFolderowIPlikow(string sciezka)
        {
            string[] directories;
            int i;
            List<DirFile> dirFiles = new List<DirFile>();
            intranetDataContext _intranetDataContext = new intranetDataContext();
            if (!string.IsNullOrEmpty(sciezka))
            {
                directories = Directory.GetDirectories(sciezka);
                for (i = 0; i < (int)directories.Length; i++)
                {
                    string str = directories[i];
                    DirFile dirFile = new DirFile()
                    {
                        rodzaj = "dir",
                        sciezka = str,
                        nazwa = str.Split(new char[] { '\\' })[(int)str.Split(new char[] { '\\' }).Length - 1],
                        data_utworzenia = Directory.GetCreationTime(str)
                    };
                    if (dirFile.data_utworzenia > DateTime.Parse("30-04-2020"))
                    {
                        dirFiles.Add(dirFile);
                    }
                }
                directories = Directory.GetFiles(sciezka);
                for (i = 0; i < (int)directories.Length; i++)
                {
                    string str1 = directories[i];
                    string str2 = str1;
                    string str3 = str2.Substring(str2.Length - 4);
                    string str4 = str1.Split(new char[] { '\\' })[(int)str1.Split(new char[] { '\\' }).Length - 1];
                    if (str3.ToLower() == ".pdf" && !str1.Contains(" "))
                    {
                        if (!(
                            from s in _intranetDataContext.skany
                            where s.sciezka == str4
                            select s).Any<skany>())
                        {
                            DirFile dirFile1 = new DirFile()
                            {
                                rodzaj = "file",
                                sciezka = str1,
                                nazwa = str4,
                                data_utworzenia = File.GetCreationTime(str1)
                            };
                            dirFiles.Add(dirFile1);
                        }
                    }
                }
            }
            else
            {

                using (NetworkConnection networkConnection = new NetworkConnection(Help.adres_serwera, new NetworkCredential(Help.user, Help.pass, Help.domain)))
                {
                    directories = Directory.GetDirectories(string.Concat(Help.adres_serwera, Help.sciezka_skanowania));
                }
                for (i = 0; i < (int)directories.Length; i++)
                {
                    string str5 = directories[i];
                    DirFile dirFile2 = new DirFile()
                    {
                        rodzaj = "dir",
                        sciezka = str5,
                        nazwa = str5.Split(new char[] { '\\' })[(int)str5.Split(new char[] { '\\' }).Length - 1],
                        data_utworzenia = Directory.GetCreationTime(str5)
                    };
                    if (dirFile2.data_utworzenia > DateTime.Parse("30-04-2020"))
                    {
                        dirFiles.Add(dirFile2);
                    }
                }
                directories = Directory.GetFiles(string.Concat(Help.adres_serwera, Help.sciezka_skanowania));
                for (i = 0; i < (int)directories.Length; i++)
                {
                    string str6 = directories[i];
                    string str7 = str6;
                    string str8 = str7.Substring(str7.Length - 4);
                    string str9 = str6.Split(new char[] { '\\' })[(int)str6.Split(new char[] { '\\' }).Length - 1];
                    if (str8 == ".pdf" && !str6.Contains(" "))
                    {
                        if (!(
                            from s in _intranetDataContext.skany
                            where s.sciezka == str9
                            select s).Any<skany>())
                        {
                            DirFile dirFile3 = new DirFile()
                            {
                                rodzaj = "file",
                                sciezka = str6,
                                nazwa = str9,
                                data_utworzenia = File.GetCreationTime(str6)
                            };
                            dirFiles.Add(dirFile3);
                        }
                    }
                }
            }

            return dirFiles;
        }

        public static int GetKierownikBudowy(int? id)
        {
            if (id == -1)
            {
                return -1;
            }
            try
            {
                intranetDataContext dc = new intranetDataContext();
                var s2 = dc.kontaSyntetyczne2.Where(ks => ks.ids2 == id).FirstOrDefault();
                var projekt = dc.projekts.Where(p => p.idprojektu == s2.idProjektu).FirstOrDefault();
                if (projekt != null)
                {
                    return Convert.ToInt32(projekt.idpracownika);
                }
            }
            catch (Exception)
            {
                //brak kierownika
            }

            return -1;
        }

        public static List<pobintranetMVC.zakup> GetFakturyDoZaakceptowania(int? zu, int rodzaj)
        {
            if (zu == null)
            {
                return new List<pobintranetMVC.zakup>();
            }
            intranetDataContext dc = new intranetDataContext();
            List<pobintranetMVC.zakup> result = new List<pobintranetMVC.zakup>();
            switch (rodzaj)
            {
                case 1:
                    var skany = dc.skany.Where(s => ((s.zaakceptowano_rachunkowo == -1 && s.przekierowanie_r == -1) || (s.przekierowanie_r == zu && s.zaakceptowano_rachunkowo == -1)) && s.data_utworzenia >= DateTime.Parse(data_uruchomienia) && dc.zakups.Where(z => z.idzakupu == s.id_faktury).First().data_wystawienia >= DateTime.Parse(data_uruchomienia)).ToList();
                    foreach (var skan in skany)
                    {
                        if (dc.zakups.Where(z => skan.id_faktury == z.idzakupu).Any())
                        {
                            if (result.Count == 0 || !result.Where(r => r.idzakupu == skan.id_faktury).Any())
                            {
                                result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                            }
                        }
                    }
                    break;
                case 2:
                    //foreach (var skan in dc.skany)
                    //{
                    //    if (!result.Where(r => r.idzakupu == skan.id_faktury && skan.zaakceptowano_dz_zakupu == -1 && skan.zaakceptowano_rachunkowo != -1).Any())
                    //    {
                    //        result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                    //    }
                    //}
                    foreach (var skan in dc.skany.Where(s => s.zaakceptowano_rachunkowo != -1 && (s.przekierowanie_z == zu && s.zaakceptowano_dz_zakupu == -1) && s.data_utworzenia >= DateTime.Parse(data_uruchomienia) && dc.zakups.Where(z => z.idzakupu == s.id_faktury).First().data_wystawienia >= DateTime.Parse(data_uruchomienia)).ToList())
                    {
                        if (dc.zakups.Where(z => skan.id_faktury == z.idzakupu).Any())
                        {
                            if (result.Count == 0 || !result.Where(r => r.idzakupu == skan.id_faktury).Any())
                            {
                                result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                            }
                        }
                    }
                    break;
                case 3:
                    //foreach (var skan in dc.skany)
                    //{
                    //    if (!result.Where(r => r.idzakupu == skan.id_faktury && skan.zaakceptowano_kierownik == -1 && skan.zaakceptowano_dz_zakupu != -1).Any())
                    //    {
                    //        result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                    //    }
                    //}
                    foreach (var skan in dc.skany.Where(s => s.zaakceptowano_dz_zakupu != -1 && (s.przekierowanie_k == zu && s.zaakceptowano_kierownik == -1) && s.data_utworzenia >= DateTime.Parse(data_uruchomienia) && dc.zakups.Where(z => z.idzakupu == s.id_faktury).First().data_wystawienia >= DateTime.Parse(data_uruchomienia)).ToList())
                    {
                        if (dc.zakups.Where(z => skan.id_faktury == z.idzakupu).Any())
                        {
                            if (result.Count == 0 || !result.Where(r => r.idzakupu == skan.id_faktury).Any())
                            {
                                result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                            }
                        }
                    }
                    break;
                case 4:
                    //foreach (var skan in dc.skany)
                    //{
                    //    if (!result.Where(r => r.idzakupu == skan.id_faktury && skan.zaakceptowano_dyrektor == -1 && skan.zaakceptowano_kierownik != -1).Any())
                    //    {
                    //        result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                    //    }
                    //}
                    foreach (var skan in dc.skany.Where(s => s.zaakceptowano_kierownik != -1 && s.zaakceptowano_dyrektor == -1 && s.data_utworzenia >= DateTime.Parse(data_uruchomienia) && dc.zakups.Where(z => z.idzakupu == s.id_faktury).First().data_wystawienia >= DateTime.Parse(data_uruchomienia)).ToList())
                    {
                        if (dc.zakups.Where(z => skan.id_faktury == z.idzakupu).Any())
                        {
                            if (result.Count == 0 || !result.Where(r => r.idzakupu == skan.id_faktury).Any())
                            {
                                result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                            }
                        }
                    }
                    break;
                case 5:
                    //foreach (var skan in dc.skany)
                    //{
                    //    if (!result.Where(r => r.idzakupu == skan.id_faktury && skan.zaakceptowano_prezes == -1 && skan.zaakceptowano_dyrektor != -1).Any())
                    //    {
                    //        result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                    //    }
                    //}
                    foreach (var skan in dc.skany.Where(s => s.zaakceptowano_prezes == -1 && s.zaakceptowano_dyrektor != -1 && s.data_utworzenia >= DateTime.Parse(data_uruchomienia) && dc.zakups.Where(z => z.idzakupu == s.id_faktury).First().data_wystawienia >= DateTime.Parse(data_uruchomienia)).ToList())
                    {
                        if (dc.zakups.Where(z => skan.id_faktury == z.idzakupu).Any())
                        {
                            if (result.Count == 0 || !result.Where(r => r.idzakupu == skan.id_faktury).Any())
                            {
                                result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                            }
                        }
                    }
                    break;
                default:
                    //foreach (var skan in dc.skany)
                    //{
                    //    if (!result.Where(r => r.idzakupu == skan.id_faktury && skan.zaakceptowano_prezes == -1).Any())
                    //    {
                    //        result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                    //    }
                    //}
                    foreach (var skan in dc.skany.Where(s => s.zaakceptowano_prezes == -1 && s.zaakceptowano_dyrektor != -1 && s.data_utworzenia >= DateTime.Parse(data_uruchomienia) && dc.zakups.Where(z => z.idzakupu == s.id_faktury).First().data_wystawienia >= DateTime.Parse(data_uruchomienia)).ToList())
                    {
                        if (dc.zakups.Where(z => skan.id_faktury == z.idzakupu).Any())
                        {
                            if (result.Count == 0 || !result.Where(r => r.idzakupu == skan.id_faktury).Any())
                            {
                                result.Add(dc.zakups.Where(z => z.idzakupu == skan.id_faktury).FirstOrDefault());
                            }
                        }
                    }
                    break;
            }
            return result.OrderBy(z => z.data_platnosci).ToList();
        }

        public static bool SprawdzUprawnienie(int? zu, int uprawnienie)
        {
            if (zu == null)
            {
                return false;
            }
            intranetDataContext dc = new intranetDataContext();
            bool result = false;
            switch (uprawnienie)
            {
                case 1:
                    result = dc.loginy_uprawnienias.Where(lu => lu.id == zu && lu.intranet_01_marketing == 1).Any();
                    break;
                case 2:
                    result = dc.loginy_uprawnienias.Where(lu => lu.id == zu && lu.intranet_02_dt == 1).Any();
                    break;
                case 3:
                    result = dc.loginy_uprawnienias.Where(lu => lu.id == zu && lu.intranet_03_controlling == 1).Any();
                    break;
                case 4:
                    result = dc.loginy_uprawnienias.Where(lu => lu.id == zu && lu.intranet_04_de == 1).Any();
                    break;
                case 5:
                    result = dc.loginy_uprawnienias.Where(lu => lu.id == zu && lu.intranet_05_biuro == 1).Any();
                    break;
                case 6:
                    result = dc.loginy_uprawnienias.Where(lu => lu.id == zu && lu.intranet_06_dp == 1).Any();
                    break;
                case 0:
                    result = dc.loginy_uprawnienias.Where(lu => lu.id == zu && lu.admin == 1).Any();
                    break;
                default:
                    return false;
            }
            return result;
        }

        public static List<DirFile> GetListaPlikowWszystkie()
        {
            List<string> listaPlikowWszystkie = Help.GetListaPlikowWszystkie("");
            List<DirFile> dirFiles = new List<DirFile>();
            foreach (string str in listaPlikowWszystkie)
            {
                DirFile dirFile = new DirFile()
                {
                    sciezka = str,
                    nazwa = str.Split(new char[] { '\\' })[(int)str.Split(new char[] { '\\' }).Length - 1]
                };
                dirFiles.Add(dirFile);
            }
            return dirFiles;
        }

        private static List<string> GetListaPlikowWszystkie(string sDir)
        {
            int i;
            List<string> strs = new List<string>();
            if (string.IsNullOrEmpty(sDir))
            {
                sDir = string.Concat(Help.adres_serwera, Help.sciezka_skanowania);
            }
            try
            {
                string[] files = null;
                using (NetworkConnection networkConnection = new NetworkConnection(Help.adres_serwera, new NetworkCredential(Help.user, Help.pass, Help.domain)))
                {
                    files = Directory.GetFiles(sDir);
                    for (i = 0; i < (int)files.Length; i++)
                    {
                        strs.Add(files[i]);
                    }
                    files = Directory.GetDirectories(sDir);
                    for (i = 0; i < (int)files.Length; i++)
                    {
                        strs.AddRange(Help.GetListaPlikowWszystkie(files[i]));
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return strs;
        }

        public static projekt GetBudowa(int idprojektu)
        {
            intranetDataContext dc = new intranetDataContext();
            return dc.projekts.Where(p => p.idprojektu == idprojektu).FirstOrDefault();
        }

        public static List<projekt> GetWszystkieBudowy()
        {
            intranetDataContext dc = new intranetDataContext();
            return dc.projekts.Where(p => p.nazwa != "n").OrderBy(p => p.numer).OrderBy(p => p.nazwa).ToList();
        }

        public static string GetKolorZadania(string status, DateTime data_zakonczenia, DateTime? zakonczono) //do zmiany na wartości
        {
            try
            {
                int wartosc = Convert.ToInt32(status);

                if (zakonczono == null && wartosc != 100 && DateTime.Now > DateTime.Parse(data_zakonczenia.AddDays(1).ToShortDateString()))
                {
                    return "#FF6666";
                }
                else if (wartosc == 0)
                {
                    return "#96DDFF";
                }
                else if (wartosc > 0 && wartosc < 50)
                {
                    return "#9999FF";
                }
                else if (wartosc > 49 && wartosc < 100)
                {
                    return "#4646FF";
                }
                else if (wartosc == 100)
                {
                    return "#82C065";
                }
                else
                {
                    return "white";
                }
                //switch (status)
                //{
                //    case "Nowy":
                //        return "#96DDFF";
                //    case "Rozpoczęty":
                //        return "#9999FF";
                //    case "W trakcie":
                //        return "#1919FF";
                //    case "Zakończony":
                //        return "#82C065";
                //    case "Po terminie":
                //        return "red";
                //    default:
                //        return "white";
                //}
            }
            catch (Exception)
            {
                return "white";
            }


        }

        public static List<loginy_pracownik> GetLoginyEmail()
        {
            return new intranetDataContext().loginy_pracowniks.Where(p => p.pracuje == true).OrderBy(p => p.login_email).ToList();
        }

        public static string GetNazwaBudowyKontoSyntetyczne(int ids2, int ids1)
        {
            try
            {
                kontaSyntetyczne2 _kontaSyntetyczne2 = (
                    from p in (new intranetDataContext()).kontaSyntetyczne2
                    where p.ids2 == ids2 && p.ids1 == (int?)ids1
                    select p).First<kontaSyntetyczne2>();
                if (_kontaSyntetyczne2 != null)
                {
                    return _kontaSyntetyczne2.s2opis;
                }
            }
            catch (Exception)
            {
            }
            return "";
        }

        public static string GetOpisKontaSyntetycznego2(int? ids2)
        {
            try
            {
                kontaSyntetyczne2 _kontaSyntetyczne2 = (
                    from p in (new intranetDataContext()).kontaSyntetyczne2
                    where p.ids2 == ids2 
                    select p).First<kontaSyntetyczne2>();
                if (_kontaSyntetyczne2 != null)
                {
                    return _kontaSyntetyczne2.s2opis;
                }
            }
            catch (Exception)
            {
            }
            return "";
        }

        public static string GetOpisKontaAnalitycznego2(int? ida2)
        {
            try
            {
                kontaAnalityczne2 _kontaAnalityczne2 = (
                    from p in (new intranetDataContext()).kontaAnalityczne2s
                    where p.ida2 == ida2
                    select p).First<kontaAnalityczne2>();
                if (_kontaAnalityczne2 != null)
                {
                    return _kontaAnalityczne2.a2opis;
                }
            }
            catch (Exception)
            {
            }
            return "";
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static string GetNazwaBudowyKontoSyntetyczne(int ids2)
        {
            try
            {
                kontaSyntetyczne2 _kontaSyntetyczne2 = (
                    from p in (new intranetDataContext()).kontaSyntetyczne2
                    where p.ids2 == ids2
                    select p).First<kontaSyntetyczne2>();
                if (_kontaSyntetyczne2 != null)
                {
                    return _kontaSyntetyczne2.s2opis;
                }
            }
            catch (Exception)
            {
            }
            return "";
        }


        public static bool CzyZamowieniaDlaKontrahenta(int? producent)
        {
            intranetDataContext dc = new intranetDataContext();
            var lista = dc.zamowieniaDokuments.OrderByDescending(zd => zd.DataWpisu).ToList();
            try
            {
                lista = lista.Where(l => l.IdKontrahenta == producent).ToList();
                if(lista.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string GetNazwaFaktury(int? id)
        {
            intranetDataContext dc = new intranetDataContext();
            int? nullable = id;
            if ((nullable.GetValueOrDefault() == 0 ? nullable.HasValue : false))
            {
                return " ";
            }
            var _zakup = dc.zakups.Where(z => z.idzakupu == id).FirstOrDefault();
            return string.Format("{0} [{1}]", _zakup.nrdok_wlasny, _zakup.nrdok_obcy);
        }

        public static string GetNazwaMaterialu(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            try
            {
                return dc.magazyn_materialies.Where(m => m.idmaterialu == id).FirstOrDefault().numer_wlasny;
            }
            catch (Exception)
            {
                return "BŁĄD";
            }
        }

        public static string GetKodMaterialu(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            try
            {
                var mat = dc.magazyn_materialies.Where(m => m.idmaterialu == id).FirstOrDefault();
                return mat.material;
            }
            catch (Exception)
            {
                return "BŁĄD";
            }
        }

        public static double? GetSaldoZakup(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            try
            {
                return dc.rozrachuneks.Where(r => r.idzakupu == id).Sum(r => r.kwota_rozchod_pln + r.kwotaRozchodVAT);
            }
            catch (Exception)
            {
                return 0.00;
            }
        }

        public static magazyn_materialy GetMaterial(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            try
            {
                return dc.magazyn_materialies.Where(m => m.idmaterialu == id).FirstOrDefault();
            }
            catch (Exception)
            {
                return new magazyn_materialy();
            }
        }

        public static string GetNazwaKontrahenta(int? id, bool czyIndeks)
        {
            if (czyIndeks)
            {
                try
                {
                    return (
                        from k in Help.db2.kontrahents
                        where (int?)k.idkontahenta == id
                        select k).First<kontrahent>().indeks;
                }
                catch (Exception)
                {
                    return "";
                }
            }
            else
            {
                try
                {
                    return (
                        from k in Help.db2.kontrahents
                        where (int?)k.idkontahenta == id
                        select k).First<kontrahent>().nazwa;
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }

        public static string GetNipKontrahenta(int? id)
        {
            try
            {
                return (
                    from k in Help.db2.kontrahents
                    where (int?)k.idkontahenta == id
                    select k).First<kontrahent>().nip;
            }
            catch (Exception)
            {
                return "-";
            }
        }

        public static string GetNazwaProducenta(int? id)
        {
            try
            {
                return (
                    from k in Help.db2.producents
                    where (int?)k.idproducenta == id
                    select k).First<producent>().nazwa_producenta;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string GetNazwaProducentaZMaterialu(int? id)
        {
            try
            {
                int idm = Convert.ToInt32(db2.magazyn_materialies.Where(m => m.idmaterialu == id).First().producent);

                return (
                    from k in Help.db2.producents
                    where (int?)k.idproducenta == idm
                    select k).First<producent>().nazwa_producenta;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static Odziez GetOdziez(int id)
        {
            Odziez odziezs;
            try
            {
                odziezs = (Odziez)(
                    from o in Help.db.Odziez
                    where o.Id == id
                    select o);
            }
            catch (Exception)
            {
                odziezs = new Odziez()
                {
                    Id = id,
                    buty = new int?(0),
                    kurtka = new int?(0),
                    spodnie = new int?(0),
                    szelki = new int?(0),
                    kask = new int?(0),
                    inne1 = new int?(0),
                    inne2 = new int?(0),
                    inne3 = new int?(0)
                };
            }
            return odziezs;
        }

        public static string GetPlikDoPodgladu(DirFile dirFile, string sciezka, string wersja, int? zu)
        {
            if (zu == null)
            {
                zu = 0;
            }
            string str = string.Concat(sciezka, "pdf\\", dirFile.nazwa);
            using (NetworkConnection networkConnection = new NetworkConnection(Help.adres_serwera, new NetworkCredential(Help.user, Help.pass, Help.domain)))
            {
                File.Copy(dirFile.sciezka, str, true);
                networkConnection.Dispose();
            }
            return dirFile.nazwa;
        }

        public static string GetSciezka(string skan)
        {
            string str;
            try
            {
                if (Help.tempDirFileList.Count == 0)
                {
                    Help.tempDirFileList = Help.GetListaPlikowWszystkie().ToList<DirFile>();
                }
                str = (
                    from tdfl in Help.tempDirFileList
                    where tdfl.nazwa == skan
                    select tdfl).First<DirFile>().sciezka;
            }
            catch (Exception)
            {
                str = "";
            }
            return str;
        }

        public static List<skany> GetSkany(int? idFaktury)
        {
            List<skany> skany = new List<skany>();
            try
            {
                //skany = (
                //    from s in Help.db2.skany
                //    where s.id_faktury == idFaktury
                //    select s).ToList<skany>();
                intranetDataContext dc = new intranetDataContext();
                skany = dc.skany.Where(s => s.id_faktury == idFaktury).ToList();
            }
            catch (Exception)
            {
            }
            return skany;
        }

        public static List<pracownik> GetWszyscyPracownicyBudowlani()
        {
            return (
                from p in Help.db.pracownik
                where p.stanowisko != "PRACOWNIK BIUROWY"
                select p).ToList<pracownik>();
        }

        public static bool IsWorkerWorkingOnThisYear(int id, int rok)
        {
            if ((
                from ag in Help.db2.akord_godzinies
                where ag.rok == (int?)rok && ag.id_prac == (int?)id
                select ag).Any<akord_godziny>())
            {
                return true;
            }
            return false;
        }

        private static string Kurs(string date, string country, bool today)
        {
            string str = "";
            int num = 1;
            DateTime dateTime = DateTime.Parse(date);
            while (string.IsNullOrEmpty(str))
            {
                num--;
                DateTime dateTime1 = dateTime.AddDays((double)num);
                string str1 = dateTime1.ToString("yyyy-MM-dd");
                using (SqlConnection sqlConnection = new SqlConnection(Help.connectionString))
                {
                    using (SqlCommand sqlCommand = new SqlCommand("dbo.oblicz_kurs", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@TAB", SqlDbType.VarChar).Value = "a";
                        sqlCommand.Parameters.Add("@COUNTRY", SqlDbType.VarChar).Value = country;
                        if (!today)
                        {
                            sqlCommand.Parameters.Add("@DATE", SqlDbType.VarChar).Value = str1;
                        }
                        else
                        {
                            date = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");
                            sqlCommand.Parameters.Add("@DATE", SqlDbType.VarChar).Value = date;
                        }
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                        str = sqlCommand.ExecuteScalar().ToString();
                    }
                }
            }
            return str;
        }

        public static double ObliczKurs(string date, string country, bool today, string ostatniadobraData)
        {
            return double.Parse(Help.Kurs(date, country, today));
        }

        public static float RaportyFinansoweGetNettoZKasy(int year, int miesiac)
        {
            float single;
            using (SqlConnection sqlConnection = new SqlConnection(Help.connectionString))
            {
                SqlCommand sqlCommand = new SqlCommand(string.Format("SELECT TOP 100 PERCENT isnull(sum(case when isnull(KwotaVat,0)<0 then 0-isnull(KwotaVat,0) else isnull(KwotaVat,0) end),0)  as vat,  isnull(sum(case when (isnull(kwota,0)-isnull(kwotavat,0))<0 then 0-(isnull(kwota,0)-isnull(kwotavat,0)) else (isnull(kwota,0)-isnull(kwotavat,0)) end),0) as netto,  isnull(sum(case when  KWOTA<0 then 0-KWOTA else KWOTA  END),0) as KWOTA FROM dbo.kasa_ww WHERE datepart(yyyy,DataOperacji)={0} and datepart(mm,DataOperacji)={1}", year, miesiac), sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                try
                {
                    if (sqlDataReader.Read())
                    {
                        single = float.Parse(sqlDataReader[1].ToString(), CultureInfo.InvariantCulture.NumberFormat);
                        return single;
                    }
                }
                finally
                {
                    sqlDataReader.Close();
                }
                single = 0f;
            }
            return single;
        }

        public static void SortujPliki(List<DirFile> listaDoSortowania)
        {
            using (NetworkConnection networkConnection = new NetworkConnection(Help.adres_serwera, new NetworkCredential(Help.user, Help.pass, Help.domain)))
            {
                foreach (DirFile list in (
                from f in listaDoSortowania
                where f.rodzaj == "file"
                select f).ToList<DirFile>())
                {
                    string str = list.data_utworzenia.ToString("dd-MM-yyyy");
                    string str1 = string.Concat(Help.adres_serwera, Path.Combine(Help.sciezka_skanowania, str));
                    if (!Directory.Exists(str1))
                    {
                        Directory.CreateDirectory(str1);
                    }
                    File.Move(list.sciezka, Path.Combine(str1, list.nazwa));
                }
                networkConnection.Dispose();
            }
        }

        internal static bool SprawdzKontoAnalityczneDlaNiezgloszonych(string s1nr, string s2nr, string a1nr, string a2nr)
        {
            if (a1nr == "411" && a2nr == "100")
            {
                return true;
            }
            if (a1nr == "469" && a2nr == "007")
            {
                return true;
            }
            if (a1nr == "429" && a2nr == "001")
            {
                return true;
            }
            if (a1nr == "429" && a2nr == "119")
            {
                return true;
            }
            if (a1nr == "429" && a2nr == "019")
            {
                return true;
            }
            if (a1nr == "429" && a2nr == "008")
            {
                return true;
            }
            if (a1nr == "469" && a2nr == "005")
            {
                return true;
            }
            return false;
        }

        public static void WgrajPlik(HttpPostedFileBase file, int idfaktury, int kto, string mapPath)
        {
            bool flag = false;
            string fileName = file.FileName;
            string pdfPath = "";

            //grafiki
            if (fileName.Contains(".jpg") || fileName.Contains(".png"))
            {
                Document document = new Document(PageSize.A4);
                string imageName = mapPath + "images/pobintranetmvc.jpg";
                file.SaveAs(imageName);
                fileName = fileName.Replace(".jpg", "").Replace(".png", "") + ".pdf";
                string str = string.Concat(Help.adres_serwera, Path.Combine(Help.sciezka_skanowania, "wgrane", fileName));
                pdfPath = mapPath + "PDF/" + fileName;
                using (var stream = new FileStream(pdfPath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    PdfWriter.GetInstance(document, stream);
                    document.Open();
                    using (var imageStream = new FileStream(imageName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        var image = Image.GetInstance(imageStream);
                        document.Add(image);
                    }
                    document.Close();
                    if (File.Exists(str))
                    {
                        string adresSerwera = Help.adres_serwera;
                        string sciezkaSkanowania = Help.sciezka_skanowania;
                        DateTime now = DateTime.Now;
                        str = string.Concat(adresSerwera, Path.Combine(sciezkaSkanowania, "wgrane", string.Concat(now.ToString("yyyyMMdd_hhmmss_"), fileName)));
                        now = DateTime.Now;
                        fileName = string.Concat(now.ToString("yyyyMMdd_hhmmss_"), fileName);
                    }
                    using (NetworkConnection networkConnection = new NetworkConnection(Help.adres_serwera, new NetworkCredential(Help.user, Help.pass, Help.domain)))
                    {
                        System.IO.File.Copy(pdfPath, str, false);
                    }
                    flag = true;
                }
            }
            else
            {
                try
                {
                    string str = string.Concat(Help.adres_serwera, Path.Combine(Help.sciezka_skanowania, "wgrane", fileName));
                    using (NetworkConnection networkConnection = new NetworkConnection(Help.adres_serwera, new NetworkCredential(Help.user, Help.pass, Help.domain)))
                    {
                        if (File.Exists(str))
                        {
                            string adresSerwera = Help.adres_serwera;
                            string sciezkaSkanowania = Help.sciezka_skanowania;
                            DateTime now = DateTime.Now;
                            str = string.Concat(adresSerwera, Path.Combine(sciezkaSkanowania, "wgrane", string.Concat(now.ToString("yyyyMMdd_hhmmss_"), fileName)));
                            now = DateTime.Now;
                            fileName = string.Concat(now.ToString("yyyyMMdd_hhmmss_"), fileName);
                        }
                        file.SaveAs(str);
                        networkConnection.Dispose();
                    }
                    flag = true;
                }
                catch (Exception)
                {
                }
            }
            if (flag)
            {
                Help.DodajSkan(idfaktury, fileName, kto);
            }

        }

        public static string ZmianaDatyNaSQL(string data)
        {
            string str;
            try
            {
                string str1 = string.Format("{0}{1}", data[0], data[1]);
                string str2 = string.Format("{0}{1}", data[3], data[4]);
                string str3 = string.Format("{0}{1}{2}{3}", new object[] { data[6], data[7], data[8], data[9] });
                str = string.Format("{0}-{1}-{2}", str3, str2, str1);
            }
            catch (Exception)
            {
                str = data;
            }
            return str;
        }

        public static pobintranetMVC.zakup GetZakup(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            pobintranetMVC.zakup result = dc.zakups.Where(z => z.idzakupu == id).FirstOrDefault();
            return result;
        }

        public static pobintranetMVC.sprzedaz GetSprzedaz(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            pobintranetMVC.sprzedaz result = dc.sprzedazs.Where(z => z.idsprzedazy == id).FirstOrDefault();
            return result;
        }

        public static double? GetKwotaRozrachunkuZWysylki(double? w, int? idw, int idz)
        {
            if (db2.wysylki_pozycje.Where(wp => wp.idwysylki == idw && wp.idzakupu == idz).Any())
            {
                wysylki_pozycje pozycja = db2.wysylki_pozycje.Where(wp => wp.idwysylki == idw && wp.idzakupu == idz).First();
                return pozycja.kwota * -1;
            }

            return w;
        }

        public static double? GetVatRozrachunkuZWysylki(double? w, int? idw, int idz)
        {
            if (db2.wysylki_pozycje.Where(wp => wp.idwysylki == idw && wp.idzakupu == idz).Any())
            {
                wysylki_pozycje pozycja = db2.wysylki_pozycje.Where(wp => wp.idwysylki == idw && wp.idzakupu == idz).First();
                return pozycja.kwotaVAT;
            }

            return w;
        }

        public static float? ObliczMaxDlaRealizacji(int? idpozycji)
        {
            if(idpozycji != null)
            {
                intranetDataContext dc = new intranetDataContext();
                var pozycja = dc.zamowieniaPozycjes.Where(zp => zp.IdPozycji == idpozycji).First();
                return pozycja.MaterialKorekta - pozycja.Realizacja;
            }
            return 0;
        }

        //public static List<loginy_uprawnienia_view> GetUprawnieniaOsoby(int? uprawnienie)
        //{

        //    intranetDataContext dc = new intranetDataContext();
        //    switch (uprawnienie)
        //    {
        //        case 0: //podpis - dział rachunkowy
        //            return dc.loginy_uprawnienia_view.OrderBy(lu => lu.imie + " " + lu.nazwisko).Where(lu => lu.podpis_01_rachunkowy == 1).ToList();
        //        case 1: //podpis - dział zakupowy
        //            return dc.loginy_uprawnienia_view.OrderBy(lu => lu.imie + " " + lu.nazwisko).Where(lu => lu.podpis_02_zakupowy == 1).ToList();
        //        case 2: //podpis - dział kierownicz
        //            return dc.loginy_uprawnienia_view.OrderBy(lu => lu.imie + " " + lu.nazwisko).Where(lu => lu.podpis_03_kierownik == 1).ToList();
        //        case 3: //podpis - dyrektor zarządu
        //            return dc.loginy_uprawnienia_view.OrderBy(lu => lu.imie + " " + lu.nazwisko).Where(lu => lu.podpis_04_dyrektor == 1).ToList();
        //        case 4: //podpis - prezes
        //            return dc.loginy_uprawnienia_view.OrderBy(lu => lu.imie + " " + lu.nazwisko).Where(lu => lu.podpis_05_prezes == 1).ToList();
        //        default:
        //            return dc.loginy_uprawnienia_view.OrderBy(lu => lu.imie + " " + lu.nazwisko).ToList();
        //    }

        //}
    }
}