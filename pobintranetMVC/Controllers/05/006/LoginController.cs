﻿using pobintranetMVC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._05._006
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index(int? id)
        {
            intranetDataContext dc = new intranetDataContext();
            List<loginy_uprawnienia_view> listaLogin = dc.loginy_uprawnienia_views.OrderBy(l => l.nazwisko + " " + l.imie).ToList();
            loginy_uprawnienia_view luv = id == null ? dc.loginy_uprawnienia_views.ToList().First() : dc.loginy_uprawnienia_views.Where(l => l.id == id).First();
            List<pracownik> pracownicyBezUprawnien = dc.pracowniks.Where(pbu => pbu.pracuje == true).ToList();
            foreach(var l in dc.loginy_uprawnienias)
            {
                pracownicyBezUprawnien.RemoveAll(pbu => pbu.idpracownika == l.id);
            }

            ViewBag.Pracownicy = pracownicyBezUprawnien;
            ViewBag.Lista = listaLogin;
            ViewBag.Login = luv;

            return View();
        }

        public ActionResult ZapiszUprawnienie(int id_login, int w1, int w2, int w3, int w4, int w5, int w6, int admin, int o1, int o2, int o3, int o4, int o5, int o6)
        {
            intranetDataContext dc = new intranetDataContext();
            loginy_uprawnienia luv = dc.loginy_uprawnienias.Where(l => l.id == id_login).First();
            luv.intranet_01_marketing = w1;
            luv.intranet_02_dt = w2;
            luv.intranet_03_controlling = w3;
            luv.intranet_04_de = w4;
            luv.intranet_05_biuro = w5;
            luv.intranet_06_dp = w6;
            luv.admin = admin;
            luv._01_readonly = o1;
            luv._02_readonly = o2;
            luv._03_readonly = o3;
            luv._04_readonly = o4;
            luv._05_readonly = o5;
            luv._06_readonly = o6;
            dc.SubmitChanges();
            var sciezka = "/";
            if (!AppConstants.Check())
            {
                sciezka = "/aplikacjeMVC/Login/";
            }

            return Redirect(sciezka + "Index?id=" + id_login + "&msg=Zmieniono uprawnienia dla użytkownika (id: " + id_login + ")");
        }

        public ActionResult DodajUzytkownikaDoUprawnienia(int id_login)
        {
            intranetDataContext dc = new intranetDataContext();
            loginy_uprawnienia lu = new loginy_uprawnienia();
            lu.admin = 0;
            lu.id = id_login;
            lu.intranet_01_marketing = 0;
            lu.intranet_02_dt = 0;
            lu.intranet_03_controlling = 0;
            lu.intranet_04_de = 0;
            lu.intranet_05_biuro = 0;
            lu.intranet_06_dp = 0;
            lu._01_readonly = 0;
            lu._02_readonly = 0;
            lu._03_readonly = 0;
            lu._04_readonly = 0;
            lu._05_readonly = 0;
            lu._06_readonly = 0;
            dc.loginy_uprawnienias.InsertOnSubmit(lu);
            dc.SubmitChanges();

            var sciezka = "/";
            if (!AppConstants.Check())
            {
                sciezka = "/aplikacjeMVC/Login/";
            }
            return Redirect(sciezka + "Index?id=" + id_login + "&msg=Dodano użytkownika (id: " + id_login + ")");
        }
    }
}