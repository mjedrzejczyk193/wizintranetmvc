﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using pobintranetMVC.GusService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers
{
    public class KontrahentController : Controller
    {
        private List<string> lista = new List<string>();

        public KontrahentController()
        {
        }

        public string GetNazwaFromJSON(string nip)
        {
            intranetDataContext dc = new intranetDataContext();
            if(dc.kontrahents.Where(k => k.nip == nip && k.nr_kontrahenta != null).Any())
            {
                kontrahent kon = dc.kontrahents.Where(k => k.nip == nip && k.nr_kontrahenta != null).FirstOrDefault();
                var result = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", new object[] { kon.nazwa, kon.kod, kon.miasto, kon.ulica, kon.indeks, kon.idkontahenta, kon.nr_kontrahenta, kon.nr_konta, "true" });
                return result;
            }

            if (dc.kontrahents.Where(k => k.nazwa.Contains(nip) || k.indeks.Contains(nip)).Any())
            {
                kontrahent kon = dc.kontrahents.Where(k => k.nazwa.Contains(nip) || k.indeks.Contains(nip)).FirstOrDefault();
                var result = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", new object[] { kon.nazwa, kon.kod, kon.miasto, kon.ulica, kon.indeks, kon.idkontahenta, kon.nr_kontrahenta, kon.nr_konta, "true" });
                return result;
            }


            string str;
            bool flag = true;
            WSHttpBinding wSHttpBinding = new WSHttpBinding();
            wSHttpBinding.Security.Mode = SecurityMode.Transport;
            wSHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            wSHttpBinding.MessageEncoding = WSMessageEncoding.Mtom;
            UslugaBIRzewnPublClient uslugaBIRzewnPublClient = new UslugaBIRzewnPublClient(wSHttpBinding, new EndpointAddress("https://wyszukiwarkaregon.stat.gov.pl/wsBIR/UslugaBIRzewnPubl.svc"));
            string str1 = uslugaBIRzewnPublClient.Zaloguj("df7bc513ef1b45cf8b38");
            OperationContextScope operationContextScope = new OperationContextScope(uslugaBIRzewnPublClient.InnerChannel);
            HttpRequestMessageProperty httpRequestMessageProperty = new HttpRequestMessageProperty();
            httpRequestMessageProperty.Headers.Add("sid", str1);
            OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestMessageProperty;
            ParametryWyszukiwania parametryWyszukiwanium = new ParametryWyszukiwania()
            {
                Nip = nip
            };
            try
            {
                string str2 = uslugaBIRzewnPublClient.DaneSzukajPodmioty(parametryWyszukiwanium);
                if (str2 != "")
                {
                    string str3 = "";
                    string str4 = "";
                    string str5 = "";
                    string str6 = "";
                    string str7 = "";
                    string str8 = "";
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(str2))))
                    {
                        while (true)
                        {
                            string str9 = streamReader.ReadLine();
                            string str10 = str9;
                            if (str9 == null)
                            {
                                break;
                            }
                            if (str10.Contains("<Nazwa>"))
                            {
                                str3 = str10.Replace("<Nazwa>", "").Replace("</Nazwa>", "").Replace("\"", "").Trim();
                                str7 = str3;
                                str7 = str7.Replace("SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ", "Sp. z o.o.");
                                str7 = str7.Replace("SPÓŁKA JAWNA", "Sp. j.");
                                str7 = str7.Replace("SPÓŁKA KOMANDYTOWA", "Sp. k.");
                                str7 = str7.Replace("SPÓŁKA AKCYJNA", "SA");
                            }
                            if (str10.Contains("<KodPocztowy>"))
                            {
                                str4 = str10.Replace("<KodPocztowy>", "").Replace("</KodPocztowy>", "").Trim();
                            }
                            if (str10.Contains("<Ulica>"))
                            {
                                str6 = str10.Replace("<Ulica>", "").Replace("</Ulica>", "").Trim();
                            }
                            if (str10.Contains("<Miejscowosc>"))
                            {
                                str5 = str10.Replace("<Miejscowosc>", "").Replace("</Miejscowosc>", "").Trim();
                            }
                            if (str10.Contains("<NrNieruchomosci>"))
                            {
                                str8 = str10.Replace("<NrNieruchomosci>", "").Replace("</NrNieruchomosci>", "").Trim();
                            }
                        }
                    }
                    str = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", new object[] { str3, str4, str5, string.Concat(str6, " ", str8), str7, 0, "", "", "false"});
                    return str;
                }
                else
                {
                    uslugaBIRzewnPublClient.GetValue("KomunikatUslugi");
                    uslugaBIRzewnPublClient.GetValue("StatusSesji");
                    uslugaBIRzewnPublClient.GetValue("StatusUslugi");
                    uslugaBIRzewnPublClient.GetValue("KomunikatKod");
                    uslugaBIRzewnPublClient.GetValue("KomunikatTresc");
                    flag = false;
                }
            }
            catch (Exception)
            {
                flag = false;
            }
            if (flag)
            {
                return "";
            }
            //string str11 = string.Format("https://api-v3.mojepanstwo.pl/dane/krs_podmioty.json?conditions[krs_podmioty.nip]={0}", nip);
            //string str12 = "";
            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //ServicePointManager.ServerCertificateValidationCallback = (object argument0, X509Certificate argument1, X509Chain argument2, SslPolicyErrors argument3) => true;
            //using (WebClient webClient = new WebClient())
            //{
            //    str12 = webClient.DownloadString(str11);
            //}
            //JToken item = JsonConvert.DeserializeObject<JObject>(str12)["Dataobject"];
            //this.RedactInformationRecursive(item, new List<string>()
            //{
            //    "krs_podmioty.firma",
            //    "krs_podmioty.nip",
            //    "krs_podmioty.adres",
            //    "krs_podmioty.nazwa_skrocona"
            //});
            //string str13 = "";
            //try
            //{
            //    string item1 = this.lista[2];
            //    List<string> list = this.lista[1].Split(new char[] { ',' }).ToList<string>();
            //    string str14 = list[4].Replace(" kod ", "");
            //    string str15 = list[3].Replace(" miejsc. ", "");
            //    string str16 = string.Concat(list[0].Replace("ul. ", ""), list[1].Replace("nr ", ""));
            //    string item2 = this.lista[3];
            //    str13 = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}", new object[] { item1, str14, str15, str16, item2, 0, "","", "false" });
            //    str = str13;
            //}
            //catch (Exception)
            //{
            //    str = str13;
            //}
            //return str;
            return "";
        }

        public string UtworzKontrahenta(string nip, string k, string kod, string m, string u, string i)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                kontrahent kon = new kontrahent();
                kon.nr_kontrahenta = dc.numerowanie(1, null, null, null).First().nk;
                kon.kategoria = "M";
                kon.nip = nip;
                kon.nazwa = k;
                kon.indeks = i;
                kon.kod = kod;
                kon.miasto = m;
                kon.ulica = u;
                kon.nip_euro = "";
                kon.bank = "";
                kon.nr_konta = "0";
                kon.brak_nipu = '0';

                dc.kontrahents.InsertOnSubmit(kon);
                dc.SubmitChanges();

                return kon.idkontahenta.ToString();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

            return "";
        }

        public ActionResult Index()
        {
            return base.View();
        }

        private void RedactInformationRecursive(JToken check, List<string> keys = null)
        {
            if (keys == null)
            {
                keys = new List<string>()
                {
                    "krs_podmioty.firma"
                };
            }
            if (check.Children().Any<JToken>())
            {
                foreach (JToken jTokens in check.Children())
                {
                    this.RedactInformationRecursive(jTokens, keys);
                    if (jTokens.Type != JTokenType.Property)
                    {
                        continue;
                    }
                    JProperty jProperty = jTokens as JProperty;
                    if (!keys.Contains(jProperty.Name))
                    {
                        continue;
                    }
                    this.lista.Add(jProperty.Value.ToString().Replace("\"", ""));
                }
            }
        }
    }
}