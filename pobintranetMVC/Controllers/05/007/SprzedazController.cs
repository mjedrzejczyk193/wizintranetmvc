﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._05._007
{
    public class SprzedazController : Controller
    {
        // GET: Sprzedaz
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DrukujWieleFakturSprzedazowych(string i)
        {
            List<string> lista = i.Split(';').ToList();
            List<int> indeksy = new List<int>();
            foreach(var l in lista)
            {
                indeksy.Add(Convert.ToInt32(l));
            }
            ViewBag.Indeksy = indeksy;

            return View();
        }
    }
}