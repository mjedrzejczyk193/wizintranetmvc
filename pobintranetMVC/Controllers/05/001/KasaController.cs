﻿using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._05._001
{
    public class KasaController : Controller
    {
        public int ktoWprowadzil;

        public KasaController()
        {
        }

        public ActionResult ExportRozliczenieZaliczkiToIntranet()
        {
            List<ExcelRozliczenieZaliczki> item;
            intranetDataContext _intranetDataContext = new intranetDataContext();
            int num = (base.Session["kto"] != null ? Convert.ToInt32(base.Session["kto"]) : this.ktoWprowadzil);
            if (base.Session["Lista"] != null)
            {
                item = (List<ExcelRozliczenieZaliczki>)base.Session["Lista"];
            }
            else
            {
                item = null;
            }
            List<ExcelRozliczenieZaliczki> excelRozliczenieZaliczkis = item;
            if (excelRozliczenieZaliczkis == null)
            {
                ((dynamic)base.ViewBag).Error = "Nie udało się wgrac pliku";
                return base.View("UploadExcelRozliczenieZaliczki");
            }
            try
            {
                List<kasa> kasas = new List<kasa>();
                foreach (ExcelRozliczenieZaliczki excelRozliczenieZaliczki in excelRozliczenieZaliczkis)
                {
                    try
                    {
                        DateTime now = DateTime.Now;
                        kasa _kasa = new kasa()
                        {
                            IDPracownik = new int?(excelRozliczenieZaliczki.idpracownika),
                            TypOperacji = new char?('R'),
                            DataOperacji = new DateTime?(excelRozliczenieZaliczki.data),
                            OpisOperacji = excelRozliczenieZaliczki.tytul,
                            DataWpisu = new DateTime?(now),
                            CzasWpisu = new DateTime?(now),
                            Kwota = new decimal?(Convert.ToDecimal(excelRozliczenieZaliczki.kwota)),
                            Zatwierdzenie = false,
                            osobaWprowadzajaca = new int?(num),
                            dataWprowadzenia = new DateTime?(now),
                            zaliczkaZumowy = "T",
                            ids1 = new int?(1004),
                            ids2 = new int?(2084),
                            ida1 = new int?(3010),
                            ida2 = new int?(4069),
                            kontoKsiegowe = "238-0001-238-001",
                            export = new bool?(false),
                            nipPracownika = "",
                            nazwaK = "",
                            ulicaK = "",
                            kodK = "",
                            miastoK = "",
                            KwotaVat = new decimal?(new decimal()),
                            ProcentVat = new int?(100),
                            podlegaRozliczeniu = new bool?(false)
                        };
                        kasas.Add(_kasa);
                    }
                    catch (Exception)
                    {
                    }
                }
                _intranetDataContext.kasas.InsertAllOnSubmit<kasa>(kasas);
                _intranetDataContext.SubmitChanges();
            }
            catch (Exception)
            {
                ((dynamic)base.ViewBag).Error = "Nie udało się eksportować rekordów do intranetu.";
            }
            DateTime dateTime = DateTime.Now.AddDays(-1);
            string str = dateTime.ToString("dd-MM-yyyy");
            dateTime = DateTime.Now;
            string str1 = string.Format("http://int.poburski.local/aplikacje/05/001/raport_r04.asp?dataP={0}&dataK={1}&idprac=-1&konto=000-0000-000-000", str, dateTime.ToString("dd-MM-yyyy"));
            return this.Redirect(str1);
        }

        public ActionResult Index()
        {
            return base.View();
        }

        public ActionResult PreviewUploadRozliczenieZaliczki()
        {
            return base.View();
        }

        public ActionResult UploadExcelRozliczenieZaliczki()
        {
            this.WyczyscExcelCache();
            this.ktoWprowadzil = Convert.ToInt32(base.Request["kto"]);
            base.Session["Kto"] = this.ktoWprowadzil;
            return base.View();
        }

        [HttpPost]
        public ActionResult UploadRozliczenieZaliczki(HttpPostedFileBase excelFile)
        {
            base.Session["fileName"] = excelFile.FileName;
            if (excelFile == null || excelFile.ContentLength == 0)
            {
                ((dynamic)base.ViewBag).Error = "Proszę wybrać plik excel";
                return base.View("Index");
            }
            if (!excelFile.FileName.EndsWith("xls") && !excelFile.FileName.EndsWith("xlsx"))
            {
                ((dynamic)base.ViewBag).Error = "Wybrany plik nie jest plikiem excel(.xlsx , .xls)";
                return base.View("UploadExcelRozliczenieZaliczki");
            }
            string fileName = excelFile.FileName;
            try
            {
                HttpServerUtilityBase server = base.Server;
                DateTime now = DateTime.Now;
                fileName = server.MapPath(string.Concat("~/Excel/", now.ToString("ddMMyyyyThhmmss"), excelFile.FileName));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }
            excelFile.SaveAs(fileName);
            List<ExcelRozliczenieZaliczki> excelRozliczenieZaliczki = Help.GetExcelRozliczenieZaliczki(fileName, excelFile);
            ((dynamic)base.ViewBag).Lista = excelRozliczenieZaliczki;
            base.Session["Lista"] = excelRozliczenieZaliczki;
            return base.View("PreviewUploadRozliczenieZaliczki");
        }

        private void WyczyscExcelCache()
        {
            FileInfo[] files = (new DirectoryInfo(base.Server.MapPath("~/Excel/"))).GetFiles();
            for (int i = 0; i < (int)files.Length; i++)
            {
                files[i].Delete();
            }
        }
    }
}