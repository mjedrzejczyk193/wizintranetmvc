﻿using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._05._003
{
    public class ProjektController : Controller
    {
        // GET: Projekt
        public ActionResult Index()
        {


            return View();
        }

        public ActionResult DodajObiekt(int idp)
        {
            ViewBag.Obiekt = new obiekty();
            ViewBag.Idp = idp;

            return View();
        }

        public ActionResult GetWszystkieKontaktyFirmowe()
        {
            ViewBag.Kontakty = new intranetDataContext().kontaktyFirmowes.OrderBy(kf => kf.Imie + kf.Nazwisko).ToList();
            return View();
        }

        public ActionResult GetWszystkichKontrahentow()
        {
            ViewBag.Kontakty = new intranetDataContext().kontrahents.OrderBy(k => k.indeks).ToList();
            return View();
        }

        public ActionResult GetWszystkieProjekty()
        {
            ViewBag.Kontakty = new intranetDataContext().projekts.OrderBy(p => p.numer).OrderBy(p => p.nazwa).ToList();
            return View();
        }

        public ActionResult GenerujProtokoly(string projekty, string data)
        {
            if (!string.IsNullOrEmpty(projekty))
            {
                projekty = projekty.TrimStart(',');
            }
            var dc = new intranetDataContext();
            var listaId = string.IsNullOrEmpty(projekty) ? new List<string>() : projekty.Split(',').ToList();
            List<projekt> lista = new List<projekt>();
            foreach (var p in listaId)
            {
                if (!string.IsNullOrEmpty(p))
                {
                    lista.Add(dc.projekts.Where(pr => pr.idprojektu == Convert.ToInt32(p)).First());
                }
            }

            if (!string.IsNullOrEmpty(data))
            {
                var protokoly = dc.protokolies.Where(p => p.data.Value.Month == Convert.ToDateTime(data).Month && p.data.Value.Year == Convert.ToDateTime(data).Year).ToList();
                ViewBag.Protokoly = protokoly;
                foreach (var p in protokoly.Select(p => p.id_projektu).Distinct().ToList())
                {
                    lista.Add(dc.projekts.Where(projekt => projekt.idprojektu == p).First());
                }
            }

            ViewBag.Projekty = lista;

            return View();
        }

        [HttpPost]
        public ActionResult GenerujProtokoly(string projekty, string[] zamowienie, string data_p, string[] data_zam, string[] projekt, string cena_netto = "55", string stawka_vat = "23")
        {
            if (!string.IsNullOrEmpty(projekty))
            {
                projekty = projekty.TrimStart(',');
            }
            var dc = new intranetDataContext();
            var listaId = string.IsNullOrEmpty(projekty) ? new List<string>() : projekty.Split(',').ToList();
            try
            {
                if (listaId.Count == 0)
                {
                    var protokolyZMiesiaca = dc.protokolies.Where(pr => pr.data_zamowienia.Value.Month == Convert.ToDateTime(data_p).Month && pr.data_zamowienia.Value.Year == Convert.ToDateTime(data_p).Year).ToList();
                    foreach (var p in protokolyZMiesiaca)
                    {
                        listaId.Add(p.id_projektu.ToString());
                    }
                }
            }
            catch (Exception)
            {

            }
            List<projekt> lista = new List<projekt>();
            foreach (var p in listaId)
            {
                lista.Add(dc.projekts.Where(pr => pr.idprojektu == Convert.ToInt32(p)).First());
            }

            var index = 0;
            var listaProtokolow = new List<protokoly>();
            foreach (var p in projekt.ToList())
            {
                if (dc.protokolies.Where(pr => pr.data.Value.Month == Convert.ToDateTime(data_p).Month && pr.data.Value.Year == Convert.ToDateTime(data_p).Year && pr.id_projektu == Convert.ToInt32(listaId[index])).Any())
                {
                    var prot = dc.protokolies.Where(pr => pr.data.Value.Month == Convert.ToDateTime(data_p).Month && pr.data.Value.Year == Convert.ToDateTime(data_p).Year && pr.id_projektu == Convert.ToInt32(listaId[index])).First();
                    try
                    {
                        prot.cena_netto = Convert.ToDecimal(cena_netto);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            prot.cena_netto = Convert.ToDecimal(cena_netto.Replace(".", ","));
                        }
                        catch (Exception)
                        {
                            prot.cena_netto = Convert.ToDecimal(cena_netto.Replace(",", "."));
                        }
                    }
                    prot.stawka_vat = Convert.ToInt32(stawka_vat);
                    prot.data_zamowienia = Convert.ToDateTime(data_zam[index]);
                    prot.nr_zamowienia = zamowienie[index];
                    index++;
                }
                else
                {
                    protokoly prot = new protokoly();
                    try
                    {
                        prot.cena_netto = Convert.ToDecimal(cena_netto);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            prot.cena_netto = Convert.ToDecimal(cena_netto.Replace(".", ","));
                        }
                        catch (Exception)
                        {

                            prot.cena_netto = Convert.ToDecimal(cena_netto.Replace(",", "."));
                        }
                    }
                    prot.stawka_vat = Convert.ToInt32(stawka_vat);
                    prot.data_zamowienia = Convert.ToDateTime(data_zam[index]);
                    prot.nr_zamowienia = zamowienie[index];
                    prot.id_projektu = Convert.ToInt32(listaId[index]);
                    prot.data = Convert.ToDateTime(data_p);
                    prot.wygenerowano_fv = 0;
                    listaProtokolow.Add(prot);
                    index++;
                }
            }

            if (listaProtokolow.Count != 0)
            {
                dc.protokolies.InsertAllOnSubmit(listaProtokolow);
            }
            dc.SubmitChanges();

            ViewBag.Protokoly = listaProtokolow;
            ViewBag.Projekty = lista;

            return View();
        }

        public ActionResult WygenerujZbiorczoProtokoly(string projekty, string data)
        {
            var dc = new intranetDataContext();
            var listaId = string.IsNullOrEmpty(projekty) ? new List<string>() : projekty.Split(',').ToList();
            List<projekt> lista = new List<projekt>();
            foreach (var p in listaId)
            {
                try
                {
                    lista.Add(dc.projekts.Where(pr => pr.idprojektu == Convert.ToInt32(p)).First());
                }
                catch (Exception)
                {
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(data))
            {
                var protokoly = dc.protokolies.Where(p => p.data == Convert.ToDateTime(data)).ToList();
                ViewBag.Protokoly = protokoly;
                foreach (var p in protokoly.Select(p => p.id_projektu).Distinct().ToList())
                {
                    lista.Add(dc.projekts.Where(projekt => projekt.idprojektu == p).First());
                }
            }

            ViewBag.Projekty = lista;

            return View();
        }

        public ActionResult KosztySprzedazNaProjekcieZbiorczo()
        {
            intranetDataContext dc = new intranetDataContext();

            var projekty = dc.projekts.Where(p => p.p_zakonczony == 0 && p.rodzaj == 0).OrderBy(p => p.numer + p.nazwa).ToList();
            List<projekt_szczegolyResult> szczegoly = new List<projekt_szczegolyResult>();
            List<double> netto = new List<double>();
            List<double> vat = new List<double>();
            List<double> sprzedaz = new List<double>();
            List<projekt> listaProjektow = new List<projekt>();
            foreach (var p in projekty)
            {
                if (p.idprojektu == 12622) // [2328] KL GDYNIA
                {
                    var x = 0;
                }
                try
                {
                    var ids2 = dc.kontaSyntetyczne2.Where(ks => ks.idProjektu == p.idprojektu).First().ids2;
                    var koszty = dc.zakup_pozycjes.Where(zp => zp.ids2 == ids2).Select(zp => zp.idzakupu).Distinct().ToList();
                    double koszt_poza_netto = 0; double koszt_poza_vat = 0;
                    foreach (var idzakupu in koszty)
                    {
                        if (dc.zakup_pozycjes.Where(zp => zp.idzakupu == idzakupu).ToList().Count > 1)
                        {
                            if (dc.zakup_pozycjes.Where(zp => zp.ids2 != ids2 && zp.idzakupu == idzakupu).Sum(zp => zp.netto) != 0)
                            {
                                koszt_poza_netto += dc.zakup_pozycjes.Where(zp => zp.ids2 != ids2 && zp.idzakupu == idzakupu).Sum(zp => zp.netto);
                                koszt_poza_vat += dc.zakup_pozycjes.Where(zp => zp.ids2 != ids2 && zp.idzakupu == idzakupu).Sum(zp => zp.vat);
                            }
                        }
                    }
                    netto.Add(koszt_poza_netto);
                    vat.Add(koszt_poza_vat);
                    try
                    {
                        sprzedaz.Add(dc.sprzedazs.Where(s => s.idprojektu == p.idprojektu && p.idkontrahenta == s.idkontrahenta).Sum(s => Convert.ToDouble(s.suma_netto_pln)));
                    }
                    catch (Exception)
                    {
                        sprzedaz.Add(-1);
                    }
                    szczegoly.Add(dc.projekt_szczegoly(p.idprojektu).First());
                    listaProjektow.Add(p);
                }
                catch (Exception)
                {
                    continue;
                }
            }
            ViewBag.Szczegoly = szczegoly;
            ViewBag.Projekty = listaProjektow;
            ViewBag.NettoPoza = netto;
            ViewBag.VatPoza = vat;
            ViewBag.Sprzedaz = sprzedaz;

            return View();
        }

        public void StworzFVzbiorczo(string data)
        {
            var dc = new intranetDataContext();
            var datetime = Convert.ToDateTime(data);
            List<projekt> lista = new List<projekt>();
            List<protokoly> protokoly = new List<protokoly>();
            if (!string.IsNullOrEmpty(data))
            {
                protokoly = dc.protokolies.Where(p => p.data == Convert.ToDateTime(data)).ToList();
                foreach (var p in protokoly.Select(p => p.id_projektu).Distinct().ToList())
                {
                    lista.Add(dc.projekts.Where(projekt => projekt.idprojektu == p).First());
                }
            }
            List<sprzedaz> listaSprzedazy = new List<sprzedaz>();
            var firma = dc.parametry_firmas.ToList()[0];
            foreach (var protokol in protokoly)
            {
                if (/*protokol.wygenerowano_fv == 0 && */!string.IsNullOrEmpty(protokol.nr_zamowienia.Replace(" ", "")))
                {
                    var p = dc.projekts.Where(proj => proj.idprojektu == protokol.id_projektu).First();
                    sprzedaz s = new sprzedaz();
                    s.nrdok_wlasny = dc.nr_dokumentu_sprzedazowego(protokol.data.ToString());
                    s.data_wystawienia = protokol.data;
                    s.datasprzedazy = protokol.data;
                    s.miejscewystawienia = firma.miejscowosc;
                    s.spid = 1;
                    s.spnazwa = firma.nazwa;
                    s.spnazwaskrocona = "";
                    s.spadres = firma.ulica + " " + firma.nr;
                    s.spkod = firma.kod;
                    s.spmiejscowosc = firma.miejscowosc;
                    s.spNIP = firma.nip;
                    s.sptel = firma.tel1;
                    s.spfax = firma.tel2;
                    s.idkontrahenta = 81432;
                    s.nbnumer = "00016";
                    s.nbnazwa = "KAUFLAND POLSKA MARKETY SPÓLKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ SP. J.";
                    s.nbnazwaskrocona = "KAUFLAND";
                    s.nbadres = "AL. ARMII KRAJOWEJ 47";
                    s.nbkod = "50-541";
                    s.nbmiejscowosc = "WROCŁAW";
                    s.nbNIP = "8992367273";
                    s.nbtel = "";
                    s.nbfax = "";
                    s.odnazwa = "";
                    s.odnazwaskrocona = "";
                    s.odadres = "";
                    s.odkod = "";
                    s.odmiejscowosc = "";
                    s.odNIP = "";
                    s.odtel = "";
                    s.odfax = "";
                    s.formazaplaty = "PRZELEW";
                    s.iloscdni = 30;
                    s.data_platnosci = protokol.data.Value.AddDays(30);
                    s.Bank = "ING   ODDZIAŁ W OPOLU";
                    s.Nrkonta = "34105015041000009031879563";
                    s.uwagi = "";
                    s.idprojektu = 12531;
                    s.nazwa1 = string.Format("USŁUGA EKSPLOATACJI INSTALACJI AUTOMATYKI NA KL {0} {1} {2} NR ZAMÓWIENIA {3} ZA MIESIĄC {4}.{5}", p.miejscowosc, p.ulica, p.opis, protokol.nr_zamowienia, datetime.Month.ToString("00"), datetime.Year);
                    s.jednostkamiary1 = "SZT.";
                    s.ilosc1 = 1;
                    s.cenanetto1 = Convert.ToDouble(protokol.cena_netto);
                    s.stawkavat1 = Convert.ToInt32(protokol.stawka_vat) + "%";

                    var procentVat = Convert.ToDouble(protokol.stawka_vat) / 100;
                    s.kwotavat1 = Math.Round(Convert.ToDouble(protokol.cena_netto)*procentVat, 2);

                    if (p.opis.Contains("9495")) //jeżeli KAUFLAND CENTRALA
                    {
                        s.cenanetto1 = 500;
                        s.kwotavat1 = 115;
                    }
                    s.wartoscnetto1 = s.cenanetto1 * s.ilosc1;
                    s.wartoscbrutto1 = s.wartoscnetto1 + s.kwotavat1;
                    s.nazwa2 = "";
                    s.jednostkamiary2 = "SZT.";
                    s.ilosc2 = 0;
                    s.cenanetto2 = 0;
                    s.stawkavat2 = "23%";
                    s.wartoscnetto2 = 0;
                    s.kwotavat2 = 0;
                    s.wartoscbrutto2 = 0;
                    s.nazwa3 = "";
                    s.jednostkamiary3 = "SZT.";
                    s.ilosc3 = 0;
                    s.cenanetto3 = 0;
                    s.stawkavat3 = "23%";
                    s.wartoscnetto3 = 0;
                    s.kwotavat3 = 0;
                    s.wartoscbrutto3 = 0;
                    s.nazwa4 = "";
                    s.jednostkamiary4 = "SZT.";
                    s.ilosc4 = 0;
                    s.cenanetto4 = 0;
                    s.stawkavat4 = "23%";
                    s.wartoscnetto4 = 0;
                    s.kwotavat4 = 0;
                    s.wartoscbrutto4 = 0;
                    s.suma_netto_pln = s.wartoscnetto1;
                    s.suma_vat_pln = s.kwotavat1;
                    s.suma_brutto_pln = s.wartoscnetto1 + s.kwotavat1;
                    s.zatrzymanieczasowe = 0;
                    s.zatrzymaniebankowe = 0;
                    s.dozaplatyn = s.suma_netto_pln;
                    s.dozaplatyv = s.suma_vat_pln;
                    s.dozaplatyb = s.suma_brutto_pln;
                    s.stawkavat5 = "23%";
                    s.zalnetto = 0;
                    s.zalvat = 0;
                    s.zalbrutto = 0;
                    s.anulowano = 'N';
                    s.rodzaj = "SPU";
                    s.export = false;

                    listaSprzedazy.Add(s);
                    dc.sprzedazs.InsertOnSubmit(s);
                    protokol.wygenerowano_fv = 1;
                    dc.SubmitChanges();
                    //var parametry = dc.parametries.OrderByDescending(pa => pa.idparametru).First();
                    //parametry.nr_sprzedazy = parametry.nr_sprzedazy + 1;

                    //dc.SubmitChanges();
                    //UPDATE parametry SET nr_sprzedazy = nr_sprzedazy + 1

                    Help.NrSprzedazyPlus();
                }
            }
            //dc.sprzedazs.InsertAllOnSubmit(listaSprzedazy);
            //dc.SubmitChanges();

            Response.Redirect("/aplikacje/05/007/koszty_przeglad_mpp.asp?opcja=C&szukaj=kaufland&szk1=SZUKAJ&konto=&rok1=&rok2=&wydruk1=&idsprzedazy=&numer=&pelnaNazwa=&sortuj=D");
        }

        [HttpPost]
        public void DodajObiekt(int projekt, string miasto, string ulica, string kod, string dojazd, int osoba, int idp)
        {
            obiekty o = new obiekty();
            o.nazwa = Help.GetBudowa(projekt).nazwa;
            o.idserwisu = idp;
            o.idprojektu = projekt;
            o.miasto = miasto;
            o.ulica = ulica;
            o.kod = kod;
            o.dojazd = Convert.ToDouble(dojazd);
            o.osoba_odpowiedzialna_id = osoba;

            intranetDataContext dc = new intranetDataContext();
            dc.obiekties.InsertOnSubmit(o);
            dc.SubmitChanges();

            Response.Redirect(string.Format("/aplikacje/05/003/projekt_edycja.asp?projekt=&pobierz=pobierz&kontrahent=&idproj={0}&projnumer={1}", idp, Help.GetProjekt(idp).numer));
        }

        public int DodajOsobe(string i, string n, string e, int k, string t)
        {
            kontaktyFirmowe kontakt = new kontaktyFirmowe();
            kontakt.Imie = i;
            kontakt.Nazwisko = n;
            kontakt.Email = e;
            kontakt.Firma = Help.GetNazwaKontrahenta(k, true);
            kontakt.telefon = t;

            intranetDataContext dc = new intranetDataContext();
            dc.kontaktyFirmowes.InsertOnSubmit(kontakt);
            dc.SubmitChanges();

            return kontakt.id;
        }
    }
}