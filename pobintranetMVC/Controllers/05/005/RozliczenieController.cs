﻿using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._05._005
{
    public class RozliczenieController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Zaplac()
        {
            base.Session["listaDodanych"] = null;
            if (!string.IsNullOrEmpty(base.Request["zakupy"]))
            {
                intranetDataContext _intranetDataContext = new intranetDataContext();
                int num = int.Parse(base.Request["idkontrahenta"]);
                List<string> list = base.Request["zakupy"].ToString().Split(new char[] { ',' }).ToList<string>();
                List<rozrachunki_z> rozrachunkiZs = new List<rozrachunki_z>();
                foreach (string str in list)
                {
                    try
                    {
                        rozrachunki_z rozrachunkiZ = (
                            from z in _intranetDataContext.rozrachunki_zs
                            where z.idzakupu == Convert.ToInt32(str)
                            select z).First<rozrachunki_z>();
                        rozrachunkiZs.Add(rozrachunkiZ);
                    }
                    catch (Exception)
                    {

                    }
                }
                ((dynamic)base.ViewBag).Id = num;
                ((dynamic)base.ViewBag).Lista = rozrachunkiZs;
            }
            else
            {
                HttpResponseBase response = base.Response;
                DateTime now = DateTime.Now;
                response.Redirect(string.Format("/aplikacje/05/005/lista_sald.asp?ujemne=P&dodatnie=P&rok={0}", now.Year));
            }
            return base.View();
        }

        public ActionResult ZaplacSprzedaz()
        {
            base.Session["listaDodanychSprzedaz"] = null;
            if (!string.IsNullOrEmpty(base.Request["zakupy"]))
            {
                intranetDataContext _intranetDataContext = new intranetDataContext();
                int num = int.Parse(base.Request["idkontrahenta"]);
                List<string> list = base.Request["zakupy"].ToString().Split(new char[] { ',' }).ToList<string>();
                List<rozrachunki_> rozrachunkiSs = new List<rozrachunki_>();
                foreach (string str in list)
                {
                    try
                    {
                        rozrachunki_ rozrachunkiS = (
                            from s in _intranetDataContext.rozrachunki_s
                            where s.idsprzedazy == Convert.ToInt32(str)
                            select s).First<rozrachunki_>();
                        rozrachunkiSs.Add(rozrachunkiS);
                    }
                    catch (Exception)
                    {

                    }
                }
                ((dynamic)base.ViewBag).Id = num;
                ((dynamic)base.ViewBag).Lista = rozrachunkiSs;
            }
            else
            {
                HttpResponseBase response = base.Response;
                DateTime now = DateTime.Now;
                response.Redirect(string.Format("/aplikacje/05/005/lista_sald.asp?ujemne=P&dodatnie=P&rok={0}", now.Year));
            }
            return base.View();
        }

        [HttpPost]
        public ActionResult ZaplacFaktury(FormCollection request)
        {
            int num = Convert.ToInt32(request["idkontrahenta"]);
            int num1 = Convert.ToInt32(request["ilosc"]);
            List<RozliczenieController.ZakupRozliczenie> zakupRozliczenies = new List<RozliczenieController.ZakupRozliczenie>();
            for (int i = 0; i < num1; i++)
            {
                RozliczenieController.ZakupRozliczenie zakupRozliczenie = new RozliczenieController.ZakupRozliczenie()
                {
                    data = Help.ZmianaDatyNaSQL(request[string.Concat("zakup[", i, "].data_platnosci")]),
                    idzakupu = Convert.ToInt32(request[string.Concat("zakup[", i, "].idzakupu")])
                };
                try
                {
                    zakupRozliczenie.kwota = Convert.ToDouble(request[string.Concat("zakup[", i, "].zaplacono")].Replace(".", ","));
                }
                catch (Exception)
                {
                    zakupRozliczenie.kwota = Convert.ToDouble(request[string.Concat("zakup[", i, "].zaplacono")]);
                }
                try
                {
                    zakupRozliczenie.vat = Convert.ToDouble(request[string.Concat("zakup[", i, "].vat")].Replace(".", ","));
                }
                catch (Exception)
                {
                    zakupRozliczenie.vat = Convert.ToDouble(request[string.Concat("zakup[", i, "].vat")]);
                }
                zakupRozliczenies.Add(zakupRozliczenie);
            }
            List<int> nums = new List<int>();
            if (base.Session["listaDodanych"] != null)
            {
                nums = (List<int>)base.Session["listaDodanych"];
            }
            intranetDataContext _intranetDataContext = new intranetDataContext();
            List<rozrachunek> rozrachuneks = new List<rozrachunek>();
            foreach (RozliczenieController.ZakupRozliczenie zakupRozliczenie1 in zakupRozliczenies.Distinct<RozliczenieController.ZakupRozliczenie>())
            {
                if ((
                    from ld in nums
                    where ld == zakupRozliczenie1.idzakupu
                    select ld).Any<int>())
                {
                    continue;
                }
                if ((
                    from r in _intranetDataContext.rozrachuneks
                    where (r.data_operacji == (DateTime?)DateTime.Parse(zakupRozliczenie1.data)) && r.kwota_rozchod_pln == (double?)zakupRozliczenie1.kwota && r.idzakupu == (int?)zakupRozliczenie1.idzakupu
                    select r).Any<rozrachunek>() || zakupRozliczenie1.kwota <= 0)
                {
                    continue;
                }
                rozrachunek _rozrachunek = new rozrachunek()
                {
                    idkontrahenta = num == 0 ? Help.GetZakup(zakupRozliczenie1.idzakupu).idkontrahenta : num,
                    data_operacji = new DateTime?(DateTime.Parse(zakupRozliczenie1.data)),
                    idzakupu = new int?(zakupRozliczenie1.idzakupu),
                    kwota_rozchod_pln = new double?(zakupRozliczenie1.kwota),
                    czas_wpisu = new DateTime?(DateTime.Now),
                    kwota_przychod_pln = new double?(0),
                    kwotaPrzychodFBZ = new double?(0),
                    kwotaPrzychodZCZ = new double?(0),
                    kwotaPrzychodZGW = new double?(0),
                    kwotaRozchodFBZ = new double?(zakupRozliczenie1.kwota),
                    kwotaRozchodZCZ = new double?(0),
                    kwotaRozchodZGW = new double?(0),
                    kwotaRozchodVAT = zakupRozliczenie1.vat,
                    opis = "Zapłacono wiele " + DateTime.Now.ToString("dd-MM-yyyy hh:mm")
                };
                rozrachuneks.Add(_rozrachunek);
                nums.Add(zakupRozliczenie1.idzakupu);
            }
            base.Session["listaDodanych"] = nums;
            _intranetDataContext.rozrachuneks.InsertAllOnSubmit<rozrachunek>(rozrachuneks);
            _intranetDataContext.SubmitChanges();
            _intranetDataContext.popraw_kontrahenta_rozrachunki();
            return this.Redirect(string.Format("/aplikacje/05/005/lista_sald.asp?ujemne=P&dodatnie=P", num));
        }

        [HttpPost]
        public ActionResult ZaplacFakturySprzedaz(FormCollection request)
        {
            int num = Convert.ToInt32(request["idkontrahenta"]);
            int num1 = Convert.ToInt32(request["ilosc"]);
            List<RozliczenieController.SprzedazRozliczenie> zakupRozliczenies = new List<RozliczenieController.SprzedazRozliczenie>();
            for (int i = 0; i < num1; i++)
            {
                RozliczenieController.SprzedazRozliczenie zakupRozliczenie = new RozliczenieController.SprzedazRozliczenie()
                {
                    data = Help.ZmianaDatyNaSQL(request[string.Concat("zakup[", i, "].data_platnosci")]),
                    idsprzedazy = Convert.ToInt32(request[string.Concat("zakup[", i, "].idsprzedazy")])
                };
                try
                {
                    zakupRozliczenie.kwota = Convert.ToDouble(request[string.Concat("zakup[", i, "].zaplacono")].Replace(".", ","));
                }
                catch (Exception)
                {
                    zakupRozliczenie.kwota = Convert.ToDouble(request[string.Concat("zakup[", i, "].zaplacono")]);
                }
                try
                {
                    zakupRozliczenie.vat = Convert.ToDouble(request[string.Concat("zakup[", i, "].vat")].Replace(".", ","));
                }
                catch (Exception)
                {
                    zakupRozliczenie.vat = Convert.ToDouble(request[string.Concat("zakup[", i, "].vat")]);
                }
                zakupRozliczenies.Add(zakupRozliczenie);
            }
            List<int> nums = new List<int>();
            if (base.Session["listaDodanych"] != null)
            {
                nums = (List<int>)base.Session["listaDodanych"];
            }
            intranetDataContext _intranetDataContext = new intranetDataContext();
            List<rozrachunek> rozrachuneks = new List<rozrachunek>();
            foreach (RozliczenieController.SprzedazRozliczenie zakupRozliczenie1 in zakupRozliczenies.Distinct<RozliczenieController.SprzedazRozliczenie>())
            {
                if ((
                    from ld in nums
                    where ld == zakupRozliczenie1.idsprzedazy
                    select ld).Any<int>())
                {
                    continue;
                }
                if ((
                    from r in _intranetDataContext.rozrachuneks
                    where (r.data_operacji == (DateTime?)DateTime.Parse(zakupRozliczenie1.data)) && r.kwota_rozchod_pln == (double?)zakupRozliczenie1.kwota && r.idzakupu == (int?)zakupRozliczenie1.idsprzedazy
                    select r).Any<rozrachunek>() || zakupRozliczenie1.kwota <= 0)
                {
                    continue;
                }
                rozrachunek _rozrachunek = new rozrachunek()
                {
                    idkontrahenta = num == 0 ? Help.GetSprzedaz(zakupRozliczenie1.idsprzedazy).idkontrahenta : num,
                    data_operacji = new DateTime?(DateTime.Parse(zakupRozliczenie1.data)),
                    idsprzedazy = new int?(zakupRozliczenie1.idsprzedazy),
                    kwota_rozchod_pln = new double?(zakupRozliczenie1.kwota),
                    czas_wpisu = new DateTime?(DateTime.Now),
                    kwota_przychod_pln = new double?(0),
                    kwotaRozchodFBZ = new double?(0),
                    kwotaPrzychodZCZ = new double?(0),
                    kwotaPrzychodZGW = new double?(0),
                    kwotaPrzychodFBZ = new double?(zakupRozliczenie1.kwota),
                    kwotaRozchodZCZ = new double?(0),
                    kwotaRozchodZGW = new double?(0),
                    kwotaPrzychodVAT = zakupRozliczenie1.vat,
                    opis = "Zapłacono wiele " + DateTime.Now.ToString("dd-MM-yyyy hh:mm")
                };
                rozrachuneks.Add(_rozrachunek);
                nums.Add(zakupRozliczenie1.idsprzedazy);
            }
            base.Session["listaDodanych"] = nums;
            _intranetDataContext.rozrachuneks.InsertAllOnSubmit<rozrachunek>(rozrachuneks);
            _intranetDataContext.SubmitChanges();
            _intranetDataContext.popraw_kontrahenta_rozrachunki();
            return this.Redirect(string.Format("/aplikacje/05/005/lista_sald.asp?ujemne=P&dodatnie=P", num));
        }

        public class ZakupRozliczenie
        {
            public string data
            {
                get;
                set;
            }

            public int idzakupu
            {
                get;
                set;
            }

            public double kwota
            {
                get;
                set;
            }

            public double vat
            {
                get;
                set;
            }

            public ZakupRozliczenie()
            {
                this.idzakupu = 0;
                this.data = DateTime.Now.ToShortDateString();
                this.kwota = 0;
                this.vat = 0;
            }
        }

        public class SprzedazRozliczenie
        {
            public string data
            {
                get;
                set;
            }

            public int idsprzedazy
            {
                get;
                set;
            }

            public double kwota
            {
                get;
                set;
            }

            public double vat
            {
                get;
                set;
            }

            public SprzedazRozliczenie()
            {
                this.idsprzedazy = 0;
                this.data = DateTime.Now.ToShortDateString();
                this.kwota = 0;
                this.vat = 0;
            }
        }
    }
}