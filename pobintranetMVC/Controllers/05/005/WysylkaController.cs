﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace pobintranetMVC.Controllers._05._005
{
    public class WysylkaController : Controller
    {
        // GET: Wysylka
        public ActionResult Index()
        {
            return View();
        }

        //przykladowe dane
        //https://wl-api.mf.gov.pl/api/search/nips/7542751410,7542617752?date=2020-05-26
        //{"result":{"subjects":[{"name":"'POBURSKI DACHTECHNIK' SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ","nip":"7542617752","statusVat":"Czynny","regon":"531583944","pesel":null,"krs":"0000232584","residenceAddress":null,"workingAddress":"JÓZEFA CYGANA 4, 45-131 OPOLE","representatives":[],"authorizedClerks":[],"partners":[],"registrationLegalDate":"1999-11-16","registrationDenialBasis":null,"registrationDenialDate":null,"restorationBasis":null,"restorationDate":null,"removalBasis":null,"removalDate":null,"accountNumbers":["04219000023000004627670101","06105015751000009031298418","06114017880000332043001019","07105015751000009031298400","07114017880000332043001001","14161011752015018000480004","17114017880000332043001015","20219000023000004627670201","30193011902220027069330002","47219000023000004627670103","49114017880000332043001021","57193011902220027069330001","66114017880000332043001006","74219000023000004627670102","76114017880000332043001020","77114017880000332043001002","79102036680000560203949161","81105015751000009031298426","95161011752015018000480001","98156000132008917520000001"],"hasVirtualAccounts":false},{"name":"\"WIZUALIZACJA\" SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ","nip":"7542751410","statusVat":"Czynny","regon":"532410039","pesel":null,"krs":"0000175520","residenceAddress":null,"workingAddress":"JÓZEFA CYGANA 4/315, 45-131 OPOLE","representatives":[],"authorizedClerks":[],"partners":[],"registrationLegalDate":"2003-10-02","registrationDenialBasis":null,"registrationDenialDate":null,"restorationBasis":null,"restorationDate":null,"removalBasis":null,"removalDate":null,"accountNumbers":["46109021380000000118915534","34105015041000009031879563"],"hasVirtualAccounts":false}],"requestDateTime":"26-05-2020 10:25:57","requestId":"16797-87hk4gl"}}
        public void BialaListaPodatnikowVAT(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            List<zakup> zakupy = dc.zakups.Where(z => z.idwysylki == id).ToList();
            string url = "https://wl-api.mf.gov.pl/api/search/nips/";
            var now = DateTime.Now;
            string date = now.ToString("yyyy-MM-dd");
            int year = now.Year;
            //dodawanie nipów
            List<int?> kontrahenci = zakupy.Select(z => z.idkontrahenta).Distinct().ToList();
            List<NipKontrahent> nipy = new List<NipKontrahent>();
            foreach (int kontrahent in kontrahenci)
            {
                var kon = dc.kontrahents.Where(k => k.idkontahenta == Convert.ToInt32(kontrahent)).First();
                NipKontrahent nk = new NipKontrahent();
                nk.nip = kon.nip;
                nk.id = kon.idkontahenta;
                if (!nipy.Contains(nk) && !nk.nip.Contains("DE") && !nk.nip.Contains("PL"))
                {
                    nipy.Add(nk);
                }
            }
            int i = 0;
            bool sprawdzenie = false;
            foreach (NipKontrahent nk in nipy)
            {
                i++;
                if (i < 29)
                {
                    url += nk.nip + ",";
                }
                else
                {
                    i = 0;
                    sprawdzenie = SprawdzBialaListe(url, date, nipy);
                    url = "https://wl-api.mf.gov.pl/api/search/nips/";
                }
            }
            if (url != "https://wl-api.mf.gov.pl/api/search/nips/")
            {
                sprawdzenie = SprawdzBialaListe(url, date, nipy);
            }

            if (sprawdzenie)
            {
                Response.Redirect(string.Format("/aplikacje/05/005/wysylka.asp?idwysylka={0}&rok={1}&message={2}", id, year, "ok"));
            }
            else
            {
                Response.Redirect(string.Format("/aplikacje/05/005/wysylka.asp?idwysylka={0}&rok={1}&message={2}", id, year, "no"));
            }

        }

        private bool SprawdzBialaListe(string url, string date, List<NipKontrahent> nipy)
        {
            intranetDataContext dc = new intranetDataContext();
            url = url.Remove(url.Length - 1, 1);

            url += "?date=" + date;
            string result = "";
            using (WebClient client = new WebClient())
            {
                try
                {
                    result = client.DownloadString(url);

                }
                catch (Exception)
                {
                    try
                    {
                        url = "https://api.yourlovecode.com/Wysylka/PobierzWysylke?url=" + url;
                        result = client.DownloadString(url);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                try
                {
                    var r = (JObject)JsonConvert.DeserializeObject(result);
                    foreach (JObject s in r["result"]["entries"])
                    {
                        JToken subject = null;
                        try
                        {
                            subject = s["subjects"][0];
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.Message);
                            continue;
                        }

                        biala_lista_podatnikow_vat blpv = new biala_lista_podatnikow_vat();
                        try
                        {
                            blpv.nip = Convert.ToInt32(subject["nip"]).ToString();
                        }
                        catch (Exception)
                        {
                            blpv.nip = s["identifier"].ToString();
                        }
                        blpv.idkontrahenta = nipy.Where(n => n.nip == blpv.nip).FirstOrDefault().id;
                        blpv.statusVat = subject["statusVat"].ToString() == "Czynny" ? 1 : 0;
                        blpv.data_sprawdzenia = date;
                        List<string> konta = new List<string>();
                        string kontaStr = "";
                        foreach (string konto in subject["accountNumbers"])
                        {
                            konta.Add(konto);
                            kontaStr += konto + ",";
                        }
                        kontaStr = kontaStr.Remove(kontaStr.Length - 1, 1);

                        blpv.konta = kontaStr;
                        blpv.ok = konta.Contains(dc.kontrahents.Where(k => k.idkontahenta == blpv.idkontrahenta).FirstOrDefault().nr_konta) ? 1 : 0;

                        dc.biala_lista_podatnikow_vat.DeleteAllOnSubmit(dc.biala_lista_podatnikow_vat.Where(b => b.idkontrahenta == blpv.idkontrahenta));
                        dc.SubmitChanges();

                        dc.biala_lista_podatnikow_vat.InsertOnSubmit(blpv);
                        dc.SubmitChanges();
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    return false;
                }
                return true;
            }
        }

        public ActionResult doWysylki(int idkotrahenta, string zakupy, int? pominIStniejace)
        {
            List<string> listaZakupow = zakupy.Split(',').ToList();
            intranetDataContext dc = new intranetDataContext();
            List<wysylki> listaWysylek = dc.wysylki.ToList();
            List<string> listaZakupowZWysylka = new List<string>();

            string message = "Wszystkie pozycje nie są przypisane do innych wysyłek.";
            int pomin = pominIStniejace != null ? Convert.ToInt32(pominIStniejace) : 0;
            if (pomin == 0)
            {
                foreach (var idz in listaZakupow)
                {
                    if (string.IsNullOrEmpty(dc.zakups.Where(z => z.idzakupu == Convert.ToInt32(idz)).FirstOrDefault().idwysylki.ToString()))
                    {
                        //Już ma wysyłkę
                        listaZakupowZWysylka.Add(idz);
                        listaZakupow.RemoveAll(r => r == idz);
                    }
                }
                if (listaZakupowZWysylka.Count > 0)
                {
                    message = "Niektóre pozycje już są zapisane do wysyłki i zostaną pominięte: ";
                    foreach (var id in listaZakupowZWysylka)
                    {
                        zakup zakup = dc.zakups.Where(z => z.idzakupu == Convert.ToInt32(id)).FirstOrDefault();
                        string wysylka = dc.wysylki.Where(w => w.idwysylka == Convert.ToInt32(zakup.idwysylki)).FirstOrDefault().numer;
                        message += string.Format("{0}Faktura: {1} w wysyłce {2}", Environment.NewLine, zakup.nrdok_obcy, wysylka);
                    }
                }
            }
            else
            {
                message = "Pobrano wszystkie wysyłki";
            }
            ViewBag.ListaZakupow = listaZakupow;
            ViewBag.Message = message;
            ViewBag.ListaWysylek = listaWysylek.OrderByDescending(w => w.na_kiedy).ThenByDescending(w => w.numer).ToList();
            ViewBag.idkontrahenta = idkotrahenta;

            return View();
        }

        public void ZmienSaldo(int w, int z, string s, string v)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                if (dc.wysylki_pozycje.Where(oldw => oldw.idzakupu == z && oldw.idwysylki == w).Any())
                {
                    dc.wysylki_pozycje.DeleteOnSubmit(dc.wysylki_pozycje.Where(oldw => oldw.idzakupu == z && oldw.idwysylki == w).FirstOrDefault());
                    dc.SubmitChanges();
                }

                if (string.IsNullOrEmpty(v.ToString()))
                {
                    v = "0";
                }

                wysylki_pozycje wp = new wysylki_pozycje();
                wp.idwysylki = w;
                wp.idzakupu = z;
                wp.kwota = Math.Round(float.Parse(s) * -1, 2);
                wp.kwotaVAT = Math.Round(float.Parse(v), 2);
                dc.wysylki_pozycje.InsertOnSubmit(wp);
                dc.SubmitChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult doWysylkiZapisz(int wysylka, string zakupy)
        {
            List<string> listaZakupow = zakupy.Remove(zakupy.Length - 1).Split(',').ToList();
            intranetDataContext dc = new intranetDataContext();
            foreach (var lz in listaZakupow)
            {
                var zakup = dc.zakups.Where(z => z.idzakupu == Convert.ToInt32(lz)).FirstOrDefault();
                zakup.idwysylki = wysylka;
                dc.SubmitChanges();
            }
            return View();
        }

        public partial class Result
        {
            [JsonProperty("subjects")]
            public Subject[] Subjects { get; set; }

            [JsonProperty("requestDateTime")]
            public string RequestDateTime { get; set; }

            [JsonProperty("requestId")]
            public string RequestId { get; set; }
        }

        public partial class Subject
        {
            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("nip")]
            public string Nip { get; set; }

            [JsonProperty("statusVat")]
            public string StatusVat { get; set; }

            [JsonProperty("pesel")]
            public object Pesel { get; set; }

            [JsonProperty("krs")]
            public string Krs { get; set; }

            [JsonProperty("residenceAddress")]
            public object ResidenceAddress { get; set; }

            [JsonProperty("workingAddress")]
            public string WorkingAddress { get; set; }

            [JsonProperty("representatives")]
            public object[] Representatives { get; set; }

            [JsonProperty("authorizedClerks")]
            public object[] AuthorizedClerks { get; set; }

            [JsonProperty("partners")]
            public object[] Partners { get; set; }

            [JsonProperty("registrationLegalDate")]
            public DateTimeOffset RegistrationLegalDate { get; set; }

            [JsonProperty("registrationDenialBasis")]
            public object RegistrationDenialBasis { get; set; }

            [JsonProperty("registrationDenialDate")]
            public object RegistrationDenialDate { get; set; }

            [JsonProperty("restorationBasis")]
            public object RestorationBasis { get; set; }

            [JsonProperty("restorationDate")]
            public object RestorationDate { get; set; }

            [JsonProperty("removalBasis")]
            public object RemovalBasis { get; set; }

            [JsonProperty("removalDate")]
            public object RemovalDate { get; set; }

            [JsonProperty("accountNumbers")]
            public string[] AccountNumbers { get; set; }

            [JsonProperty("hasVirtualAccounts")]
            public bool HasVirtualAccounts { get; set; }
        }
    }
}