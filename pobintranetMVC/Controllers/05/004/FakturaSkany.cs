﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace pobintranetMVC.Controllers._05._004
{
    public class FakturaSkany
    {
        public int id_faktury;

        public string faktura;

        public string kierownik;

        public List<string> skany;

        public string sciezka;

        public DateTime data;

        public string kto;

        public double kwota;

        public FakturaSkany()
        {
        }

        public FakturaSkany(int idfaktury, int idkierownika, int idskanu)
        {
            double? sumaVatDm;
            double? nullable;
            double? nullable1;
            double num;
            double? nullable2;
            double? nullable3;
            double? nullable4;
            intranetDataContext _intranetDataContext = new intranetDataContext();
            this.id_faktury = idfaktury;
            this.faktura = (
                from z in _intranetDataContext.zakups
                where z.idzakupu == idfaktury
                select string.Format("{0} [{1}]", z.nrdok_wlasny, z.nrdok_obcy)).First<string>();
            this.kierownik = (
                from p in _intranetDataContext.pracowniks
                where p.idpracownika == idkierownika
                select (p.nazwisko + " ") + p.imie).First<string>();
            this.skany = (
                from s in _intranetDataContext.skany
                where s.id_faktury == idfaktury
                select s.sciezka).ToList<string>();
            skany _skany = (
                from s in _intranetDataContext.skany
                where s.id_skanu == idskanu
                select s).First<skany>();
            this.data = _skany.data_utworzenia;
            this.kto = (
                from p in _intranetDataContext.pracowniks
                where (int?)p.idpracownika == _skany.kto
                select (p.nazwisko + " ") + p.imie).First<string>();
            zakup _zakup = (
                from z in _intranetDataContext.zakups
                where z.idzakupu == idfaktury
                select z).First<zakup>();
            double? sumaNettoPln = _zakup.suma_netto_pln;
            double? sumaVatPln = _zakup.suma_vat_pln;
            if (sumaNettoPln.HasValue & sumaVatPln.HasValue)
            {
                nullable = new double?(sumaNettoPln.GetValueOrDefault() + sumaVatPln.GetValueOrDefault());
            }
            else
            {
                sumaVatDm = null;
                nullable = sumaVatDm;
            }
            double? sumaVatPln1 = nullable;
            if ((sumaVatPln1.GetValueOrDefault() == 0 ? !sumaVatPln1.HasValue : true))
            {
                sumaVatPln = _zakup.suma_netto_pln;
                sumaVatPln1 = _zakup.suma_vat_pln;
                if (sumaVatPln.HasValue & sumaVatPln1.HasValue)
                {
                    nullable1 = new double?(sumaVatPln.GetValueOrDefault() + sumaVatPln1.GetValueOrDefault());
                }
                else
                {
                    sumaVatDm = null;
                    nullable1 = sumaVatDm;
                }
                num = Convert.ToDouble(nullable1);
            }
            else
            {
                sumaNettoPln = _zakup.suma_netto_dm;
                sumaVatDm = _zakup.suma_vat_dm;
                if (sumaNettoPln.HasValue & sumaVatDm.HasValue)
                {
                    nullable2 = new double?(sumaNettoPln.GetValueOrDefault() + sumaVatDm.GetValueOrDefault());
                }
                else
                {
                    nullable2 = null;
                }
                sumaVatPln1 = nullable2;
                float? kursEuro = _zakup.kurs_euro;
                if (kursEuro.HasValue)
                {
                    nullable3 = new double?((double)kursEuro.GetValueOrDefault());
                }
                else
                {
                    sumaVatDm = null;
                    nullable3 = sumaVatDm;
                }
                sumaVatPln = nullable3;
                if (sumaVatPln1.HasValue & sumaVatPln.HasValue)
                {
                    nullable4 = new double?(sumaVatPln1.GetValueOrDefault() * sumaVatPln.GetValueOrDefault());
                }
                else
                {
                    sumaVatDm = null;
                    nullable4 = sumaVatDm;
                }
                num = Convert.ToDouble(nullable4);
            }
            this.kwota = num;
        }

        public List<FakturaSkany> Get100Skanow()
        {
            intranetDataContext _intranetDataContext = new intranetDataContext();
            List<int> list = (
                from s in _intranetDataContext.skany
                select s.id_faktury).ToList<int>();
            List<FakturaSkany> fakturaskany = new List<FakturaSkany>();
            foreach (int num in list)
            {
                skany _skany = (
                    from s in _intranetDataContext.skany
                    where s.id_faktury == num
                    select s).First<skany>();
                FakturaSkany fakturaSkany = new FakturaSkany(_skany.id_faktury, Convert.ToInt32(_skany.id_pracownika), _skany.id_skanu);
                if ((
                    from lfis in fakturaskany
                    where lfis.faktura == fakturaSkany.faktura
                    select lfis).Any<FakturaSkany>())
                {
                    continue;
                }
                fakturaskany.Add(fakturaSkany);
            }
            return fakturaskany.Take<FakturaSkany>(100).ToList<FakturaSkany>();
        }

        public List<FakturaSkany> GetResztaSkanow()
        {
            intranetDataContext _intranetDataContext = new intranetDataContext();
            List<int> list = (
                from s in _intranetDataContext.skany
                select s.id_faktury).ToList<int>();
            List<FakturaSkany> fakturaskany = new List<FakturaSkany>();
            foreach (int num in list)
            {
                skany _skany = (
                    from s in _intranetDataContext.skany
                    where s.id_faktury == num
                    select s).First<skany>();
                FakturaSkany fakturaSkany = new FakturaSkany(_skany.id_faktury, Convert.ToInt32(_skany.id_pracownika), _skany.id_skanu);
                fakturaskany.Add(fakturaSkany);
            }
            int num1 = 100;
            while (fakturaskany[num1].faktura == fakturaskany[num1 + 1].faktura)
            {
                num1++;
            }
            return fakturaskany.Skip<FakturaSkany>(num1).ToList<FakturaSkany>();
        }
    }
}