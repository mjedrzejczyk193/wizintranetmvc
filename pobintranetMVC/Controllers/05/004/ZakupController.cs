﻿using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._05._004
{

    public class ZakupController : Controller
    {
        private string adres_serwera = @"\\10.66.15.109";

        private string sciezka_skanowania = @"\skany\faktury";

        // GET: Zakup
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult DodajSkan(int idfaktury)
        {
            string str = Convert.ToString(base.Session["PlikDoPodgladu"]);
            int num = Convert.ToInt32(base.Session["kt_pracownik_id"]);
            Help.DodajSkan(idfaktury, str, num);
            return base.RedirectToAction("PodgladSkanowFaktury", "Zakup", new { idFaktury = idfaktury, index = 0, zu = num });
        }

        [HttpPost]
        public ActionResult FileUpload(FormCollection collection, HttpPostedFileBase[] files)
        {
            if (!base.ModelState.IsValid)
            {
                return base.View();
            }
            int num = Convert.ToInt32(collection["idfaktury"]);
            int num1 = Convert.ToInt32(base.Session["kt_pracownik_id"]);
            HttpPostedFileBase[] httpPostedFileBaseArray = files;
            for (int i = 0; i < (int)httpPostedFileBaseArray.Length; i++)
            {
                HttpPostedFileBase httpPostedFileBase = httpPostedFileBaseArray[i];
                if (httpPostedFileBase != null && httpPostedFileBase.ContentLength > 0 && (Path.GetExtension(httpPostedFileBase.FileName.ToLower()) == ".pdf" || Path.GetExtension(httpPostedFileBase.FileName.ToLower()) == ".jpg" || Path.GetExtension(httpPostedFileBase.FileName.ToLower()) == ".png"))
                {
                    Help.WgrajPlik(httpPostedFileBase, num, num1, Server.MapPath("~/"));
                }
            }
            return base.RedirectToAction("PodgladSkanowFaktury", "Zakup", new { idFaktury = num, index = 0, zu = num1 });
        }

        public string GetKurs(string date, string country)
        {
            string[] strArrays = date.Split(new char[] { '-' });
            double num = Help.ObliczKurs(string.Concat(new string[] { strArrays[2], "-", strArrays[1], "-", strArrays[0] }), country, false, "");
            return num.ToString();
        }

        public string GetKursOstatniDzien(string date, string country)
        {
            string[] strArrays = date.Split(new char[] { '-' });
            DateTime dateTime = DateTime.Parse(string.Concat(new string[] { strArrays[2], "-", strArrays[1], "-", strArrays[0] }));
            dateTime = dateTime.AddDays(-1);
            double num = Help.ObliczKurs(dateTime.ToShortDateString(), country, false, "");
            return num.ToString();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void GeTPlik(string file)
        {
            try
            {
                base.Response.Clear();
                base.Response.ClearContent();
                base.Response.ClearHeaders();
                base.Response.ContentType = "application/pdf";
                string str = HttpUtility.UrlDecode(file);
                base.Response.TransmitFile(str);
            }
            catch (Exception)
            {
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ListaSkanow()
        {
            ((dynamic)base.ViewBag).Skany100 = (new FakturaSkany()).Get100Skanow();
            return base.View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Pliki()
        {
            List<DirFile> list = Help.GetListaFolderowIPlikow("").Where<DirFile>((DirFile f) =>
            {
                if (f.rodzaj != "dir")
                {
                    return false;
                }
                return f.nazwa != "wgrane";
            }).ToList<DirFile>();
            ((dynamic)base.ViewBag).Foldery = list;
            List<DirFile> dirFiles = new List<DirFile>();
            using (NetworkConnection networkConnection = new NetworkConnection(Help.adres_serwera, new NetworkCredential(Help.user, Help.pass, Help.domain)))
            {
                if (list.Count > 0)
                {
                    if (!string.IsNullOrEmpty(base.Request.QueryString["data"]))
                    {
                        dirFiles = (
                            from f in Help.GetListaFolderowIPlikow(string.Concat(this.adres_serwera, this.sciezka_skanowania, "\\", base.Request.QueryString["data"]))
                            where f.rodzaj == "file"
                            select f).ToList<DirFile>();
                    }
                    else if (base.Session["GoToFirst"] != null)
                    {
                        dirFiles = (
                            from f in Help.GetListaFolderowIPlikow(list[0].sciezka)
                            where f.rodzaj == "file"
                            select f).ToList<DirFile>();
                    }
                    else
                    {
                        List<DirFile> dirFiles1 = list;
                        dirFiles = (
                            from f in Help.GetListaFolderowIPlikow(dirFiles1[dirFiles1.Count - 1].sciezka)
                            where f.rodzaj == "file"
                            select f).ToList<DirFile>();
                    }
                ((dynamic)base.ViewBag).Pliki = dirFiles;
                    if (!string.IsNullOrEmpty(base.Request.QueryString["plik"]))
                    {
                        string str = base.Request.QueryString["plik"].Replace("%5c", "\\").Replace("%", " ").Replace("%20", " ");
                        DirFile dirFile = dirFiles.Where<DirFile>((DirFile p) =>
                        {
                            if (p.sciezka.Contains(str))
                            {
                                return true;
                            }
                            return p.sciezka == str;
                        }).First<DirFile>();
                        base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "", Convert.ToInt32(Session["kt_pracownik_id"]));
                    }
                    else if (dirFiles.Count > 0)
                    {
                        Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFiles[0], base.Server.MapPath("~/"), "", Convert.ToInt32(Session["kt_pracownik_id"]));
                    }
                }
            }
            ((dynamic)base.ViewBag).Pliki = dirFiles;
            return base.View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Podglad()
        {
            List<DirFile> list = (
                from f in Help.GetListaFolderowIPlikow("")
                where f.rodzaj == "dir"
                select f).ToList<DirFile>();
            List<DirFile> dirFiles = new List<DirFile>();
            if (list.Count > 0)
            {
                if (string.IsNullOrEmpty(base.Request.QueryString["data"]))
                {
                    List<DirFile> dirFiles1 = list;
                    dirFiles = (
                        from f in Help.GetListaFolderowIPlikow(dirFiles1[dirFiles1.Count - 1].sciezka)
                        where f.rodzaj == "file"
                        select f).ToList<DirFile>();
                }
                else
                {
                    dirFiles = (
                        from f in Help.GetListaFolderowIPlikow(string.Concat(this.adres_serwera, this.sciezka_skanowania, "\\", base.Request.QueryString["data"]))
                        where f.rodzaj == "file"
                        select f).ToList<DirFile>();
                }
                if (!string.IsNullOrEmpty(base.Request.QueryString["plik"]))
                {
                    string str = base.Request.QueryString["plik"].Replace("%5c", "\\").Replace("%", " ").Replace("%20", " ");
                    DirFile dirFile = dirFiles.Where<DirFile>((DirFile p) =>
                    {
                        if (p.sciezka.Contains(str))
                        {
                            return true;
                        }
                        return p.sciezka == str;
                    }).First<DirFile>();
                    base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "", Convert.ToInt32(Session["kt_pracownik_id"]));
                }
                else if (dirFiles.Count > 0)
                {
                    base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFiles[0], base.Server.MapPath("~/"), "", Convert.ToInt32(Session["kt_pracownik_id"]));
                }
            }
            ((dynamic)base.ViewBag).PlikDoPodgladu = base.Session["PlikDoPodgladu"];
            return base.View();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult PodgladSkanowFaktury(int idFaktury, int? index, int? zu)
        {
            int? nullable = index;
            try
            {
                Help.CzyscSciezkePDF(base.Server.MapPath("~/"));
            }
            catch (Exception)
            {

            }
            if (zu.HasValue)
            {
                int? nullable1 = zu;
                if ((nullable1.GetValueOrDefault() == 0 ? !nullable1.HasValue : true))
                {
                    base.Session["kt_pracownik_id"] = zu;
                }
            }
            zakup faktura = Help.GetFaktura(idFaktury);
            List<pobintranetMVC.skany> skany = Help.GetSkany(idFaktury);
            if (skany.Count <= 0)
            {
                nullable = new int?(0);
            }
            else
            {
                if (!nullable.HasValue)
                {
                    nullable = new int?(0);
                }
                DirFile dirFile = null;
                try
                {
                    dirFile = (
                        from pif in Help.GetListaPlikowWszystkie()
                        where pif.sciezka.Contains(skany[Convert.ToInt32(nullable)].sciezka)
                        select pif).First<DirFile>();
                }
                catch (Exception)
                {
                }

                if (dirFile != null)
                {
                    base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "0", zu);
                }
            }
            ((dynamic)base.ViewBag).Wybrany = nullable;
            ((dynamic)base.ViewBag).Zakup = faktura;
            ((dynamic)base.ViewBag).ListaSkanow = skany;
            ViewBag.Zu = zu;
            return base.View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult reszta_skanow()
        {
            ((dynamic)base.ViewBag).Skany = (new FakturaSkany()).GetResztaSkanow();
            return base.View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Skan(int idfaktury, int? b)
        {
            string doPodgladu = Session["PlikDoPodgladu"] == null ? "" : Session["PlikDoPodgladu"].ToString();
            Session.Clear();
            Session.Abandon();
            Session["PlikDoPodgladu"] = doPodgladu;

            if (!string.IsNullOrEmpty(base.Request.QueryString["plik"]))
            {
                ((dynamic)base.ViewBag).Plik = base.Request.QueryString["plik"];
            }
            if (!string.IsNullOrEmpty(base.Request.QueryString["data"]))
            {
                ((dynamic)base.ViewBag).Data = base.Request.QueryString["data"];
            }
            int? nullable = b;
            if ((nullable.GetValueOrDefault() == 1 ? nullable.HasValue : false))
            {
                base.Session["GoToFirst"] = 1;
            }
            if (base.Session["Version"] != null)
            {
                base.Session["Version"] = Convert.ToInt32(base.Session["Version"]) + 1;
            }
            else
            {
                base.Session["Version"] = 0;
            }
            return base.View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Sortowanie()
        {
            List<DirFile> listaFolderowIPlikow = Help.GetListaFolderowIPlikow("");
            try
            {
                Help.SortujPliki(listaFolderowIPlikow);
                ((dynamic)base.ViewBag).Message = "Wszystkie pliki posortowane. :)";
                ((dynamic)base.ViewBag).TitleMessage = "";
                ((dynamic)base.ViewBag).ColorMessage = "green";
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                ((dynamic)base.ViewBag).Message = "Coś poszło nie tak. Pliki nieposortowane. :(";
                ((dynamic)base.ViewBag).TitleMessage = exception.Message;
                ((dynamic)base.ViewBag).ColorMessage = "red";
            }
            return base.View();
        }

        public string SprawdzSugestieKsiegowania(string nrKonta)
        {
            try
            {
                intranetDataContext _intranetDataContext = new intranetDataContext();
                string str = nrKonta.Substring(0, 3);
                string str1 = nrKonta.Substring(4, 4);
                string str2 = nrKonta.Substring(9, 3);
                string str3 = nrKonta.Substring(13, 3);
                string str4 = (
                    from s2 in _intranetDataContext.kontaSyntetyczne2
                    where s2.s2nr == str1
                    select s2).First<kontaSyntetyczne2>().s2opis;
                if ((
                    from w in _intranetDataContext.wyposazenies
                    where w.numer_rejestracyjny == str4
                    select w).Any<wyposazenie>() && str == "521")
                {
                    int? nullable = (
                        from w in _intranetDataContext.wyposazenies
                        where w.numer_rejestracyjny == str4
                        select w).First<wyposazenie>().zgloszony;
                    if ((nullable.GetValueOrDefault() == 0 ? nullable.HasValue : false) && Help.SprawdzKontoAnalityczneDlaNiezgloszonych(str, str1, str2, str3))
                    {
                        return "TAK";
                    }
                }
            }
            catch (Exception)
            {
            }
            return "NIE";
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult UsunSkan(int id, int idfaktury)
        {
            intranetDataContext _intranetDataContext = new intranetDataContext();
            skany _skany = (
                from s in _intranetDataContext.skany
                where s.id_skanu == id
                select s).First<skany>();
            _intranetDataContext.skany.DeleteOnSubmit(_skany);
            _intranetDataContext.SubmitChanges();
            return base.RedirectToAction("PodgladSkanowFaktury", "Zakup", new { idFaktury = idfaktury, index = 0 });
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void UsunSkanyZFaktury(int id)
        {
            intranetDataContext _intranetDataContext = new intranetDataContext();
            List<skany> list = (
                from s in _intranetDataContext.skany
                where s.id_faktury == id
                select s).ToList<skany>();
            _intranetDataContext.skany.DeleteAllOnSubmit<skany>(list);
            _intranetDataContext.SubmitChanges();
            base.Response.Redirect(string.Format("/aplikacje/05/004/koszt.asp?id_koszt={0}", id));
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult StatusPodpisow(int? id)
        {
            intranetDataContext dc = new intranetDataContext();
            List<skany> result = new List<skany>();
            List<skany> dResult = new List<skany>(); //distinct faktura
            if (id == null)
            {
                result = dc.skany.Distinct().ToList();
            }
            else
            {
                result = dc.skany.Where(s => s.id_faktury == id).Distinct().ToList();
            }

            foreach (var r in result)
            {
                if (!dResult.Where(dr => dr.id_faktury == r.id_faktury).Any())
                {
                    dResult.Add(r);
                }
            }

            ViewBag.Statusy = dResult;

            return View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult DodajPodpis(int? zu, int? idzakupu, int? ktory)
        {
            var relative = "~/images/signatures/" + zu + ".jpg";
            var absolutePath = HttpContext.Server.MapPath(relative);
            if (System.IO.File.Exists(absolutePath))
            {
                ViewBag.Istnieje = 1;


                ViewBag.Podpis = string.Format("../Images/Signatures/{0}.jpg", zu);
            }
            else
            {
                ViewBag.Istnieje = 0;
            }
            ViewBag.Id = idzakupu;
            ViewBag.Zu = zu;

            if (ktory == null)
            {
                ktory = 1;
            }
            ViewBag.Ktory = ktory;

            return View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        [HttpPost]
        public void DodajPodpis(int zu, int idzakupu, List<string> imageData, int ktory)
        {
            try
            {
                string fileName = string.Format("{0}.jpg", zu);
                var relative = "~/images/signatures/" + zu + ".jpg";
                var absolutePath = HttpContext.Server.MapPath(relative);
                string currentPodpis = null;
                if (imageData != null && !string.IsNullOrEmpty(imageData[0]) && imageData[0] != Help.pustyPodpis)
                {
                    using (FileStream fs = new FileStream(absolutePath, FileMode.Create))
                    {
                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {
                            bw.Write(Convert.FromBase64String(imageData[0]));
                            bw.Close();
                        }
                        fs.Close();
                    }
                    currentPodpis = fileName;
                }
            }
            catch (Exception)
            {
            }
            idzakupu = idzakupu == 0 ? idzakupu = -1 : idzakupu;
            if (ktory == 2)
            {
                Response.Redirect(string.Format("Podpis_z?zu={0}&idzakupu={1}&first=0", zu, idzakupu));
            }
            else if (ktory == 3)
            {
                Response.Redirect(string.Format("Podpis_k?zu={0}&idzakupu={1}&first=0", zu, idzakupu));
            }
            else if (ktory == 4)
            {
                Response.Redirect(string.Format("Podpis_d?zu={0}&idzakupu={1}&first=0", zu, idzakupu));
            }
            else if (ktory == 5)
            {
                Response.Redirect(string.Format("Podpis_p?zu={0}&idzakupu={1}&first=0", zu, idzakupu));
            }
            else
            {
                Response.Redirect(string.Format("Podpis_r?zu={0}&idzakupu={1}&first=0", zu, idzakupu));
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void Przekieruj_r(int? zu, int idzakupu)
        {
            if (zu == null)
            {
                zu = -1;
            }

            try
            {
                intranetDataContext dc = new intranetDataContext();
                var skany = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
                foreach (var skan in skany)
                {
                    skan.przekierowanie_r = zu;
                    dc.SubmitChanges();
                }
            }
            catch (Exception)
            {
            }

            //Response.Redirect(string.Format("Podpis_r?zu={0}&idzakupu={1}&first=0", zu, idzakupu));
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void Przekieruj_z(int? zu, int idzakupu)
        {
            if (zu == null)
            {
                zu = -1;
            }
            try
            {
                intranetDataContext dc = new intranetDataContext();
                var skany = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
                foreach (var skan in skany)
                {
                    skan.przekierowanie_z = zu;
                    dc.SubmitChanges();
                }
            }
            catch (Exception)
            {
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void Przekieruj_k(int? zu, int idzakupu)
        {
            if (zu == null)
            {
                zu = -1;
            }
            try
            {
                intranetDataContext dc = new intranetDataContext();
                var skany = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
                foreach (var skan in skany)
                {
                    skan.przekierowanie_k = zu;
                    dc.SubmitChanges();
                }
            }
            catch (Exception)
            {
            }


        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Podpis_r(int? zu, int? idzakupu, int? ktory, int? first)
        {
            Session["kt_pracownik_id"] = zu;
            if (Help.SprawdzUprawnienie(zu, 1))
            {
                ViewBag.Dostep = true;
                List<zakup> fakturyDoZaakceptowania = null;
                List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];
                //first = first == null ? 0 : first;
                //if (fakturyDoZaakceptowaniaSesja != null)
                //{
                //    fakturyDoZaakceptowania = (List<zakup>)Session["FakturyDoZaakceptowania"];
                //}
                //else
                //{
                fakturyDoZaakceptowania = Help.GetFakturyDoZaakceptowania(zu, 1);
                //Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowania;
                //}
                ViewBag.ListaDoZatwierdzenia = fakturyDoZaakceptowania;
                if (fakturyDoZaakceptowania.Count > 0 && idzakupu != -1)
                {
                    List<skany> skany = Help.GetSkany(idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu);
                    ViewBag.IdFaktury = idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu;
                    ViewBag.ListaSkanow = skany;
                    int? wybrany = skany.Count != 0 ? ktory == null || ktory == 0 ? skany[0].id_skanu : skany[Convert.ToInt32(ktory)].id_skanu : 0;
                    ViewBag.Wybrany = wybrany;
                    if (wybrany != 0)
                    {
                        DirFile dirFile = (
                            from pif in Help.GetListaPlikowWszystkie()
                            where pif.sciezka.Contains(skany.Where(s => s.id_skanu == wybrany).FirstOrDefault().sciezka)
                            select pif).First<DirFile>();
                        if (dirFile != null)
                        {
                            base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "0", zu);
                        }
                    }

                    Session["LiczbaFaktur"] = fakturyDoZaakceptowania.Count;
                    if (idzakupu == null)
                    {
                        Session["Indeks"] = 1;
                        ViewBag.Kolejny = fakturyDoZaakceptowania.Count == 1 ? -1 : fakturyDoZaakceptowania[1].idzakupu;
                        ViewBag.Poprzedni = -1;
                    }
                    else
                    {
                        Session["Indeks"] = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        var indekskolejnego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        if (indekskolejnego == fakturyDoZaakceptowania.Count)
                        {
                            ViewBag.Kolejny = -1;
                        }
                        else
                        {
                            ViewBag.Kolejny = fakturyDoZaakceptowania[indekskolejnego].idzakupu;
                        }

                        var indekspoprzedniego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) - 1;
                        if (indekspoprzedniego == -1)
                        {
                            ViewBag.Poprzedni = -1;
                        }
                        else
                        {
                            try
                            {
                                ViewBag.Poprzedni = fakturyDoZaakceptowania[indekspoprzedniego].idzakupu;
                            }
                            catch (Exception)
                            {
                                ViewBag.Poprzedni = -1;
                            }
                        }
                    }
                }
            }
            else
            {
                ViewBag.Dostep = false;
            }

            ViewBag.zu = zu;

            var relative = "~/images/signatures/" + zu + ".jpg";
            var absolutePath = HttpContext.Server.MapPath(relative);
            if (System.IO.File.Exists(absolutePath))
            {
                ViewBag.Istnieje = 1;


                ViewBag.Podpis = string.Format("/Images/Signatures/{0}.jpg", zu);
            }
            else
            {
                ViewBag.Istnieje = 0;
            }

            return View();
        }

        public void DodajNotatke(int zu, int idzakupu, string tresc)
        {
            intranetDataContext dc = new intranetDataContext();
            zakup_notatki notatka = new zakup_notatki();
            notatka.idzakupu = idzakupu;
            notatka.tresc = tresc;
            notatka.zu = zu;
            dc.zakup_notatki.InsertOnSubmit(notatka);
            dc.SubmitChanges();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void NieAkceptujeR(int zu, int kolejny)
        {
            Response.Redirect(string.Format("Podpis_r?zu={0}&idzakupu={1}&first=0", zu, kolejny));
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void AkceptujeR(int zu, int kolejny, int idzakupu)
        {
            List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];

            //Akceptacja skanów
            intranetDataContext dc = new intranetDataContext();
            var skanyDoAkceptacji = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
            foreach (var skan in skanyDoAkceptacji)
            {
                skan.zaakceptowano_rachunkowo = zu;
                skan.przekierowanie_r = zu;
                dc.SubmitChanges();
            }

            if (fakturyDoZaakceptowaniaSesja == null)
            {
                Response.Redirect(string.Format("Podpis_r?zu={0}&idzakupu={1}&first=1", zu, kolejny));
            }
            else
            {
                fakturyDoZaakceptowaniaSesja.RemoveAt(fakturyDoZaakceptowaniaSesja.FindIndex(f => f.idzakupu == idzakupu));
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowaniaSesja;
                Response.Redirect(string.Format("Podpis_r?zu={0}&idzakupu={1}&first=0", zu, kolejny));
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Podpis_z(int? zu, int? idzakupu, int? ktory, int? first)
        {
            Session["kt_pracownik_id"] = zu;
            if (Help.SprawdzUprawnienie(zu, 2))
            {
                ViewBag.Dostep = true;
                List<zakup> fakturyDoZaakceptowania = null;
                List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];
                //first = first == null ? 0 : first;
                //if (fakturyDoZaakceptowaniaSesja != null)
                //{
                //    fakturyDoZaakceptowania = (List<zakup>)Session["FakturyDoZaakceptowania"];
                //}
                //else
                //{
                fakturyDoZaakceptowania = Help.GetFakturyDoZaakceptowania(zu, 2);
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowania;
                //}
                ViewBag.ListaDoZatwierdzenia = fakturyDoZaakceptowania;
                if (fakturyDoZaakceptowania.Count > 0 && idzakupu != -1)
                {
                    List<skany> skany = Help.GetSkany(idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu);
                    ViewBag.IdFaktury = idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu;
                    ViewBag.ListaSkanow = skany;
                    int? wybrany = skany.Count != 0 ? ktory == null || ktory == 0 ? skany[0].id_skanu : skany[Convert.ToInt32(ktory)].id_skanu : 0;
                    ViewBag.Wybrany = wybrany;
                    if (wybrany != 0)
                    {
                        DirFile dirFile = (
                            from pif in Help.GetListaPlikowWszystkie()
                            where pif.sciezka.Contains(skany.Where(s => s.id_skanu == wybrany).FirstOrDefault().sciezka)
                            select pif).First<DirFile>();
                        if (dirFile != null)
                        {
                            base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "0", zu);
                        }
                    }

                    Session["LiczbaFaktur"] = fakturyDoZaakceptowania.Count;
                    if (idzakupu == null)
                    {
                        Session["Indeks"] = 1;
                        ViewBag.Kolejny = fakturyDoZaakceptowania.Count == 1 ? -1 : fakturyDoZaakceptowania[1].idzakupu;
                        ViewBag.Poprzedni = -1;
                    }
                    else
                    {
                        Session["Indeks"] = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        var indekskolejnego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        if (indekskolejnego == fakturyDoZaakceptowania.Count)
                        {
                            ViewBag.Kolejny = -1;
                        }
                        else
                        {
                            ViewBag.Kolejny = fakturyDoZaakceptowania[indekskolejnego].idzakupu;
                        }

                        var indekspoprzedniego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) - 1;
                        if (indekspoprzedniego == -1)
                        {
                            ViewBag.Poprzedni = -1;
                        }
                        else
                        {
                            try
                            {
                                ViewBag.Poprzedni = fakturyDoZaakceptowania[indekspoprzedniego].idzakupu;
                            }
                            catch (Exception)
                            {

                                ViewBag.Poprzedni = -1;
                            }
                        }
                    }
                }
            }
            else
            {
                ViewBag.Dostep = false;
            }

            ViewBag.zu = zu;

            var relative = "~/images/signatures/" + zu + ".jpg";
            var absolutePath = HttpContext.Server.MapPath(relative);
            if (System.IO.File.Exists(absolutePath))
            {
                ViewBag.Istnieje = 1;


                ViewBag.Podpis = string.Format("/Images/Signatures/{0}.jpg", zu);
            }
            else
            {
                ViewBag.Istnieje = 0;
            }

            return View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void NieAkceptujeZ(int zu, int kolejny)
        {
            Response.Redirect(string.Format("Podpis_z?zu={0}&idzakupu={1}&first=0", zu, kolejny));
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void AkceptujeZ(int zu, int kolejny, int idzakupu)
        {
            List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];

            //Akceptacja skanów
            intranetDataContext dc = new intranetDataContext();
            var skanyDoAkceptacji = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
            foreach (var skan in skanyDoAkceptacji)
            {
                skan.zaakceptowano_dz_zakupu = zu;
                skan.przekierowanie_z = zu;
                if (Help.SprawdzUprawnienie(zu, 3))
                {
                    skan.zaakceptowano_kierownik = zu;
                    skan.przekierowanie_k = zu;
                }
                if (Help.SprawdzUprawnienie(zu, 4))
                {
                    skan.zaakceptowano_dyrektor = zu;
                }
                if (Help.SprawdzUprawnienie(zu, 5))
                {
                    skan.zaakceptowano_prezes = zu;
                }
                dc.SubmitChanges();
            }

            if (fakturyDoZaakceptowaniaSesja == null)
            {
                Response.Redirect(string.Format("Podpis_z?zu={0}&idzakupu={1}&first=1", zu, kolejny));
            }
            else
            {
                fakturyDoZaakceptowaniaSesja.RemoveAt(fakturyDoZaakceptowaniaSesja.FindIndex(f => f.idzakupu == idzakupu));
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowaniaSesja;
                Response.Redirect(string.Format("Podpis_z?zu={0}&idzakupu={1}&first=0", zu, kolejny));
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Podpis_k(int? zu, int? idzakupu, int? ktory, int? first)
        {
            Session["kt_pracownik_id"] = zu;
            if (Help.SprawdzUprawnienie(zu, 3))
            {
                ViewBag.Dostep = true;
                List<zakup> fakturyDoZaakceptowania = null;
                List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];
                first = first == null ? 0 : first;
                //if (fakturyDoZaakceptowaniaSesja != null)
                //{
                //    fakturyDoZaakceptowania = (List<zakup>)Session["FakturyDoZaakceptowania"];
                //}
                //else
                //{
                fakturyDoZaakceptowania = Help.GetFakturyDoZaakceptowania(zu, 3);
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowania;
                //}
                ViewBag.ListaDoZatwierdzenia = fakturyDoZaakceptowania;
                if (fakturyDoZaakceptowania.Count > 0 && idzakupu != -1)
                {
                    List<skany> skany = Help.GetSkany(idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu);
                    ViewBag.IdFaktury = idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu;
                    ViewBag.ListaSkanow = skany;
                    int? wybrany = skany.Count != 0 ? ktory == null || ktory == 0 ? skany[0].id_skanu : skany[Convert.ToInt32(ktory)].id_skanu : 0;
                    ViewBag.Wybrany = wybrany;
                    if (wybrany != 0)
                    {
                        DirFile dirFile = (
                                from pif in Help.GetListaPlikowWszystkie()
                                where pif.sciezka.Contains(skany.Where(s => s.id_skanu == wybrany).FirstOrDefault().sciezka)
                                select pif).First<DirFile>();
                        if (dirFile != null)
                        {
                            base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "0", zu);
                        }
                    }

                    Session["LiczbaFaktur"] = fakturyDoZaakceptowania.Count;
                    if (idzakupu == null)
                    {
                        Session["Indeks"] = 1;
                        ViewBag.Kolejny = fakturyDoZaakceptowania.Count == 1 ? -1 : fakturyDoZaakceptowania[1].idzakupu;
                        ViewBag.Poprzedni = -1;
                    }
                    else
                    {
                        Session["Indeks"] = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        var indekskolejnego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        if (indekskolejnego == fakturyDoZaakceptowania.Count)
                        {
                            ViewBag.Kolejny = -1;
                        }
                        else
                        {
                            ViewBag.Kolejny = fakturyDoZaakceptowania[indekskolejnego].idzakupu;
                        }

                        var indekspoprzedniego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) - 1;
                        if (indekspoprzedniego == -1)
                        {
                            ViewBag.Poprzedni = -1;
                        }
                        else
                        {
                            try
                            {
                                ViewBag.Poprzedni = fakturyDoZaakceptowania[indekspoprzedniego].idzakupu;
                            }
                            catch (Exception)
                            {

                                ViewBag.Poprzedni = -1;
                            }
                        }
                    }
                }
            }
            else
            {
                ViewBag.Dostep = false;
            }

            ViewBag.zu = zu;

            var relative = "~/images/signatures/" + zu + ".jpg";
            var absolutePath = HttpContext.Server.MapPath(relative);
            if (System.IO.File.Exists(absolutePath))
            {
                ViewBag.Istnieje = 1;


                ViewBag.Podpis = string.Format("/Images/Signatures/{0}.jpg", zu);
            }
            else
            {
                ViewBag.Istnieje = 0;
            }

            return View();
        }

        public ActionResult GetSelectObiekt(int id, int idp, int? id2, string name)
        {
            if (idp == 0)
            {
                return View();
            }
            else
            {
                intranetDataContext dc = new intranetDataContext();
                var ks2 = dc.kontaSyntetyczne2.Where(k => k.ids2 == idp).First();
                var projekt = dc.projekts.Where(p => p.idprojektu == ks2.idProjektu).First();
                ViewBag.Projekt = projekt;
                if (projekt.rodzaj == 1)
                {
                    ViewBag.Obiekty = dc.obiekties.Where(o => o.idserwisu == projekt.idprojektu).ToList();
                    if (id == 0 && id2 != 0)
                    {
                        ViewBag.Wybrany = id2;
                    }
                    else
                    {
                        ViewBag.Wybrany = id;
                    }
                }
                return View();
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void NieAkceptujeK(int zu, int kolejny)
        {
            Response.Redirect(string.Format("Podpis_k?zu={0}&idzakupu={1}&first=0", zu, kolejny));
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void AkceptujeK(int zu, int kolejny, int idzakupu)
        {
            List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];

            //Akceptacja skanów
            intranetDataContext dc = new intranetDataContext();
            var skanyDoAkceptacji = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
            foreach (var skan in skanyDoAkceptacji)
            {
                skan.zaakceptowano_kierownik = zu;
                skan.przekierowanie_k = zu;
                if (Help.SprawdzUprawnienie(zu, 4))
                {
                    skan.zaakceptowano_dyrektor = zu;
                }
                if (Help.SprawdzUprawnienie(zu, 5))
                {
                    skan.zaakceptowano_prezes = zu;
                }
                dc.SubmitChanges();
            }

            if (fakturyDoZaakceptowaniaSesja == null)
            {
                Response.Redirect(string.Format("Podpis_k?zu={0}&idzakupu={1}&first=1", zu, kolejny));
            }
            else
            {
                fakturyDoZaakceptowaniaSesja.RemoveAt(fakturyDoZaakceptowaniaSesja.FindIndex(f => f.idzakupu == idzakupu));
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowaniaSesja;
                Response.Redirect(string.Format("Podpis_k?zu={0}&idzakupu={1}&first=0", zu, kolejny));
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Podpis_d(int? zu, int? idzakupu, int? ktory, int? first)
        {
            Session["kt_pracownik_id"] = zu;
            if (Help.SprawdzUprawnienie(zu, 4))
            {
                ViewBag.Dostep = true;
                List<zakup> fakturyDoZaakceptowania = null;
                List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];
                first = first == null ? 0 : first;
                //if (fakturyDoZaakceptowaniaSesja != null)
                //{
                //    fakturyDoZaakceptowania = (List<zakup>)Session["FakturyDoZaakceptowania"];
                //}
                //else
                //{
                fakturyDoZaakceptowania = Help.GetFakturyDoZaakceptowania(zu, 4);
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowania;
                //}
                ViewBag.ListaDoZatwierdzenia = fakturyDoZaakceptowania;
                if (fakturyDoZaakceptowania.Count > 0 && idzakupu != -1)
                {
                    List<skany> skany = Help.GetSkany(idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu);
                    ViewBag.IdFaktury = idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu;
                    ViewBag.ListaSkanow = skany;
                    int? wybrany = skany.Count != 0 ? ktory == null || ktory == 0 ? skany[0].id_skanu : skany[Convert.ToInt32(ktory)].id_skanu : 0;
                    ViewBag.Wybrany = wybrany;
                    if (wybrany != 0)
                    {
                        DirFile dirFile = (
                            from pif in Help.GetListaPlikowWszystkie()
                            where pif.sciezka.Contains(skany.Where(s => s.id_skanu == wybrany).FirstOrDefault().sciezka)
                            select pif).First<DirFile>();
                        if (dirFile != null)
                        {
                            base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "0", zu);
                        }
                    }

                    Session["LiczbaFaktur"] = fakturyDoZaakceptowania.Count;
                    if (idzakupu == null)
                    {
                        Session["Indeks"] = 1;
                        ViewBag.Kolejny = fakturyDoZaakceptowania.Count == 1 ? -1 : fakturyDoZaakceptowania[1].idzakupu;
                        ViewBag.Poprzedni = -1;
                    }
                    else
                    {
                        Session["Indeks"] = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        var indekskolejnego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        if (indekskolejnego == fakturyDoZaakceptowania.Count)
                        {
                            ViewBag.Kolejny = -1;
                        }
                        else
                        {
                            ViewBag.Kolejny = fakturyDoZaakceptowania[indekskolejnego].idzakupu;
                        }

                        var indekspoprzedniego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) - 1;
                        if (indekspoprzedniego == -1)
                        {
                            ViewBag.Poprzedni = -1;
                        }
                        else
                        {
                            try
                            {
                                ViewBag.Poprzedni = fakturyDoZaakceptowania[indekspoprzedniego].idzakupu;
                            }
                            catch (Exception)
                            {
                                ViewBag.Poprzedni = -1;
                            }
                        }
                    }
                }
            }
            else
            {
                ViewBag.Dostep = false;
            }

            ViewBag.zu = zu;

            var relative = "~/images/signatures/" + zu + ".jpg";
            var absolutePath = HttpContext.Server.MapPath(relative);
            if (System.IO.File.Exists(absolutePath))
            {
                ViewBag.Istnieje = 1;


                ViewBag.Podpis = string.Format("/Images/Signatures/{0}.jpg", zu);
            }
            else
            {
                ViewBag.Istnieje = 0;
            }

            return View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void NieAkceptujeD(int zu, int kolejny)
        {
            Response.Redirect(string.Format("Podpis_d?zu={0}&idzakupu={1}&first=0", zu, kolejny));
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void AkceptujeD(int zu, int kolejny, int idzakupu)
        {
            List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];

            //Akceptacja skanów
            intranetDataContext dc = new intranetDataContext();
            var skanyDoAkceptacji = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
            foreach (var skan in skanyDoAkceptacji)
            {
                skan.zaakceptowano_dyrektor = zu;
                dc.SubmitChanges();
            }

            if (fakturyDoZaakceptowaniaSesja == null)
            {
                Response.Redirect(string.Format("Podpis_d?zu={0}&idzakupu={1}&first=1", zu, kolejny));
            }
            else
            {
                fakturyDoZaakceptowaniaSesja.RemoveAt(fakturyDoZaakceptowaniaSesja.FindIndex(f => f.idzakupu == idzakupu));
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowaniaSesja;
                Response.Redirect(string.Format("Podpis_d?zu={0}&idzakupu={1}&first=0", zu, kolejny));
            }
        }

        public string NazwaKontaSyntetycznego(int ids2)
        {
            return Help.GetNazwaBudowyKontoSyntetyczne(ids2);
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Podpis_p(int? zu, int? idzakupu, int? ktory, int? first)
        {
            Session["kt_pracownik_id"] = zu;
            if (Help.SprawdzUprawnienie(zu, 5))
            {
                ViewBag.Dostep = true;
                List<zakup> fakturyDoZaakceptowania = null;
                List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];
                first = first == null ? 0 : first;
                //if (fakturyDoZaakceptowaniaSesja != null)
                //{
                //    fakturyDoZaakceptowania = (List<zakup>)Session["FakturyDoZaakceptowania"];
                //}
                //else
                //{
                fakturyDoZaakceptowania = Help.GetFakturyDoZaakceptowania(zu, 5);
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowania;
                //}
                ViewBag.ListaDoZatwierdzenia = fakturyDoZaakceptowania;
                if (fakturyDoZaakceptowania.Count > 0 && idzakupu != -1)
                {
                    List<skany> skany = Help.GetSkany(idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu);
                    ViewBag.IdFaktury = idzakupu == null ? fakturyDoZaakceptowania[0].idzakupu : idzakupu;
                    ViewBag.ListaSkanow = skany;
                    int? wybrany = skany.Count != 0 ? ktory == null || ktory == 0 ? skany[0].id_skanu : skany[Convert.ToInt32(ktory)].id_skanu : 0;
                    ViewBag.Wybrany = wybrany;
                    if (wybrany != 0)
                    {
                        DirFile dirFile = (
                            from pif in Help.GetListaPlikowWszystkie()
                            where pif.sciezka.Contains(skany.Where(s => s.id_skanu == wybrany).FirstOrDefault().sciezka)
                            select pif).First<DirFile>();
                        if (dirFile != null)
                        {
                            base.Session["PlikDoPodgladu"] = Help.GetPlikDoPodgladu(dirFile, base.Server.MapPath("~/"), "0", zu);
                        }
                    }

                    Session["LiczbaFaktur"] = fakturyDoZaakceptowania.Count;
                    if (idzakupu == null)
                    {
                        Session["Indeks"] = 1;
                        ViewBag.Kolejny = fakturyDoZaakceptowania.Count == 1 ? -1 : fakturyDoZaakceptowania[1].idzakupu;
                        ViewBag.Poprzedni = -1;
                    }
                    else
                    {
                        Session["Indeks"] = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        var indekskolejnego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) + 1;
                        if (indekskolejnego == fakturyDoZaakceptowania.Count)
                        {
                            ViewBag.Kolejny = -1;
                        }
                        else
                        {
                            ViewBag.Kolejny = fakturyDoZaakceptowania[indekskolejnego].idzakupu;
                        }

                        var indekspoprzedniego = fakturyDoZaakceptowania.FindIndex(f => f.idzakupu == idzakupu) - 1;
                        if (indekspoprzedniego == -1)
                        {
                            ViewBag.Poprzedni = -1;
                        }
                        else
                        {
                            try
                            {
                                ViewBag.Poprzedni = fakturyDoZaakceptowania[indekspoprzedniego].idzakupu;
                            }
                            catch (Exception)
                            {
                                ViewBag.Poprzedni = -1;
                            }
                        }
                    }
                }
            }
            else
            {
                ViewBag.Dostep = false;
            }

            ViewBag.zu = zu;

            var relative = "~/images/signatures/" + zu + ".jpg";
            var absolutePath = HttpContext.Server.MapPath(relative);
            if (System.IO.File.Exists(absolutePath))
            {
                ViewBag.Istnieje = 1;


                ViewBag.Podpis = string.Format("/Images/Signatures/{0}.jpg", zu);
            }
            else
            {
                ViewBag.Istnieje = 0;
            }

            return View();
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void NieAkceptujeP(int zu, int kolejny)
        {
            Response.Redirect(string.Format("Podpis_p?zu={0}&idzakupu={1}&first=0", zu, kolejny));
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public void AkceptujeP(int zu, int kolejny, int idzakupu)
        {
            List<zakup> fakturyDoZaakceptowaniaSesja = (List<zakup>)Session["FakturyDoZaakceptowania"];

            //Akceptacja skanów
            intranetDataContext dc = new intranetDataContext();
            var skanyDoAkceptacji = dc.skany.Where(s => s.id_faktury == idzakupu).ToList();
            foreach (var skan in skanyDoAkceptacji)
            {
                skan.zaakceptowano_prezes = zu;
                dc.SubmitChanges();
            }

            if (fakturyDoZaakceptowaniaSesja == null)
            {
                Response.Redirect(string.Format("Podpis_p?zu={0}&idzakupu={1}&first=1", zu, kolejny));
            }
            else
            {
                fakturyDoZaakceptowaniaSesja.RemoveAt(fakturyDoZaakceptowaniaSesja.FindIndex(f => f.idzakupu == idzakupu));
                Session["FakturyDoZaakceptowania"] = fakturyDoZaakceptowaniaSesja;
                Response.Redirect(string.Format("Podpis_p?zu={0}&idzakupu={1}&first=0", zu, kolejny));
            }
        }
    }
}