﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Urodziny()
        {
            intranetDataContext dc = new intranetDataContext();
            var pracownicy = dc.pracowniks.Where(p => p.pracuje == true).ToList();

            if (pracownicy.Where(p => p.data_urodzenia.Value.AddYears(DateTime.Now.Year - p.data_urodzenia.Value.Year) >= DateTime.Now.AddDays(-1) && DateTime.Now.AddDays(14) >= p.data_urodzenia.Value.AddYears(DateTime.Now.Year - p.data_urodzenia.Value.Year)).Any())
            {
                ViewBag.Urodziny = pracownicy.Where(p => p.data_urodzenia.Value.AddYears(DateTime.Now.Year - p.data_urodzenia.Value.Year) >= DateTime.Now.AddDays(-1) && DateTime.Now.AddDays(14) >= p.data_urodzenia.Value.AddYears(DateTime.Now.Year - p.data_urodzenia.Value.Year)).OrderBy(p => p.data_urodzenia).ToList();
            }
            else
            {
                ViewBag.Urodziny = new List<pracownik>();
            }

            return View();
        }

        public ActionResult PrzegladySerwis()
        {
            intranetDataContext dc = new intranetDataContext();
            var projektuSerwis = dc.projekts.Where(p => p.rodzaj == 1).ToList();
            List<projekt> lista = new List<projekt>();

            foreach (var p in projektuSerwis)
            {
                if (p.data_przegladu != null)
                {
                    if (DateTime.Now.AddDays(14) >= p.data_przegladu.Value.AddYears(1))
                    {
                        lista.Add(p);
                    }
                }
            }

            ViewBag.Lista = lista;

            return View();
        }

        public ActionResult Komunikaty()
        {
            intranetDataContext dc = new intranetDataContext();
            var pracownicy = dc.pracowniks.Where(p => p.pracuje == true).ToList();



            //BHP
            if (pracownicy.Where(p => DateTime.Now > p.bad_bhp).Any())
            {
                ViewBag.BadanieBhpPrzeterminowane = pracownicy.Where(p => DateTime.Now > p.bad_bhp).ToList();
            }
            else
            {
                ViewBag.BadanieBhpPrzeterminowane = new List<pracownik>();
            }

            if (pracownicy.Where(p => DateTime.Now.AddDays(30) <= p.bad_bhp && DateTime.Now <= p.bad_bhp).Any())
            {
                ViewBag.BadanieBhp = pracownicy.Where(p => DateTime.Now.AddDays(30) <= p.bad_bhp && DateTime.Now <= p.bad_bhp).ToList();
            }
            else
            {
                ViewBag.BadanieBhp = new List<pracownik>();
            }
            //BADANIE LEKARSKIE
            if (pracownicy.Where(p => DateTime.Now > p.bad_lekarskie_nast).Any())
            {
                ViewBag.BadanieLekarskiePrzeterminowane = pracownicy.Where(p => DateTime.Now > p.bad_lekarskie_nast).ToList();
            }
            else
            {
                ViewBag.BadanieLekarskiePrzeterminowane = new List<pracownik>();
            }

            if (pracownicy.Where(p => DateTime.Now.AddDays(30) <= p.bad_lekarskie_nast && DateTime.Now <= p.bad_lekarskie_nast).Any())
            {
                ViewBag.BadanieLekarskie = pracownicy.Where(p => DateTime.Now.AddDays(30) >= p.bad_lekarskie_nast && DateTime.Now <= p.bad_lekarskie_nast).ToList();
            }
            else
            {
                ViewBag.BadanieLekarskie = new List<pracownik>();
            }

            var wyposazenie = dc.wyposazenies.Where(w => w.numer_rejestracyjny != null).ToList();
            //Polisa
            List<polisa> lista_polisa = new List<polisa>();
            List<polisa> lista_polisa_po_terminie = new List<polisa>();
            foreach (var w in wyposazenie)
            {
                polisa p = new polisa();
                p.id = w.id;
                p.numer = w.numer_rejestracyjny;
                try
                {
                    var polisa_do = dc.wyposazenie_ubezpieczenies.Where(wu => wu.id_polisy == w.id).OrderByDescending(wu => wu.termin_obowiazywania_do).First();
                    p.polisa_do = Convert.ToDateTime(polisa_do.termin_obowiazywania_do);
                }
                catch (Exception)
                {

                }
                if (DateTime.Now > p.polisa_do)
                {
                    lista_polisa_po_terminie.Add(p);
                }
                if (DateTime.Now.AddDays(30) >= p.polisa_do && DateTime.Now <= p.polisa_do)
                {
                    lista_polisa.Add(p);
                }
            }
            ViewBag.Polisa = lista_polisa;
            ViewBag.PolisaPrzeterminowane = lista_polisa_po_terminie;
            //Przegląd
            List<przeglad> lista_przegladow = new List<przeglad>();
            List<przeglad> lista_przegladow_po_terminie = new List<przeglad>();
            foreach (var w in wyposazenie)
            {
                przeglad p = new przeglad();
                p.id = w.id;
                p.numer = w.numer_rejestracyjny;
                try
                {
                    var przeglad_do = dc.wyposazenie_przegladies.Where(wu => wu.id_wyposazenia == w.id).OrderByDescending(wu => wu.data_przegladu_rejestracyjnego).First().data_przegladu_rejestracyjnego;
                    p.przeglad_do = Convert.ToDateTime(przeglad_do);
                }
                catch (Exception)
                {
                    p.przeglad_do = DateTime.Parse("01-01-2001");
                }

                if (DateTime.Now > p.przeglad_do)
                {
                    lista_przegladow_po_terminie.Add(p);
                }
                if (DateTime.Now.AddDays(30) >= p.przeglad_do && DateTime.Now <= p.przeglad_do)
                {
                    lista_przegladow.Add(p);
                }
            }
            ViewBag.Przeglad = lista_przegladow;
            ViewBag.PrzegladPrzeterminowane = lista_przegladow_po_terminie;

            //przeterminowane faktury sprzedażowe
            ViewBag.Sprzedaz = dc.sprzedaz_przeterminowanes.ToList();

            return View();
        }

        public class przeglad
        {
            public int id { get; set; }
            public string numer { get; set; }
            public DateTime przeglad_do { get; set; }
        }

        public class polisa
        {
            public int id { get; set; }
            public string numer { get; set; }
            public DateTime polisa_do { get; set; }
        }
    }
}

