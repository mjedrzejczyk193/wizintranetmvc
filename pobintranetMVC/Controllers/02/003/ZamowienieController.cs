﻿using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._02._003
{
    public class ZamowienieController : Controller
    {
        // GET: Zamowienie
        public ActionResult Index(int? idz)
        {
            if (idz == null)
            {
                Response.Redirect("/zamowienia/ZamowieniaLista.aspx");
            }

            intranetDataContext dc = new intranetDataContext();
            List<zamowieniaPozycje> pozycje = dc.zamowieniaPozycjes.Where(p => p.IdZamowienia == idz).ToList();
            zamowieniaDokument dokument = dc.zamowieniaDokuments.Where(d => d.IdZamowienia == idz).First();

            ViewBag.Pozycje = pozycje;
            ViewBag.Dokument = dokument;

            return View();
        }

        public ActionResult LadujUlubione()
        {
            intranetDataContext dc = new intranetDataContext();
            var lista = dc.zamowieniaTopKontrahencis.OrderBy(ztk => ztk.liczba).Take(5).ToList();
            var lista2 = dc.kontrahents.Where(k => k.nazwa.ToLower().Contains("enteli") || k.nazwa.ToLower().Contains("alfa elektro") || k.nazwa.ToLower().Contains("tim spółka akcyjna") || k.nazwa.ToLower().Contains("el 12") || k.nazwa.ToLower().Contains("onninen")).ToList();

            ViewBag.Lista = lista;
            ViewBag.Lista2 = lista2;
            return View();
        }

        public ActionResult Faktury(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            List<zakup_zamowienia> zz = dc.zakup_zamowienias.Where(z => z.id_zakupu == id).OrderBy(z => z.data_wprowadzenia).ToList();

            ViewBag.Lista = zz;

            return View();
        }

        public void UsunFakture(int id, int idz)
        {
            intranetDataContext dc = new intranetDataContext();
            zakup_zamowienia zz = dc.zakup_zamowienias.Where(z => z.id == id).First();
            dc.zakup_zamowienias.DeleteOnSubmit(zz);
            dc.SubmitChanges();

            Response.Redirect("/aplikacje/05/004/koszt.asp?id_koszt=" + idz);
        }

        [HttpPost]
        public void DodajZakupDoZamowienia(int idz, int id_zam, string kwota, int zu, int p)
        {
            intranetDataContext dc = new intranetDataContext();
            zakup_zamowienia zz = new zakup_zamowienia();
            zz.id_zamowienia = id_zam;
            zz.id_zakupu = idz;
            try
            {
                zz.kwota = float.Parse(kwota);
            }
            catch (Exception)
            {
                zz.kwota = float.Parse(kwota.Replace(".", ","));
            }
            zz.kto_wprowadzil = zu;
            zz.data_wprowadzenia = DateTime.Now;
            zz.pozycja = p.ToString();
            dc.zakup_zamowienias.InsertOnSubmit(zz);
            dc.SubmitChanges();

            Response.Redirect("/aplikacje/05/004/koszt.asp?id_koszt=" + idz);
        }

        public void ZatwierdzCeny(int idz)
        {
            intranetDataContext dc = new intranetDataContext();
            var pozycje = dc.zamowieniaPozycjes.Where(zp => zp.IdZamowienia == idz).ToList();
            foreach (var p in pozycje)
            {
                var mat = dc.magazyn_materialies.Where(mm => mm.idmaterialu == p.IdMaterialu).First();
                decimal nowa_cena = Convert.ToDecimal(Math.Round(Convert.ToDouble(p.MaterialCenaJednostkowa), 2, MidpointRounding.AwayFromZero));
                decimal stara_cena = Convert.ToDecimal(Math.Round(Convert.ToDouble(mat.cena_katolofowa), 2, MidpointRounding.AwayFromZero));
                Help.WpiszCenyMaterialuDoArchiwum(p.IdMaterialu, stara_cena, nowa_cena);
                try
                {
                    mat.cena_katolofowa = nowa_cena;
                    dc.SubmitChanges();
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        public ActionResult SelectZamowienia(int? producent)
        {
            intranetDataContext dc = new intranetDataContext();
            var lista = dc.zamowieniaDokuments.OrderByDescending(zd => zd.DataWpisu).ToList();
            if (producent != null)
            {
                lista = lista.Where(l => l.IdKontrahenta == producent).ToList();
            }
            List<zamowieniaDokument> vblista = new List<zamowieniaDokument>();
            foreach (var z in lista)
            {
                try
                {
                    var suma1 = Math.Round(Convert.ToDecimal(dc.zamowieniaPozycjes.Where(zp => zp.IdZamowienia == z.IdZamowienia).Sum(zp => Math.Round(Convert.ToDecimal(zp.WartoscPozycji), 2, MidpointRounding.AwayFromZero))), 2, MidpointRounding.AwayFromZero);
                    var suma2 = Math.Round(Convert.ToDecimal(dc.zakup_zamowienias.Where(zp => zp.id_zamowienia == z.IdZamowienia).Sum(zp => zp.kwota)), 2, MidpointRounding.AwayFromZero);

                    if (suma1 - suma2 != 0)
                    {
                        vblista.Add(z);
                    }
                }
                catch (Exception)
                {

                }

            }
            ViewBag.Lista = vblista;
            return View();
        }

        public ActionResult GetFakturyDoZamowienia(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            List<zakup_zamowienia> zz = dc.zakup_zamowienias.Where(z => z.id_zamowienia == id).OrderBy(z => z.data_wprowadzenia).ToList();

            ViewBag.Lista = zz;

            return View();
        }

        public string SelectCena(int id)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                return Help.GetSumaZamowieniaZPozycji(id);
            }
            catch (Exception)
            {
                return "0.00";
            }
        }

        public string SelectCena2(int id, decimal max)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                if (Convert.ToDecimal(Help.GetSumaZamowieniaZPozycji2(id)) > max)
                {
                    return max.ToString();
                }
                return Help.GetSumaZamowieniaZPozycji2(id);
            }
            catch (Exception)
            {
                return "0.00";
            }
        }

        public ActionResult ZamowienieDoPozycji(int id, float max, int pozycja)
        {
            return View();
        }

        public ActionResult DuplikujZamowienie(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            var zd = dc.zamowieniaDokuments.Where(z => z.IdZamowienia == id).First();
            var pozycjeDuplikowane = dc.zamowieniaPozycjes.Where(zp => zp.IdZamowienia == id).ToList();

            var nz = new zamowieniaDokument();
            nz.DataWpisu = DateTime.Now;
            nz.KtoWpisal = zd.KtoWpisal;
            nz.Modyfikowano = false;
            nz.DataModyfikacji = DateTime.Now;
            nz.Usunieto = zd.Usunieto;
            nz.KtoUsunal = zd.KtoUsunal;
            nz.Status = zd.Status;
            nz.NrProjektu = zd.NrProjektu;
            nz.IdProjektu = zd.IdProjektu;
            nz.DataZamowienia = zd.DataZamowienia;
            nz.NrZamowienia = dc.nadajNrZamowienia(nz.NrProjektu, nz.DataZamowienia);
            nz.ProjektNazwa = zd.ProjektNazwa;
            nz.ProjektAdres = zd.ProjektAdres;
            nz.KontrahentOsoba = zd.KontrahentOsoba;
            nz.KontrahentNazwa = zd.KontrahentNazwa;
            nz.KontrahentUlica = zd.KontrahentUlica;
            nz.IdKontrahenta = zd.IdKontrahenta;
            nz.KontrahentKod = zd.KontrahentKod;
            nz.KontrahentMiejscowosc = zd.KontrahentMiejscowosc;
            nz.KontrahentEmail = zd.KontrahentEmail;
            nz.KontrahentTelefon = zd.KontrahentTelefon;
            nz.KontrahentFax = zd.KontrahentFax;
            nz.FakturaNazwa = zd.FakturaNazwa;
            nz.FakturaOsoba = zd.FakturaOsoba;
            nz.FakturaUlica = zd.FakturaUlica;
            nz.FakturaKod = zd.FakturaKod;
            nz.FakturaMiejscowosc = zd.FakturaMiejscowosc;
            nz.KontaktOsoba = zd.KontaktOsoba;
            nz.KontaktEmail = zd.KontaktEmail;
            nz.KontaktFax = zd.KontaktFax;
            nz.KontaktTelefon = zd.KontaktTelefon;
            nz.WaznoscOferty = zd.WaznoscOferty;
            nz.SumaNetto = zd.SumaNetto;
            nz.Gwarancja = zd.Gwarancja;
            nz.GwarancjaOpis = zd.GwarancjaOpis;
            nz.TerminDostawy = zd.TerminDostawy;
            nz.TerminDostawyOpis = zd.TerminDostawyOpis;
            nz.TerminDostawyGodzina = zd.TerminDostawyGodzina;
            nz.Platnosc = zd.Platnosc;
            nz.PlatnoscOpis = zd.PlatnoscOpis;
            nz.ZamowienieOpis = zd.ZamowienieOpis;
            nz.OfertaZdnia = zd.OfertaZdnia;
            nz.ZnakWlasny = zd.ZnakWlasny;
            nz.ZnakObcy = zd.ZnakObcy;
            nz.PowierzchniaDachu = zd.PowierzchniaDachu;
            nz.Zatwierdzono = false;
            nz.KtoZatwierdzil = "";
            nz.DataZatwierdzenia = null;
            nz.Konstrukcja = zd.Konstrukcja;
            nz.NrZamowieniaKontrahenta = zd.NrZamowieniaKontrahenta;
            nz.OsobaOdpID = zd.OsobaOdpID;
            nz.OsobaOdpImie = zd.OsobaOdpImie;
            nz.OsobaOdpNazwisko = zd.OsobaOdpNazwisko;
            nz.OsobaOdpTelefon = zd.OsobaOdpTelefon;
            nz.OsobaOdpFax = zd.OsobaOdpFax;
            nz.OsobaOdpEmail = zd.OsobaOdpEmail;

            dc.zamowieniaDokuments.InsertOnSubmit(nz);
            dc.SubmitChanges();

            List<zamowieniaPozycje> nowePozycje = new List<zamowieniaPozycje>();
            foreach (var sp in pozycjeDuplikowane)
            {
                var np = new zamowieniaPozycje();
                np.IdZamowienia = nz.IdZamowienia;
                np.IdMaterialu = sp.IdMaterialu;
                np.MaterialNazwa = sp.MaterialNazwa;
                np.MaterialJednostka = sp.MaterialJednostka;
                np.MaterialCenaJednostkowa = sp.MaterialCenaJednostkowa;
                np.MaterialCenaWaluta = sp.MaterialCenaWaluta;
                np.MaterialIlosc = sp.MaterialIlosc;
                np.WartoscPozycji = sp.WartoscPozycji;
                np.Opis = sp.Opis;
                np.Realizacja = 0;
                np.CzasWpisu = DateTime.Now;
                np.MaterialKorekta = sp.MaterialKorekta;
                nowePozycje.Add(np);
            }

            dc.zamowieniaPozycjes.InsertAllOnSubmit(nowePozycje);
            dc.SubmitChanges();

            return Redirect("/zamowienia/Default.aspx?idZamowienia=" + nz.IdZamowienia);
        }

        public ActionResult ZamowienieOpis(int id_zamowienia, int? zu)
        {
            var dc = new intranetDataContext();
            ViewBag.Opisy = dc.zamowienieOpis.Where(zo => zo.id_zamowienia == id_zamowienia).ToList();
            ViewBag.Zamowienie = dc.zamowieniaDokuments.Where(zd => zd.IdZamowienia == id_zamowienia).First();

            return View();
        }

        public ActionResult ZamowienieOpisDodaj(int idzamowienia, string data_wprowadzenia, string data, string opis, int? zu)
        {
            zamowienieOpi zam_opis = new zamowienieOpi();
            zam_opis.id_zamowienia = idzamowienia;
            zam_opis.data_wprowadzenia = DateTime.Parse(data_wprowadzenia);
            zam_opis.data = DateTime.Parse(data);
            zam_opis.opis = opis;
            zam_opis.id_osoby = zu;

            intranetDataContext dc = new intranetDataContext();
            dc.zamowienieOpis.InsertOnSubmit(zam_opis);
            dc.SubmitChanges();

            return Redirect("/zamowienia/Default.aspx?idZamowienia=" + idzamowienia);
        }

        public ActionResult UsunOpis(int id, int idzam)
        {
            intranetDataContext dc = new intranetDataContext();
            zamowienieOpi zo = dc.zamowienieOpis.Where(z => z.id == id).First();
            dc.zamowienieOpis.DeleteOnSubmit(zo);
            dc.SubmitChanges();

            return Redirect("/zamowienia/Default.aspx?idZamowienia=" + idzam);
        }

        public ActionResult ZamowienieRealizacjeDoPozycji(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            zamowieniaPozycje pozycja = dc.zamowieniaPozycjes.Where(z => z.IdPozycji == id).First();
            List<zamowieniaPozycjaOpi> zpo = dc.zamowieniaPozycjaOpis.Where(z => z.id_materialu == pozycja.IdPozycji).ToList();
            zamowieniaDokument zamowienie = dc.zamowieniaDokuments.Where(z => z.IdZamowienia == pozycja.IdZamowienia).First();

            ViewBag.Opisy = zpo;
            ViewBag.Zamowienie = zamowienie;
            ViewBag.Pozycja = pozycja;

            return View();
        }

        public ActionResult DodajOpisPozycji(int id, string data, string opis, string ilosc, int idzam, int? zu, int idprojektu)
        {
            intranetDataContext dc = new intranetDataContext();
            zamowieniaDokument zamowienie = dc.zamowieniaDokuments.Where(z => z.IdZamowienia == idzam).First();
            zamowieniaPozycje pozycja = dc.zamowieniaPozycjes.Where(z => z.IdPozycji == id).First();
            pozycja.Realizacja += float.Parse(ilosc);
            zamowieniaPozycjaOpi zpo = new zamowieniaPozycjaOpi();
            zpo.data = DateTime.Parse(data);
            zpo.data_wprowadzenia = DateTime.Now;
            zpo.ilosc = float.Parse(ilosc);
            zpo.id_materialu = id;
            zpo.id_osoby = zu;
            zpo.opis = opis;
            if (idprojektu == 1)
            {
                zpo.id_projektu = zamowienie.IdProjektu;
            }
            else if (idprojektu > 1)
            {
                zpo.id_projektu = idprojektu;
            }
            else
            {
                zpo.id_projektu = 12550;
            }
            zpo.id_zamowienia = idzam;

            dc.zamowieniaPozycjaOpis.InsertOnSubmit(zpo);

            try
            {
                if (idprojektu == 1)
                {
                    magazyn_wyplyw mw = new magazyn_wyplyw();
                    mw.data = DateTime.Parse(data);
                    mw.datawprowadzenia = DateTime.Now;
                    mw.idprojektu = zpo.id_projektu;
                    mw.miesiac = zpo.data.Month;
                    mw.rok = zpo.data.Year;
                    mw.wartosc = Convert.ToDouble(zpo.ilosc * pozycja.WartoscPozycji);
                    dc.magazyn_wyplyws.InsertOnSubmit(mw);
                }
                else
                {
                    magazyn_wplyw mw = new magazyn_wplyw();
                    mw.data = DateTime.Parse(data);
                    mw.datawprowadzenia = DateTime.Now;
                    mw.idprojektu = zpo.id_projektu;
                    mw.miesiac = zpo.data.Month;
                    mw.rok = zpo.data.Year;
                    mw.wartosc = Convert.ToDouble(zpo.ilosc * pozycja.WartoscPozycji);
                    dc.magazyn_wplyws.InsertOnSubmit(mw);
                }
            }
            catch (Exception)
            {
            }
            dc.SubmitChanges();

            return Redirect("/zamowienia/Default.aspx?idZamowienia=" + idzam);
        }

        public ActionResult EdytujOpisPozycji(int id, string data, string opis, string ilosc, int idzam, int idprojektu, int idrealizacji)
        {
            intranetDataContext dc = new intranetDataContext();
            zamowieniaDokument zamowienie = dc.zamowieniaDokuments.Where(z => z.IdZamowienia == idzam).First();
            zamowieniaPozycje pozycja = dc.zamowieniaPozycjes.Where(z => z.IdPozycji == id).First();
            zamowieniaPozycjaOpi zpo = dc.zamowieniaPozycjaOpis.Where(z => z.id == idrealizacji).First();

            pozycja.Realizacja -= float.Parse(zpo.ilosc.ToString());
            pozycja.Realizacja += float.Parse(ilosc);

            zpo.data = DateTime.Parse(data);
            zpo.data_wprowadzenia = DateTime.Now;
            zpo.ilosc = float.Parse(ilosc);
            zpo.opis = opis;
            if (idprojektu == 1)
            {
                zpo.id_projektu = zamowienie.IdProjektu;
            }
            else
            {
                zpo.id_projektu = 12550;
            }

            try
            {
                if (idprojektu == 1)
                {
                    magazyn_wyplyw mw = new magazyn_wyplyw();
                    mw.data = DateTime.Parse(data);
                    mw.datawprowadzenia = DateTime.Now;
                    mw.idprojektu = zpo.id_projektu;
                    mw.miesiac = zpo.data.Month;
                    mw.rok = zpo.data.Year;
                    mw.wartosc = Convert.ToDouble(zpo.ilosc * pozycja.WartoscPozycji);
                    dc.magazyn_wyplyws.InsertOnSubmit(mw);
                }
                else
                {
                    magazyn_wplyw mw = new magazyn_wplyw();
                    mw.data = DateTime.Parse(data);
                    mw.datawprowadzenia = DateTime.Now;
                    mw.idprojektu = zpo.id_projektu;
                    mw.miesiac = zpo.data.Month;
                    mw.rok = zpo.data.Year;
                    mw.wartosc = Convert.ToDouble(zpo.ilosc * pozycja.WartoscPozycji);
                    dc.magazyn_wplyws.InsertOnSubmit(mw);
                }
            }
            catch (Exception)
            {
            }
            dc.SubmitChanges();

            return Redirect("/zamowienia/Default.aspx?idZamowienia=" + idzam);
        }

        public ActionResult UsunRealizacje(int id, int idzam, int pozycja)
        {
            intranetDataContext dc = new intranetDataContext();
            zamowieniaPozycjaOpi zo = dc.zamowieniaPozycjaOpis.Where(z => z.id == id).First();

            var poz = dc.zamowieniaPozycjes.Where(zp => zp.IdPozycji == pozycja).First();
            poz.Realizacja -= float.Parse(zo.ilosc.ToString());

            dc.zamowieniaPozycjaOpis.DeleteOnSubmit(zo);
            dc.SubmitChanges();

            return Redirect("/zamowienia/Default.aspx?idZamowienia=" + idzam);
        }

        public ActionResult GetRealizacjeRaport(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            List<zamowieniaPozycjaOpi> zo = dc.zamowieniaPozycjaOpis.Where(z => z.id_materialu == id).ToList();
            ViewBag.Lista = zo;
            return View();
        }

        
    }
}