﻿using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._02._003
{
    public class MagazynController : Controller
    {
        // GET: Magazyn
        public ActionResult Index(string nazwa, string producent, string rodzaje, string kategorie, int? zu)
        {
            ViewBags(nazwa, producent, rodzaje, kategorie, Convert.ToInt32(zu), false);

            return View("Index", new { zu = zu });
        }

        [HttpPost]
        public ActionResult Wyszukaj(string nazwa, string producent, string rodzaje, string kategorie, int zu)
        {
            ViewBags(nazwa, producent, rodzaje, kategorie, zu, true);

            return View("Index", new { zu = zu });
        }

        private void ViewBags(string nazwa, string producent, string rodzaje, string kategorie, int zu, bool wyszukaj)
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Rodzaje = dc.rodzajs.ToList();
            ViewBag.Kategorie = dc.magazyn_kategories.ToList();

            List<magazyn_materialy> materialyDoPokazania = dc.magazyn_materialies.ToList();
            List<magazyn_materialy> tmpMaterialy = new List<magazyn_materialy>();
            ViewBag.MaterialyWszystkieNazwy = materialyDoPokazania.Select(m => m.numer_wlasny).ToList();
            int liczbaRekordow = 50;
            if(wyszukaj)
            {
                liczbaRekordow = 10000;
            }
            //if (!string.IsNullOrEmpty(nazwa) || !string.IsNullOrEmpty(producent) || !string.IsNullOrEmpty(rodzaje) || !string.IsNullOrEmpty(kategorie))
            if (!string.IsNullOrEmpty(nazwa) || !string.IsNullOrEmpty(producent) || !string.IsNullOrEmpty(kategorie))
            {
                if (!string.IsNullOrEmpty(nazwa) && !string.IsNullOrEmpty(producent))
                {
                    if (producent != "-1")
                    {
                        materialyDoPokazania = materialyDoPokazania.Where(m => (m.numer_wlasny.ToLower().Contains(nazwa.ToLower()) || m.material.ToLower().Contains(nazwa.ToLower())) && m.producent.Contains(producent)).Take(liczbaRekordow).ToList();
                    }
                    else
                    {
                        materialyDoPokazania = materialyDoPokazania.Where(m => (m.numer_wlasny.ToLower().Contains(nazwa.ToLower()) || m.material.ToLower().Contains(nazwa.ToLower()))).Take(liczbaRekordow).ToList();
                    }
                }
                else if (!string.IsNullOrEmpty(nazwa))
                {
                    materialyDoPokazania = materialyDoPokazania.Where(m => (m.numer_wlasny.ToLower().Contains(nazwa.ToLower()) || m.material.ToLower().Contains(nazwa.ToLower()))).Take(liczbaRekordow).ToList();
                }
                else if (!string.IsNullOrEmpty(producent))
                {
                    if (producent != "-1")
                    {
                        materialyDoPokazania = materialyDoPokazania.Where(m => m.producent.Contains(producent)).Take(liczbaRekordow).ToList();
                    }
                }

                //materialyDoPokazania = dc.magazyn_materialies.Where(k => k.rodzaj.ToLower().Contains(rodzaje.ToLower()) && k.nazwy_kategorii.ToLower().Contains(kategorie.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(kategorie)/* && !string.IsNullOrEmpty(rodzaje)*/)
            {
                foreach (var m in materialyDoPokazania)
                {
                    bool dodacK = false;
                    //bool dodacR = false;
                    foreach (var kat in kategorie.Split(',').ToList())
                    {
                        var pkat = kat.TrimEnd();
                        if (m.nazwy_kategorii.Contains(pkat))
                        {
                            dodacK = true;
                        }
                    }
                    //foreach (var rodz in rodzaje.Split(',').ToList())
                    //{
                    //    if (m.rodzaj.Contains(rodz))
                    //    {
                    //        dodacR = true;
                    //    }
                    //}
                    //if (dodacK && dodacR)
                    if (dodacK)
                    {
                        tmpMaterialy.Add(m);
                    }
                }

                ViewBag.Materialy = tmpMaterialy;
            }
            else
            {

                ViewBag.Materialy = materialyDoPokazania;
            }
            ViewBag.Kontrahenci = dc.producents.OrderBy(k => k.nazwa_producenta).ToList();

            ViewBag.LicznikKoszyk = dc.materialyKoszyks.Where(k => k.IdPracownika == zu).Count();

            ViewBag.Jednostki = dc.jednostkis.ToList();
        }

        [HttpPost]
        public ActionResult Dodaj(string nazwa, string producent, string rodzaje, string kategorie, float cena, int ilosc, string img1, string img2, string img3, string href1, string href2, string href3, int zu, string kod, string cena_katologowa, string jednostka)
        {
            intranetDataContext dc = new intranetDataContext();
            magazyn_materialy nowyMaterial = new magazyn_materialy();
            nowyMaterial.numer_wlasny = nazwa;
            nowyMaterial.material = kod;
            nowyMaterial.producent = producent;
            nowyMaterial.rodzaj = rodzaje;
            nowyMaterial.nazwy_kategorii = kategorie;
            nowyMaterial.cena = cena;
            nowyMaterial.img = img1;
            nowyMaterial.img2 = img2;
            nowyMaterial.img3 = img3;
            nowyMaterial.href = href1;
            nowyMaterial.href2 = href2;
            nowyMaterial.href3 = href3;
            nowyMaterial.ilosc = ilosc;
            try
            {
                nowyMaterial.cena_katolofowa = (decimal)float.Parse(cena_katologowa.Replace(".", ","));
            }
            catch (Exception)
            {
                nowyMaterial.cena_katolofowa = (decimal)float.Parse(cena_katologowa);
            }
            nowyMaterial.jednostka = jednostka;

            var splitKategorie = kategorie.Split(',').ToList();
            var kategorieAll = dc.magazyn_kategories.ToList();
            var idkategorii = "";
            foreach (var k in splitKategorie)
            {
                try
                {
                    idkategorii += kategorieAll.Where(a => k.TrimEnd(' ') == a.nazwa.TrimEnd(' ')).First().id_kategorii + ",";
                }
                catch (Exception)
                {
                }
            }
            nowyMaterial.id_kategorii = idkategorii.TrimEnd(',');
            dc.magazyn_materialies.InsertOnSubmit(nowyMaterial);
            dc.SubmitChanges();
            ViewBags(nazwa, producent, rodzaje, kategorie, zu, true);

            Help.WpiszCenyMaterialuDoArchiwum(nowyMaterial.idmaterialu, 0, Math.Round(Convert.ToDecimal(nowyMaterial.cena_katolofowa), 2, MidpointRounding.AwayFromZero));

            return View("Index", new { zu = zu });
        }

        public ActionResult Edytuj(int m, int zu)
        {
            intranetDataContext dc = new intranetDataContext();
            var material = dc.magazyn_materialies.Where(mm => mm.idmaterialu == m).FirstOrDefault();
            ViewBag.Kontrahenci = dc.producents.OrderBy(k => k.nazwa_producenta).ToList();
            ViewBag.Material = material;
            ViewBag.Rodzaje = dc.rodzajs.ToList();
            ViewBag.Kategorie = dc.magazyn_kategories.ToList();
            ViewBag.Jednostki = dc.jednostkis.ToList();
            ViewBag.Zu = zu;
            return View();
        }

        public ActionResult SelectProducent()
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Kontrahenci = dc.producents.OrderBy(k => k.nazwa_producenta).ToList();
            return View();
        }

        public ActionResult SelectProducent2()
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Kontrahenci = dc.producents.OrderBy(k => k.nazwa_producenta).ToList();
            return View();
        }

        public ActionResult SelectJednostki()
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Jednostki = dc.jednostkis.OrderBy(k => k.nazwa_jednostki).ToList();
            return View();
        }

        public ActionResult SelectMaterialy()
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Materialy = dc.magazyn_materialies.OrderBy(k => k.material).ToList();
            return View();
        }

        public string SelectNip(int id)
        {
            intranetDataContext dc = new intranetDataContext();
            var nip = dc.kontrahents.Where(n => n.idkontahenta == id).First().nip;
            return nip;
        }

        public string SelectCena(int id)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                return dc.magazyn_materialies.Where(m => m.idmaterialu == id).First().cena_katolofowa.ToString();
            }
            catch (Exception)
            {
                return "0.00";
            }
        }

        public string GetWartoscOgolemZProjektu(int id)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                var listaZamowien = dc.zamowieniaDokuments.Where(zd => zd.IdProjektu == id).ToList();
                decimal suma = 0;
                foreach (var z in listaZamowien)
                {
                    decimal sumaPozycje = Convert.ToDecimal(dc.zamowieniaPozycjes.Where(zp => zp.IdZamowienia == z.IdZamowienia).Sum(zp => zp.WartoscPozycji));
                    suma += sumaPozycje;
                }
                return suma.ToString("C"); ;
            }
            catch (Exception)
            {
                return "0";
            }
        }

        public string GetWartoscOgolemZZamowienia(int id)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                decimal sumaPozycje = Convert.ToDecimal(dc.zamowieniaPozycjes.Where(zp => zp.IdZamowienia == id).Sum(zp => zp.WartoscPozycji));
                return sumaPozycje.ToString("C");
            }
            catch (Exception)
            {
                return "0";
            }
        }

        public string SelectJednostka(int id)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                return dc.magazyn_materialies.Where(m => m.idmaterialu == id).First().jednostka;
            }
            catch (Exception)
            {
                return "0.00";
            }
        }

        [HttpPost]
        public ActionResult Edytuj(int zu, int m, string nazwa, int producent, string[] kategorie, string[] rodzaje, float cena, int ilosc, string img1, string img2, string img3, string href1, string href2, string href3, string kod, string cena_katologowa, string jednostka)
        {
            intranetDataContext dc = new intranetDataContext();
            var material = dc.magazyn_materialies.Where(mm => mm.idmaterialu == m).FirstOrDefault();
            decimal stara_cena = Math.Round(Convert.ToDecimal(material.cena_katolofowa), 2, MidpointRounding.AwayFromZero);
            material.numer_wlasny = nazwa;
            material.producent = producent.ToString();
            material.material = kod;
            material.nazwy_kategorii = String.Join(",", kategorie);
            material.rodzaj = rodzaje != null ? String.Join(",", rodzaje) : "";
            material.cena = cena;
            material.ilosc = ilosc;
            material.img = img1;
            material.img2 = img2;
            material.img3 = img3;
            material.href = href1;
            material.href2 = href2;
            material.href3 = href3;
            try
            {
                material.cena_katolofowa = (decimal)float.Parse(cena_katologowa.Replace(".", ","));
            }
            catch(Exception)
            {
                material.cena_katolofowa = (decimal)float.Parse(cena_katologowa);
            }
            material.jednostka = jednostka;

            var splitKategorie = kategorie[0].Split(',');
            var kategorieAll = dc.magazyn_kategories.ToList();
            var idkategorii = "";
            foreach (var k in splitKategorie)
            {
                try
                {
                    idkategorii += kategorieAll.Where(a => k.TrimEnd(' ') == a.nazwa.TrimEnd(' ')).First().id_kategorii + ",";
                }
                catch (Exception)
                {
                }
            }
            material.id_kategorii = idkategorii.TrimEnd(',');

            dc.SubmitChanges();

            Help.WpiszCenyMaterialuDoArchiwum(material.idmaterialu, stara_cena, Math.Round(Convert.ToDecimal(material.cena_katolofowa), 2, MidpointRounding.AwayFromZero));

            return RedirectToAction("Index", "Magazyn", new { zu = zu });
        }

        public string DodajKodProducenta(int m, string kod)
        {
            intranetDataContext dc = new intranetDataContext();
            producent p = dc.producents.Where(s => s.idproducenta == m).FirstOrDefault();
            p.kod_producenta = kod;
            dc.SubmitChanges();

            return string.Format("Zmieniono kod producenta {0} na {1}", p.nazwa_producenta, kod);
        }

        public string DodajDoKoszyka(int zu, int id)
        {
            intranetDataContext dc = new intranetDataContext();
            if (!dc.materialyKoszyks.Where(m => m.IdPracownika == zu && m.IdMaterialu == id).Any())
            {
                materialyKoszyk koszyk = new materialyKoszyk();
                koszyk.czasWpisu = DateTime.Now;
                koszyk.IdMaterialu = id;
                koszyk.IdPracownika = zu;
                dc.materialyKoszyks.InsertOnSubmit(koszyk);
                dc.SubmitChanges();
                return string.Format("Dodano materiał do koszyka");
            }
            else
            {
                return "Materiał już jest w koszyku";
            }

        }

        public string UsunZKoszyka(int zu, int id)
        {
            intranetDataContext dc = new intranetDataContext();
            var mat = dc.materialyKoszyks.Where(mk => mk.IdPracownika == zu && mk.IdMaterialu == id).FirstOrDefault();
            dc.materialyKoszyks.DeleteOnSubmit(mat);
            dc.SubmitChanges();

            return string.Format("Usunięto materiał {0} z koszyka.", Help.GetNazwaMaterialu(id));

        }

        public string UsunWszystkoZKoszyka(int zu)
        {
            intranetDataContext dc = new intranetDataContext();
            var mat = dc.materialyKoszyks.Where(mk => mk.IdPracownika == zu);
            dc.materialyKoszyks.DeleteAllOnSubmit(mat);
            dc.SubmitChanges();

            return string.Format("Usunięto wszystkie materiały z koszyka");

        }

        public ActionResult Usun(int m, int zu)
        {
            try
            {
                intranetDataContext dc = new intranetDataContext();
                magazyn_materialy mat = dc.magazyn_materialies.Where(z => z.idmaterialu == m).FirstOrDefault();
                dc.magazyn_materialies.DeleteOnSubmit(mat);
                dc.SubmitChanges();
                List<materialyKoszyk> mk = dc.materialyKoszyks.Where(k => k.IdMaterialu == m).ToList();
                dc.materialyKoszyks.DeleteAllOnSubmit(mk);
                dc.SubmitChanges();
            }
            catch (Exception)
            {
            }

            ViewBags("", "", "", "", zu, false);
            return View("Index", new { zu = zu });
        }

        [HttpPost]
        public ActionResult DodajRodzaj(string nazwa, int zu)
        {
            intranetDataContext dc = new intranetDataContext();
            rodzaj r = new rodzaj();
            r.nazwa_rodzaju = nazwa;
            r.kto_wpisal = Convert.ToInt32(zu);
            r.czas_wpisu = DateTime.Now;
            dc.rodzajs.InsertOnSubmit(r);
            dc.SubmitChanges();
            ViewBags("", "", "", "", zu, false);

            return View("Index", new { zu = zu });
        }

        public ActionResult PokazKoszyk(int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            List<materialyKoszyk> koszyk = dc.materialyKoszyks.Where(mk => mk.IdPracownika == zu).ToList();
            ViewBag.Koszyk = koszyk;
            return View();
        }

        public ActionResult PokazCenyMaterialuArchiwalne(int idm)
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Ceny = dc.magazyn_materialy_cena_arches.Where(mmca => mmca.idmaterialu == idm).OrderBy(mmca => mmca.data_wprowadzenia).ToList();
            return View();
        }

        public int SprawdzCzyDuplikat(string kod, string producent)
        {
            intranetDataContext dc = new intranetDataContext();
            return Convert.ToInt32(dc.magazyn_materialies.Where(mm => mm.material == kod && mm.producent == producent).Any());
        }
    }
}