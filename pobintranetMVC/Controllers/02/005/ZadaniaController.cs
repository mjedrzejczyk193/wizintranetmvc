﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using pobintranetMVC.Models;

namespace pobintranetMVC.Controllers._02._005
{
    public class ZadaniaController : Controller
    {
        // GET: Podgląd zadań
        public ActionResult Index(int? idprojektu, string from, string to, string zakonczone, string szczegoly)
        {
            intranetDataContext dc = new intranetDataContext();

            List<zadania> listaZadan = new List<zadania>();
            if (idprojektu == null)
            {
                listaZadan = dc.zadanias.OrderByDescending(z => z.idzadania).Take(50).ToList();
            }
            else
            {
                listaZadan = dc.zadanias.Where(z => z.id_projketu == idprojektu).OrderByDescending(z => z.idzadania).ToList();
            }

            if (!string.IsNullOrWhiteSpace(from))
            {
                listaZadan = listaZadan.Where(z => z.data_rozpoczecia >= DateTime.Parse(from)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(to))
            {
                listaZadan = listaZadan.Where(z => z.data_rozpoczecia <= DateTime.Parse(to)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(zakonczone))
            {
                listaZadan = listaZadan.Where(z => z.status == "100" || z.zakonczono > DateTime.Now).ToList();
            }

            ViewBag.Lista = listaZadan;

            return View();
        }

        public ActionResult Podzadania(int idzadania, int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            var listaPodzadan = dc.zadania_podzadanias.Where(pz => pz.id_zadania == idzadania).ToList();
            ViewBag.Lista = listaPodzadan;
            ViewBag.Zadanie = dc.zadanias.Where(z => z.idzadania == idzadania).First();

            return View();
        }

        public ActionResult DodajPodzadanie(int idzadania)
        {
            ViewBag.Id = idzadania;
            return View();
        }

        [HttpPost]
        public ActionResult DodajPodzadanie(string nazwa, string opis, int idzadania, int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            var zadanie = dc.zadanias.Where(z => z.idzadania == idzadania).First();
            var podzadanie = new zadania_podzadania();
            podzadanie.data_utworzenia = DateTime.Now;
            podzadanie.nazwa = nazwa;
            podzadanie.opis = opis;
            podzadanie.id_zadania = idzadania;
            dc.zadania_podzadanias.InsertOnSubmit(podzadanie);
            dc.SubmitChanges();

            return RedirectToAction("Index", "Zadania", new { idprojektu = zadanie.id_projketu, zu = zu });
        }

        public ActionResult UsunPodzadanie(int idzadania, int idpodzadania, int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            var zadanie = dc.zadanias.Where(z => z.idzadania == idzadania).First();
            var podzadanie = dc.zadania_podzadanias.Where(pz => pz.id_podzadania == idpodzadania).First();
            dc.zadania_podzadanias.DeleteOnSubmit(podzadanie);
            dc.SubmitChanges();

            return RedirectToAction("Index", "Zadania", new { idprojektu = zadanie.id_projketu, zu = zu });
        }

        public ActionResult ZakonczPodzadanie(int id, int idzadania, bool czyUsunac, int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            var zadanie = dc.zadanias.Where(z => z.idzadania == idzadania).First();
            var podzadanie = dc.zadania_podzadanias.Where(pz => pz.id_podzadania == id).First();
            if (czyUsunac)
            {
                podzadanie.data_ukonczenia = DateTime.Now;
                podzadanie.zakonczono = true;
            }
            else
            {
                podzadanie.zakonczono = false;
            }
            dc.SubmitChanges();

            return RedirectToAction("Index", "Zadania", new { idprojektu = zadanie.id_projketu, zu = zu });
        }

        public ActionResult Wykres(int? idprojektu, string from, string to)
        {
            intranetDataContext dc = new intranetDataContext();

            List<zadania> listaZadan = new List<zadania>();
            if (idprojektu == null)
            {
                listaZadan = dc.zadanias.OrderByDescending(z => z.idzadania).Take(50).ToList();
            }
            else
            {
                listaZadan = dc.zadanias.Where(z => z.id_projketu == idprojektu).OrderByDescending(z => z.idzadania).ToList();
            }

            if (!string.IsNullOrWhiteSpace(from))
            {
                listaZadan = listaZadan.Where(z => z.data_rozpoczecia >= DateTime.Parse(from)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(to))
            {
                listaZadan = listaZadan.Where(z => z.data_rozpoczecia <= DateTime.Parse(to)).ToList();
            }

            #region Aspose - trzeba by kupić
            //Project project = new Project();

            //foreach(var z in listaZadan.OrderBy(lz => Help.GetBudowa(lz.id_projketu).nazwa).Distinct())
            //{
            //    Task task = project.RootTask.Children.Add(Help.GetBudowa(z.id_projketu).nazwa);
            //    task.Set(Tsk.Start, z.data_rozpoczecia);
            //    task.Set(Tsk.Finish, z.data_zakonczenia);
            //    task.Set(Tsk.Name, Help.GetBudowa(z.id_projketu).nazwa);
            //}

            //string path = Server.MapPath("~/Uploads/");
            //string filename = "Wykres zadań.mpp";

            //project.Save(path + filename, SaveFileFormat.MPP);

            ////Read the File data into Byte Array.
            //byte[] bytes = System.IO.File.ReadAllBytes(path + filename);

            ////Send the File to Download.
            //return File(bytes, "application/octet-stream", filename);
            #endregion

            List<Root> lista = new List<Root>();
            foreach (var z in listaZadan.Select(lz => lz.id_projketu).Distinct().OrderBy(lz => Help.GetBudowa(lz).nazwa))
            {
                Root r = new Root();
                r.id = z;
                r.name = Help.GetBudowa(z).nazwa;
                r.series = GetSeries(z, listaZadan, dc);
                lista.Add(r);
            }

            string json = JsonConvert.SerializeObject(lista);

            ViewBag.Data = json;
            return View();
        }

        public ActionResult Wykres2(int? idprojektu, string from, string to)
        {
            intranetDataContext dc = new intranetDataContext();

            List<zadania> listaZadan = new List<zadania>();
            if (idprojektu == null)
            {
                listaZadan = dc.zadanias.OrderByDescending(z => z.idzadania).ToList();
            }
            else
            {
                listaZadan = dc.zadanias.Where(z => z.id_projketu == idprojektu).OrderByDescending(z => z.idzadania).ToList();
            }

            if (!string.IsNullOrWhiteSpace(from))
            {
                listaZadan = listaZadan.Where(z => z.data_rozpoczecia >= DateTime.Parse(from)).ToList();
            }

            if (!string.IsNullOrWhiteSpace(to))
            {
                listaZadan = listaZadan.Where(z => z.data_rozpoczecia <= DateTime.Parse(to)).ToList();
            }

            ViewBag.Lista = listaZadan;
            var projektyID = listaZadan.Select(lz => lz.id_projketu).Distinct().ToList();
            var projekty = new List<projekt>();
            var wszystkieProjekty = dc.projekts.ToList();
            foreach (var p in projektyID)
            {
                projekty.Add(wszystkieProjekty.Where(wp => wp.idprojektu == p).First());
            }
            ViewBag.Projekty = projekty;

            return View();
        }

        private List<Series> GetSeries(int id_projketu, List<zadania> listaZadan, intranetDataContext dc)
        {
            List<Series> seria = new List<Series>();

            var lista = listaZadan.Where(lz => lz.id_projketu == id_projketu).ToList();
            foreach (var l in lista)
            {
                Series s = new Series();
                s.name = l.nazwa_zadania;
                s.start = l.data_rozpoczecia;
                s.end = l.data_zakonczenia;
                s.color = Help.GetKolorZadania(l.status, l.data_zakonczenia, l.zakonczono);
                s.idz = l.idzadania;
                seria.Add(s);
            }

            return seria;
        }

        public ActionResult DodajZadanie(int? idserwisu, int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            zadania zadanie = new zadania();
            zadanie.status = "0";
            zadanie.data_rozpoczecia = DateTime.Now;
            zadanie.data_zakonczenia = DateTime.Now.AddDays(7);
            ViewBag.Pracownicy = dc.loginy_pracowniks.Where(p => p.pracuje == true).OrderBy(p => p.login_email).ToList();
            ViewBag.Edycja = "0";
            ViewBag.Zu = zu;

            return View(zadanie);
        }

        public ActionResult EdytujZadanie(int idzadania, int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            zadania zadanie = dc.zadanias.Where(z => z.idzadania == idzadania).FirstOrDefault();
            ViewBag.Pracownicy = dc.loginy_pracowniks.Where(p => p.pracuje == true).OrderBy(p => p.login_email).ToList();
            ViewBag.Edycja = "1";
            ViewBag.Zu = zu;

            return View("DodajZadanie", zadanie);
        }

        public ActionResult UsunZadanie(int idzadania, int? zu)
        {
            ViewBag.Zadanie = idzadania;
            ViewBag.Zu = zu;

            return View();
        }

        [HttpPost]
        public ActionResult UsunZadanie(int idzadania, int zu)
        {
            intranetDataContext dc = new intranetDataContext();
            zadania zadanie = dc.zadanias.Where(z => z.idzadania == idzadania).FirstOrDefault();
            dc.zadanias.DeleteOnSubmit(zadanie);
            dc.SubmitChanges();

            return RedirectToAction("Index", "Zadania", new { zu = zu });
        }

        [HttpPost]
        public ActionResult DodajZadanie(string nazwa_zadania, string data_rozpoczecia, string data_zakonczenia, int idprojektu, string status, string adresaci, string rozpoczeto, string wykonanie, string zakonczono, int? idserwisu, int? utworzyl, string zmodyfikowal, string wykonuje, int? zakonczyl, int? zu)
        {
            zadania z = new zadania();
            z.nazwa_zadania = nazwa_zadania;
            z.data_rozpoczecia = DateTime.Parse(data_rozpoczecia);
            z.data_zakonczenia = DateTime.Parse(data_zakonczenia);
            z.id_projketu = idprojektu;
            z.status = status;
            z.adresaci = adresaci;
            if (!string.IsNullOrEmpty(rozpoczeto))
            {
                z.rozpoczeto = DateTime.Parse(rozpoczeto);
            }
            if (!string.IsNullOrEmpty(wykonanie))
            {
                z.wykonanie = DateTime.Parse(wykonanie);
            }
            if (!string.IsNullOrEmpty(zakonczono))
            {
                z.zakonczono = DateTime.Parse(zakonczono);
                z.status = "100";
            }
            else if (status == "100")
            {
                z.zakonczyl = zakonczyl.ToString();
                if (zakonczono == null)
                {
                    z.zakonczono = DateTime.Now;
                }
            }
            z.id_serwisu = idserwisu;
            z.utworzyl = utworzyl == null ? 0 : Convert.ToInt32(utworzyl);
            z.wykonuje = wykonuje;
            z.data_utworzenia = DateTime.Now;

            intranetDataContext dc = new intranetDataContext();
            dc.zadanias.InsertOnSubmit(z);
            dc.SubmitChanges();

            return View("Success");
        }

        [HttpPost]
        public ActionResult EdytujZadanie(int idzadania, string nazwa_zadania, string data_rozpoczecia, string data_zakonczenia, int idprojektu, string status, string adresaci, string rozpoczeto, string wykonanie, string zakonczono, int? idserwisu, int? utworzyl, string zmodyfikowal, string wykonuje, int? zakonczyl, int? zu)
        {
            intranetDataContext dc = new intranetDataContext();
            zadania z = dc.zadanias.Where(zad => zad.idzadania == idzadania).FirstOrDefault();
            z.nazwa_zadania = nazwa_zadania;
            z.data_rozpoczecia = DateTime.Parse(data_rozpoczecia);
            z.data_zakonczenia = DateTime.Parse(data_zakonczenia);
            z.id_projketu = idprojektu;
            z.status = status;
            z.adresaci = adresaci;
            if (!string.IsNullOrEmpty(rozpoczeto))
            {
                z.rozpoczeto = DateTime.Parse(rozpoczeto);
            }
            if (!string.IsNullOrEmpty(wykonanie))
            {
                z.wykonanie = DateTime.Parse(wykonanie);
            }
            if (!string.IsNullOrEmpty(zakonczono))
            {
                if (DateTime.Parse(zakonczono) <= DateTime.Now)
                {
                    z.zakonczono = DateTime.Parse(zakonczono);
                    z.status = "100";
                }
            }
            else if (status == "100")
            {
                z.zakonczyl = zakonczyl.ToString();
                if (zakonczono == null)
                {
                    z.zakonczono = DateTime.Now;
                }
            }

            z.id_serwisu = idserwisu;
            z.zmodyfikowal += zmodyfikowal.ToString() + " {" + DateTime.Now.ToString("dd-MM-yyyy hh:mm") + "} ,";
            z.wykonuje = wykonuje;

            dc.SubmitChanges();

            return RedirectToAction("Index", new { idprojektu = idprojektu, zu = zu });
        }

        public void ZmienStartKoniec(int z, string s, string e)
        {
            intranetDataContext dc = new intranetDataContext();
            var zadanie = dc.zadanias.Where(zad => zad.idzadania == z).First();
            zadanie.data_rozpoczecia = DateTime.Parse(s);
            zadanie.data_zakonczenia = DateTime.Parse(e);
            dc.SubmitChanges();
        }

        public void ZmienNazwe(int z, string n)
        {
            intranetDataContext dc = new intranetDataContext();
            var zadanie = dc.zadanias.Where(zad => zad.idzadania == z).First();
            zadanie.nazwa_zadania = n;
            dc.SubmitChanges();
        }

        private class Series
        {
            public string name { get; set; }
            public DateTime start { get; set; }
            public DateTime end { get; set; }
            public string color { get; set; }
            public int idz { get; set; }
        }

        private class Root
        {
            public int id { get; set; }
            public string name { get; set; }
            public List<Series> series { get; set; }
        }
    }
}