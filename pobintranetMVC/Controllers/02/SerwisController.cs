﻿using Ionic.Zip;
using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._02
{
    public class SerwisController : Controller
    {
        private string sciezkaExceli = @"\\drzewko\wiz\5 TECHNICZNY\503 SERWISY\2. KAUFLAND MARKETY";

        // GET: Serwis
        public ActionResult Index()
        {
            var folders = getListaFolderowIPlikow("");
            List<ZleceniaOZ> zlecenia = new List<ZleceniaOZ>();
            //foreach (var f in folders)
            //{
            //    var currentFolder = getListaFolderowIPlikow(f.sciezka);
            //    if (currentFolder.Where(cf => cf.nazwa == "1. ZLECENIA OTWARTE").Any())
            //    {
            //        var otwarte = getListaFolderowIPlikow(f.sciezka + "\\1. ZLECENIA OTWARTE").ToList();
            //        int lbOtwartych = 0;
            //        foreach (var p in otwarte.Where(o => o.rodzaj == "dir").ToList())
            //        {
            //            var current = getListaFolderowIPlikow(p.sciezka).ToList();
            //            lbOtwartych += current.Where(g => g.rodzaj == "file").Count();
            //        }

            //        var zamkniete = getListaFolderowIPlikow(f.sciezka + "\\2. ZLECENIA ZAMKNIĘTE").ToList();
            //        int lbZamknietych = 0;
            //        foreach (var p in zamkniete.Where(o => o.rodzaj == "dir").ToList())
            //        {
            //            var current = getListaFolderowIPlikow(p.sciezka).ToList();
            //            lbZamknietych += current.Where(g => g.rodzaj == "file").Count();
            //        }

            //        var nazwa = f.nazwa;

            //        ZleceniaOZ zlecenie = new ZleceniaOZ();
            //        zlecenie.liczba_otwartych = lbOtwartych;
            //        zlecenie.liczba_zamknietych = lbZamknietych;
            //        zlecenie.nazwa = nazwa;
            //        zlecenia.Add(zlecenie);
            //    }
            //}
            var serwisy = new intranetDataContext().Usterkis.Where(ser => ser.ktoAnulowal == -1).ToList();

            foreach (var p in serwisy.OrderBy(ser => ser.NrProjektu).Select(ser => ser.idProjektu).Distinct())
            {
                var projekt = Help.GetBudowa(Convert.ToInt32(p));
                ZleceniaOZ zlecenie = new ZleceniaOZ();
                zlecenie.liczba_otwartych = serwisy.Where(s => s.idProjektu == p && s.zakonczono == false).ToList().Count;
                zlecenie.liczba_zamknietych = serwisy.Where(s => s.idProjektu == p && s.zakonczono == true).ToList().Count;
                try
                {
                    zlecenie.nazwa = string.Format("[{0}] {1}", projekt.numer, projekt.nazwa);
                }
                catch (Exception)
                {
                    zlecenie.nazwa = "BRAK NAZWY";
                }
                zlecenia.Add(zlecenie);
            }

            ViewBag.Zlecenia = zlecenia;

            return View();
        }

        public ActionResult Mapa(string numer, string data, string osoba, string obiekt, string serwis, string rozliczono, string anulowano, string projekt, string kontrahent, string rodzaj_projektu, string zakonczono, string typ, string zr)
        {
            intranetDataContext dc = new intranetDataContext();
            var projekty = dc.projekts.Where(p => p.latitude != 0).ToList();
            var serwisyAll = dc.Usterkis.ToList();
            var serwisy = serwisyAll;

            if(zr == "%")
            {
                zr = "";
            }

            if (!string.IsNullOrEmpty(numer) || !string.IsNullOrEmpty(data) || !string.IsNullOrEmpty(osoba) || !string.IsNullOrEmpty(obiekt) || !string.IsNullOrEmpty(serwis) || !string.IsNullOrEmpty(rozliczono) || !string.IsNullOrEmpty(anulowano) || !string.IsNullOrEmpty(typ) || !string.IsNullOrEmpty(zr))
            {
                if (!string.IsNullOrEmpty(numer))
                {
                    serwisy = serwisy.Where(s => s.numerObcy.Contains(numer)).ToList();
                }
                if (!string.IsNullOrEmpty(data))
                {
                    serwisy = serwisy.Where(s => s.dataZgloszenia == DateTime.Parse(data)).ToList();
                }
                if (!string.IsNullOrEmpty(osoba))
                {
                    serwisy = serwisy.Where(s => s.zgloszenieOsoba == osoba).ToList();
                }
                if (!string.IsNullOrEmpty(obiekt))
                {
                    var projektyFiltr = dc.projekts.Where(p => p.nazwa.ToLower().Contains(obiekt.ToLower())).ToList();
                    List<Usterki> serwisyFiltr = new List<Usterki>();
                    foreach (var pf in projektyFiltr.Select(p => p.idprojektu).Distinct().ToList())
                    {
                        var serwisTmp = serwisy.Where(u => u.idProjektu == pf).ToList();
                        foreach (var s in serwisTmp)
                        {
                            serwisyFiltr.Add(s);
                        }
                    }
                    serwisy = serwisyFiltr;
                }
                if (!string.IsNullOrEmpty(rozliczono))
                {
                    if (rozliczono != "Z")
                    //{
                    //    serwisy = serwisy.Where(s => s.zakonczono == true).ToList();
                    //}
                    //else
                    {
                        serwisy = serwisy.Where(s => s.statusZgloszenia != "Z").ToList();
                    }
                }
                if (!string.IsNullOrEmpty(anulowano))
                {
                    if (anulowano != "A")
                    {
                    //    serwisy = serwisy.Where(s => s.statusZgloszenia == "A").ToList();
                    //}
                    //else
                    //{
                        serwisy = serwisy.Where(s => s.statusZgloszenia != "A").ToList();
                    }
                }
                if (!string.IsNullOrEmpty(typ))
                {
                    serwisy = serwisy.Where(s => s.typ.ToUpper() == typ.ToUpper()).ToList();
                }

                if (!string.IsNullOrEmpty(zr))
                {
                    serwisy = serwisy.Where(s => s.zakonczono_roboty.ToUpper() == zr.ToUpper()).ToList();
                }


                projekty = new List<projekt>();
                foreach (var s in serwisy.Select(p => p.idProjektu).Distinct().ToList())
                {
                    try
                    {
                        var pr = dc.projekts.Where(p => p.idprojektu == s).First();
                        projekty.Add(pr);
                    }
                    catch (Exception)
                    {

                    }
                }
            }

            if (!string.IsNullOrEmpty(projekt) || !string.IsNullOrEmpty(kontrahent) || !string.IsNullOrEmpty(rodzaj_projektu) || !string.IsNullOrEmpty(zakonczono))
            {
                if (!string.IsNullOrEmpty(projekt))
                {
                    projekty = projekty.Where(p => p.nazwa.ToLower().Contains(projekt.ToLower())).ToList();
                }
                if (!string.IsNullOrEmpty(kontrahent))
                {
                    var kontrahenciLista = dc.kontrahents.Where(k => k.nazwa.ToLower().Contains(kontrahent.ToLower())).ToList();
                    var projektyTmp = new List<projekt>();
                    foreach (var k in kontrahenciLista)
                    {
                        var projektyLista = projekty.Where(p => p.idkontrahenta == k.idkontahenta).ToList();
                        foreach (var p in projektyLista)
                        {
                            projektyTmp.Add(p);
                        }
                    }
                    projekty = projektyTmp;
                }
                if (!string.IsNullOrEmpty(rodzaj_projektu))
                {
                    if (rodzaj_projektu != "%")
                    {
                        projekty = projekty.Where(p => p.rodzaj == Convert.ToInt32(rodzaj_projektu)).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(zakonczono))
                {
                    if (zakonczono != "%")
                    {
                        projekty = projekty.Where(p => p.p_zakonczony == Convert.ToInt32(zakonczono)).ToList();
                    }
                }
            }

            ViewBag.Projekty = projekty;
            ViewBag.Serwisy = serwisyAll;
            return View();
        }

        public string ZakonczSerwis(int zu, int id)
        {
            intranetDataContext dc = new intranetDataContext();
            Usterki usterka = dc.Usterkis.Where(u => u.IdUsterki == id).First();
            try
            {
                usterka.data_zakonczenia = DateTime.Now;
                usterka.ktoZakonczyl = zu;
                usterka.zakonczono = true;
                usterka.statusZgloszenia = "Z";
                usterka.zakonczono_roboty = "TAK";
                dc.SubmitChanges();
                return "Serwis zakończony w dniu " + usterka.data_zakonczenia;
            }
            catch (Exception)
            {
                return "Nie udało się zakończyć serwisu";
            }
        }

        internal List<DirFile> getListaFolderowIPlikow(string sciezka)
        {
            var impersonationContext = new WrappedImpersonationContext("praca", "mjedrzejczyk", "MJ2020!@");
            impersonationContext.Enter();

            List<DirFile> listaFolderowIPlikow = new List<DirFile>();
            if (string.IsNullOrEmpty(sciezka))
            {
                foreach (string d in Directory.GetDirectories(sciezkaExceli))
                {
                    DirFile folder = new DirFile();
                    folder.rodzaj = "dir";
                    folder.sciezka = d;
                    folder.nazwa = d.Split('\\')[d.Split('\\').Length - 1];

                    listaFolderowIPlikow.Add(folder);
                }

                foreach (string f in Directory.GetFiles(sciezkaExceli))
                {
                    string type = f.Substring(f.Length - 4);
                    //if (type == "xlsx" || type == ".xls")
                    //{
                    DirFile plik = new DirFile();
                    plik.rodzaj = "file";
                    plik.sciezka = f;
                    plik.nazwa = f.Split('\\')[f.Split('\\').Length - 1];

                    listaFolderowIPlikow.Add(plik);
                    //}
                }
            }
            else
            {
                foreach (string d in Directory.GetDirectories(sciezka))
                {
                    DirFile folder = new DirFile();
                    folder.rodzaj = "dir";
                    folder.sciezka = d;
                    folder.nazwa = d.Split('\\')[d.Split('\\').Length - 1];

                    listaFolderowIPlikow.Add(folder);
                }

                foreach (string f in Directory.GetFiles(sciezka))
                {
                    string type = f.Substring(f.Length - 4);
                    //if (type == "xlsx" || type == ".xls")
                    //{
                    DirFile plik = new DirFile();
                    plik.rodzaj = "file";
                    plik.sciezka = f;
                    plik.nazwa = f.Split('\\')[f.Split('\\').Length - 1];

                    listaFolderowIPlikow.Add(plik);
                    //}
                }
            }
            impersonationContext.Leave();

            return listaFolderowIPlikow.OrderByDescending(rekord => rekord.rodzaj).ThenBy(rekord => rekord.nazwa).ToList();
        }

        public void DodajDzialanie(int idusterki, string data, string opis, string osoba, int zu, string ilosc, string realizacja, int dojazd, string f, string t, int? dzialanie, string podwykonawcy)
        {
            //anulacja = "A"
            intranetDataContext dc = new intranetDataContext();
            UsterkiDzialania ud = new UsterkiDzialania();

            if (!dc.UsterkiDzialanias.Where(u => u.IdUsterki == idusterki && u.IdDzialania == dzialanie).Any())
            {
                ud.ktoWpisal = zu;
                ud.czasWpisu = DateTime.Now;
                ud.status = "N";

                ud.IdUsterki = idusterki;
                ud.dataDzialania = DateTime.Parse(data);
                ud.Opis = opis;
                ud.Osoba = string.Format("{0},{1}", osoba, podwykonawcy).TrimEnd(',').TrimEnd();
                if (!string.IsNullOrEmpty(ilosc))
                {
                    try
                    {
                        float fIlosc = TimeSpan.Parse(ilosc).Hours + Help.GetResztaZGodziny(ilosc);
                        ud.ilosc = fIlosc;
                    }
                    catch (Exception)
                    {
                        float fIlosc = float.Parse(ilosc.Split(':')[0]) + Help.GetResztaZGodziny(ilosc);
                        ud.ilosc = fIlosc;
                    }
                }
                ud.realizacja = float.Parse(realizacja.Replace(".", ",").Replace(":", ","));
                ud.czyDojazd = dojazd == 1 ? true : false;
                ud.godzina_do = t;
                ud.godzina_od = f;

                dc.UsterkiDzialanias.InsertOnSubmit(ud);
            }
            else
            {
                ud = dc.UsterkiDzialanias.Where(u => u.IdUsterki == idusterki && u.IdDzialania == dzialanie).First();
                ud.ktoModyfikowal = zu;
                ud.czasModyfikacji = DateTime.Now;

                ud.IdUsterki = idusterki;
                ud.dataDzialania = DateTime.Parse(data);
                ud.Opis = opis;
                ud.Osoba = string.Format("{0},{1}", osoba, podwykonawcy).TrimEnd(',').TrimEnd();

                if (!string.IsNullOrEmpty(ilosc))
                {
                    try
                    {
                        float fIlosc = TimeSpan.Parse(ilosc).Hours + Help.GetResztaZGodziny(ilosc);
                        ud.ilosc = fIlosc;
                    }
                    catch (Exception)
                    {
                        float fIlosc = float.Parse(ilosc.Split(':')[0]) + Help.GetResztaZGodziny(ilosc);
                        ud.ilosc = fIlosc;
                    }
                }
                ud.realizacja = float.Parse(realizacja.Replace(".", ",").Replace(":", ","));
                ud.czyDojazd = dojazd == 1 ? true : false;
                ud.godzina_do = t;
                ud.godzina_od = f;
            }
            dc.SubmitChanges();

            Response.Redirect("/aplikacje/02/008/kartaUsterki.asp?idUsterki=" + idusterki);
        }

        public void UsunDzialanie(int idserwisu, int iddzialania)
        {
            intranetDataContext dc = new intranetDataContext();
            var serwis = dc.UsterkiDzialanias.Where(u => u.IdDzialania == iddzialania).First();
            dc.UsterkiDzialanias.DeleteOnSubmit(serwis);
            dc.SubmitChanges();

            Response.Redirect("/aplikacje/02/008/kartaUsterki.asp?idUsterki=" + idserwisu);
        }

        public ActionResult Protokol(string data, string zamowienie, string data_zamowienia, string idprojektu)
        {
            intranetDataContext dc = new intranetDataContext();
            if (!string.IsNullOrEmpty(idprojektu))
            {
                ViewBag.Projekt = dc.projekts.Where(p => p.idprojektu == Convert.ToInt32(idprojektu)).First();
            }
            else
            {
                ViewBag.Projekt = dc.projekts.Where(p => p.numer == 2303).First();
            }

            return View();
        }

        public ActionResult Protokol2(string data, string zamowienie, string data_zamowienia, string idprojektu)
        {
            intranetDataContext dc = new intranetDataContext();
            if (!string.IsNullOrEmpty(idprojektu))
            {
                ViewBag.Projekt = dc.projekts.Where(p => p.idprojektu == Convert.ToInt32(idprojektu)).First();
            }
            else
            {
                ViewBag.Projekt = dc.projekts.Where(p => p.numer == 2303).First();
            }

            return View();
        }

        public ActionResult Kosztorys(int idserwisu)
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Serwis = dc.Usterkis.Where(u => u.IdUsterki == idserwisu).First();
            ViewBag.Dzialania = dc.UsterkiDzialanias.Where(ud => ud.IdUsterki == idserwisu).ToList();
            return View();
        }

        public ActionResult PobierzZalaczniki(int idserwisu)
        {
            using (ZipFile zip = new ZipFile())
            {
                intranetDataContext dc = new intranetDataContext();
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                var serwisy = dc.plikiMultis.Where(p => p.usterkaId == idserwisu).ToList();

                foreach (var plik in serwisy.Select(s => s.plikV).Distinct().ToList())
                {
                    zip.AddFile(plik);
                }

                string zipName = String.Format("SerwisZałaczniki_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmm"));
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    zip.Save(memoryStream);
                    return File(memoryStream.ToArray(), "application/zip", zipName);
                }
            }
        }

        public ActionResult GetSelectPodwykonawca()
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Wyniki = dc.serwisy_podwykonawcies.OrderBy(sp => sp.nazwa).ToList();
            return View();
        }
    }
}