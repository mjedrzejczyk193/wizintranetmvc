﻿using pobintranetMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._02._002
{
    public class KosztyProjektuController : Controller
    {
        // GET: KosztyProjektu
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult KosztyPoPozycji(int IdProj, string DataOd, string DataDo)
        {
            DateTime from = DateTime.Parse(DataOd);
            DateTime to = DateTime.Parse(DataDo);

            intranetDataContext dc = new intranetDataContext();
            int ids2 = dc.kontaSyntetyczne2.Where(ks => ks.idProjektu == IdProj).First().ids2;
            List<pobintranetMVC.zakup> zakupy = dc.zakups.Where(z => z.ids2 == ids2 && z.data_wystawienia.Value >= from && z.data_wystawienia.Value <= to).OrderBy(z => z.idkontrahenta).ToList();
            List<zakup_pozycje> pozycje = dc.zakup_pozycjes.Where(zp => zp.ids2 == ids2).ToList();
            List<int?> lista_id_kontrahnetow = zakupy.Select(z => z.idkontrahenta).Distinct().ToList();
            List<kontrahent> kontrahenci = new List<kontrahent>();
            List<kontrahent> wszyscy_kontrahenci = dc.kontrahents.ToList();
            foreach(var lik in lista_id_kontrahnetow)
            {
                kontrahenci.Add(wszyscy_kontrahenci.Where(wk => wk.idkontahenta == lik).First());
            }

            ViewBag.Zakupy = zakupy;
            ViewBag.Pozycje = pozycje;
            ViewBag.Kontrahenci = kontrahenci.OrderBy(k => Help.GetNazwaKontrahenta(k.idkontahenta, true)).ToList();

            return View();
        }
    }
}