﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pobintranetMVC.Controllers._06._001
{
    public class PracownikController : Controller
    {
        // GET: Pracownik
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSelectPracownicy()
        {
            intranetDataContext dc = new intranetDataContext();
            ViewBag.Wyniki = dc.pracowniks.Where(p => p.pracuje == true).OrderBy(p => p.imie + p.nazwisko).ToList();
            return View();
        }

        public ActionResult GetGodzinyNaProjekcie(int? idprojektu)
        {
            intranetDataContext dc = new intranetDataContext();
            if (idprojektu != null)
            {
                var akord_od = dc.akord_godzinies.Where(ag => ag.idbudowy == idprojektu).OrderBy(ag => ag.Data).First();
                var akord_do = dc.akord_godzinies.Where(ag => ag.idbudowy == idprojektu).OrderByDescending(ag => ag.Data).First();
                //int miesiac_od = akord_od.Data.Value.Month;
                //int miesiac_do = akord_do.Data.Value.Month;
                //int rok_od = akord_od.Data.Value.Year;
                //int rok_do = akord_do.Data.Value.Year;

                DateTime odData = Convert.ToDateTime(akord_od.Data);
                DateTime doData = Convert.ToDateTime(akord_do.Data);
                ViewBag.odData = odData;
                ViewBag.doData = doData;

                //ViewBag.OdM = miesiac_od;
                //ViewBag.DoM = miesiac_do;
                //ViewBag.OdR = rok_od;
                //ViewBag.DoR = rok_do;
            }

            ViewBag.Projekty = dc.projekts.Where(p => p.idprojektu != 12537).ToList();
            return View();
        }

        public void ZmienDiete(int? id_dek, bool czyPrzypisac)
        {
            intranetDataContext dc = new intranetDataContext();
            var a = dc.akord_godzinies.Where(ag => ag.id_dek == id_dek).First();
            if (czyPrzypisac)
            {
                a.dieta = '1';
            }
            else
            {
                a.dieta = '0';
            }
            dc.SubmitChanges();

        }
    }
}