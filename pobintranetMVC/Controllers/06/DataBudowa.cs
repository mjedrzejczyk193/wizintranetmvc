﻿using System;
using System.Runtime.CompilerServices;

namespace pobintranetMVC.Controllers._06
{
    public class data_budowa
    {
        public string budowa
        {
            get;
            set;
        }

        public DateTime data
        {
            get;
            set;
        }

        public double dojazd
        {
            get;
            set;
        }

        public double @from
        {
            get;
            set;
        }

        public int id_prac
        {
            get;
            set;
        }

        public double przestoje
        {
            get;
            set;
        }

        public double to
        {
            get;
            set;
        }

        public data_budowa()
        {
        }
    }
}