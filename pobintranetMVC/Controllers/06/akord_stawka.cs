﻿using System;
using System.Runtime.CompilerServices;

namespace pobintranetMVC.Controllers._06
{
    public class akord_stawka
    {
        public double akord
        {
            get;
            set;
        }

        public int idpracownika
        {
            get;
            set;
        }

        public int miesiac
        {
            get;
            set;
        }

        public double stawka
        {
            get;
            set;
        }

        public akord_stawka()
        {
        }
    }
}